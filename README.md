
# 尹柏元

## 2021.09.21

1. 文章阅读

- [中高级前端」干货！深度解析瀑布流布局](https://juejin.cn/post/6844904004720263176)

2. 源码阅读

3. 项目进度

- [x]跳转城市页面

## 2021.09.19
1. 文章阅读

- [Refs](https://v3.cn.vuejs.org/api/refs-api.html)

2. 源码阅读

3. 项目进度

- [x] 分类瀑布流

## 2021.09.17

1. 文章阅读

- [探索Vue高阶组件](https://www.jianshu.com/p/6b149189e035)

2. 源码阅读

3. 项目进度

- [] 分类瀑布流

## 2021.09.16

1. 文章阅读

- [vuex源码](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)

2. 源码阅读

3. 项目进度

- [x] 首页分类渲染

- [x] 渲染跳详情

- [x] 获取城市数据并渲染

## 2021.09.15

1. 文章阅读
   
- [vuex源码](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)
  
2. 源码阅读
   
3. 项目进度
   
- [x] 首页轮播图

## 2021.09.14

1. 文章阅读
   
- [移动端兼容问题](https://jasonandjay.github.io/study/zh/standard/Compatibility.html#%E5%B8%B8%E7%94%A8navigator-useragent)
  
- [小程序组件](https://developers.weixin.qq.com/miniprogram/dev/component/)
  
1. 源码阅读
   
2. 项目进度
   
- [x] 安装证书
  
- [x] 安装抓包工具

## 2021.09.13

1. 文章阅读
   
- [小程序组件](https://developers.weixin.qq.com/miniprogram/dev/component/swiper.html)
  
- [小程序路由](https://developers.weixin.qq.com/miniprogram/dev/api/route/wx.switchTab.html)
  
1. 源码阅读
   
2. 项目进度
   
- [x] 一级路由

## 2021.09.12

1. 文章阅读
   
- [uni-app](https://uniapp.dcloud.io/quickstart-cli)
  
1. 源码阅读
   
2. 项目进度

- [x] 发邮件

## 2021.09.10

1. 文章阅读

- [ts-5](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)

2. 源码阅读

- 无

3. leecode 刷题

- [ 存在重复项](https://leetcode-cn.com/problems/contains-duplicate/submissions/)

4. 项目进度

- [x] 请求数据，css 布局
- [x] 文件管理页面的操作
- [x] 知识小册页面
## 2021.09.09

1. 文章阅读
- [ts-7](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)

2. 源码阅读
- [react-hooks](https://react.docschina.org/docs/getting-started.html)

3. leecode 刷题

- [移除元素](https://leetcode-cn.com/problems/remove-element/)

4. 项目进度

- [x] 知识小册页面的排版
- [x] 知识小册跳详情
## 2021.09.08

1. 文章阅读

- [ts-6](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)

2. 源码阅读

- 无

3. leecode 刷题

- [反转链表](https://leetcode-cn.com/problems/reverse-linked-list/)

4. 项目进度

- [x] 请求数据，css 布局
- [x] 知识小册页面

## 2021.9.5
1. 文章阅读
-  [后端技术选型](https://juejin.cn/book/6844733825164148744/section/6844733825205927949)
- [开发环境配置](https://juejin.cn/book/6844733825164148744/section/6844733825210122247)

2. 源码阅读
 
3. leecode 刷题
- [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)

4. 项目进度 
- [x] 页面管理 多选框选中 下线 删除 功能 
- [x] 知识小册反选功能
## 2021.9.3
1. 文章阅读
-  [TypeScript 开发基础知识](https://juejin.cn/book/6844733825164148744/section/6844733825205927944)
- [TypeScript 的使用](https://juejin.cn/book/6844733825164148744/section/6844733825205927943)

2. 源码阅读
 
3. leecode 刷题
- [字符串转换整数 (atoi)](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnoilh/)

4. 项目进度 
- [x] 页面管理 多选框选中 下线 删除 功能 
- [x] 知识小册 图片发布/草稿 删除 功能
## 2021.9.2
1. 文章阅读
-  [Egg.js 构建 GraphQL Api 服务](https://juejin.cn/book/6844733825164148744/section/6844733825134624776)
- [GraphQL API 介绍](https://juejin.cn/book/6844733825164148744/section/6844733825201733645)

2. 源码阅读
 
3. leecode 刷题
- [外观数列](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnpvdm/)

4. 项目进度 
- [x] 页面管理优化
- [x] 知识小册优化
## 2021.9.1
1. 文章阅读
- [React Ho7oks 详解 【近 1W 字】+ 项目实战](https://juejin.cn/post/6844903985338400782)
- [Hooks 优势](https://juejin.cn/post/6844903985338400782)
2. 源码阅读
3. 项目进度
- [x] 搜索
- [x] 文件管理，抽屉效果
## 2021.8.31
1. 文章阅读
- [常用Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B8%B8%E7%94%A8hooks)
- [HTML,CSS,Javascript](https://jasonandjay.github.io/study/zh/interview/#css)
2. 项目进度
- [x] 评论删除，获取数据
## 2021.8.30
1. 文章阅读
-[使用 JavaScript 生成二维码](https://www.runoob.com/w3cnote/javascript-qrcodejs-library.html)
-[数据结构-对象和数组(二)-数组](https://juejin.cn/post/6999262868860305422?utm_source=gold_browser_extension)
2. 源码阅读
3. 项目进度
- [x] 评论管理排版
## 2021.8.29
1. 文章阅读
-  [高级装饰器](https://juejin.cn/book/6844733813021491207/section/6844733813130526727)
- [Reflect Metadata](https://juejin.cn/book/6844733813021491207/section/6844733813130526733)

2. 源码阅读
 
3. 项目进度 
- [x] 八维管理后台路由配置
- [x] 自动化打包上线
## 2021.8.27
1. 文章阅读
- [开发流程](https://jasonandjay.github.io/study/zh/standard/Project.html#%E5%9B%BE%E8%A7%A3)
- [前后端是如何交互的](https://jasonandjay.github.io/study/zh/standard/Cooperation.html#_1-%E5%89%8D%E7%AB%AF%E8%AF%B7%E6%B1%82%E6%95%B0%E6%8D%AEurl%E7%94%B1%E8%B0%81%E6%9D%A5%E5%86%99)
2. 源码阅读
3. 项目答辩
## 2021.8.26
1. 文章阅读
- [React路由](https://jasonandjay.github.io/study/zh/react/React%E8%B7%AF%E7%94%B1.html)
- [单页面应用开发](https://jasonandjay.github.io/study/zh/standard/Spa.html)
2. 源码阅读
3. 项目进度
- [x] hash模式
- [x] history
## 2021.8.25
1. 文章阅读
- [每次渲染都是独立的闭包](https://juejin.cn/post/6844903985338400782)
- [浏览器的缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#%E6%A6%82%E5%BF%B5)
2. 源码阅读
3. 项目进度
- [x] 百度统计
- [x] 懒加载
## 2021.8.24
1. 文章阅读
- [React Ho7oks 详解 【近 1W 字】+ 项目实战](https://juejin.cn/post/6844903985338400782)
- [Hooks 优势](https://juejin.cn/post/6844903985338400782)
2. 源码阅读
3. 项目进度
- [x] 打包上线
- [x] 详情分享
## 2021.8.23
1. 文章阅读
- [命名规范](https://jasonandjay.github.io/study/zh/standard/Standard.html#%E5%89%8D%E7%AB%AF%E8%A7%84%E8%8C%83%E7%B3%BB%E5%88%97)
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)
2. 源码阅读
3. 项目进度
- [] 点击喜欢按钮---->没写完
- [x] 拆分组件
## 2021.8.22
1. 文章阅读
-[使用 JavaScript 生成二维码](https://www.runoob.com/w3cnote/javascript-qrcodejs-library.html)
-[数据结构-对象和数组(二)-数组](https://juejin.cn/post/6999262868860305422?utm_source=gold_browser_extension)
2. 源码阅读
3. 项目进度
- [x] 弹窗二维码
- [x] 弹窗下载
## 2021.8.20
1. 文章阅读
- [自适应处理](https://jasonandjay.github.io/study/zh/standard/Compatibility.html#%E5%A4%9A%E5%80%8D%E5%B1%8F%E5%A4%84%E7%90%86)
- [浏览器的缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html#%E6%A6%82%E5%BF%B5)
2. 源码阅读
3. 项目进度
- [x] 分享弹窗
- [x] 拆分组件
## 2021.8.19
1. 文章阅读
- [常用Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E5%B8%B8%E7%94%A8hooks)
- [HTML,CSS,Javascript](https://jasonandjay.github.io/study/zh/interview/#css)
2. 项目进度
- [x] 小楼又清新 分类组件筛选
- [x] 提交代码，合并。

## 2021.8.18
1. 文章阅读

- [动态路由](https://umijs.org/zh-CN/docs/convention-routing)
- [React Hooks](https://jasonandjay.github.io/study/zh/standard/Hooks.html#%E7%AE%80%E4%BB%8B)
-  ↑ 没看懂

2. 源码阅读

3. 项目进度
- [X] 小楼又清新 封装推荐阅读组件
- [x] 小楼又清新 封装文章标签组件
## 2021.8.17  
1. 文章阅读
-  [Network 篇 - Network 的骚操作](https://juejin.cn/book/6844733783166418958/section/6844733783216750605)
-  [跨域](https://jasonandjay.github.io/study/zh/interview/#%E6%80%A7%E8%83%BD%E4%BC%98%E5%8C%96)
2. 源码阅读
 
3. leecode 刷题

4. 项目进度 
- [x] 小楼又清新 文章页面上拉加载
- [x] 小楼又清新 组件分享遮罩层
- [x] 小楼又清新 优化排版
  
## 2021.8.16
1. 文章阅读
- [UmiJS约定式路由](https://umijs.org/zh-CN/docs/convention-routing)
- [umijs/plugin-dva](https://umijs.org/zh-CN/plugins/plugin-dva) 

1. 源码阅读

2. LeetBook 刷题

3. 项目进度

- [x] 小楼又清新 关于页面渲染
- [x] 小楼又清新 关于页面分页
- [x] 小组合并
  
## 2021.8.15
1. 文章阅读
- [UmiJS约定式路由](https://umijs.org/zh-CN/docs/convention-routing)
- [umijs/plugin-dva](https://umijs.org/zh-CN/plugins/plugin-dva) 

1. 源码阅读

2. LeetBook 刷题

3. 项目进度

- [x] 小楼又清新 关于页面渲染
- [x] 小楼又清新 关于页面分页
- [x] 小组合并
## 2021.8.14
1. 文章阅读
- [网络安全](https://jasonandjay.github.io/study/zh/standard/Security.html#%E7%BD%91%E7%BB%9C%E5%AE%89%E5%85%A8)

1. 源码阅读

2. LeetBook 刷题

3. 项目进度

- [x] 小楼又清新 关于页面排版
- [x] 小楼又清新 获取文章数据
- [x] 小组合并

## 2021.8.13
1. 文章阅读
- [阅读umijsdva](https://umijs.org/zh-CN/plugins/plugin-dva)
- [阅读umijs/plugin-request](https://umijs.org/zh-CN/plugins/plugin-request)
  
2. 源码阅读

3. 项目进度
- [X]学习less
- [X]页面渲染
- [X]获取数据

## 2021.8.12
1. 文章阅读
- [git 命令速查](https://jasonandjay.github.io/study/zh/standard/Start.html#git%E7%8E%AF%E5%A2%83)
- [从 Chrome 说起](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
2. 源码阅读

3. leecode 刷题

4. 项目进度
- [x] UmiJS   
- [x] Hooks
- [x] TAPD
## 2021.8.11
1. 文章阅读
- [git常用操作命令](https://jasonandjay.github.io/study/zh/standard/Start.html#git%E7%8E%AF%E5%A2%83)

2. leecode 刷题
- [数组去重](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)


3. 项目进度

- [x] git 克隆 git clone SSH
- [x] git 合并 git pull origin 名字
- [x] git 上传 git add . git commit -m "X" git push


# 范瑞祥
## 2021.9.15
1. 文章阅读
-  [Vuex框架原理与源码分析](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)
2. 源码阅读
 
3. leecode 刷题
- [实现 Trie (前缀树)](https://leetcode-cn.com/leetbook/read/top-interview-questions/xaeate/)

4. 项目进度 
- [x] 小程序框架搭建
- [x] 美味大牌优惠排版
## 2021.9.14
1. 文章阅读
-  [云开发快速入门](https://juejin.cn/book/6897486502482149376/section/6897486502213713935)
- [项目的创建与配置](https://juejin.cn/book/6897486502482149376/section/6897514181193367552)

2. 源码阅读
 
3. leecode 刷题
- [单词拆分](https://leetcode-cn.com/leetbook/read/top-interview-questions/xa503c/)

4. 项目进度 
- [x] 小程序框架搭建
- [x] 小程序项目分配 

## 2021.09.13
1. 文章阅读
- [小程序组件](https://developers.weixin.qq.com/miniprogram/dev/component/swiper.html)
- [小程序路由](https://developers.weixin.qq.com/miniprogram/dev/api/route/wx.switchTab.html)
2. 源码阅读
3. 项目进度
- [x] 一级路由
## 2021.09.12
1. 文章阅读
- [uni-app](https://uniapp.dcloud.io/quickstart-cli)
2. 源码阅读
3. 项目进度
- [x] 发邮件
## 2021.09.10

1. 文章阅读

- [ts-5](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)

2. 源码阅读

- 无

3. leecode 刷题

- [ 存在重复项](https://leetcode-cn.com/problems/contains-duplicate/submissions/)

4. 项目进度

- [x] 请求数据，css 布局
- [x] 文件管理页面的操作
- [x] 知识小册页面
## 2021.09.09

1. 文章阅读
- [ts-7](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)

2. 源码阅读
- [react-hooks](https://react.docschina.org/docs/getting-started.html)

3. leecode 刷题

- [移除元素](https://leetcode-cn.com/problems/remove-element/)

4. 项目进度

- [x] 知识小册页面的排版
- [x] 知识小册跳详情
## 2021.09.08

1. 文章阅读

- [ts-6](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)

2. 源码阅读

- 无

3. leecode 刷题

- [反转链表](https://leetcode-cn.com/problems/reverse-linked-list/)

4. 项目进度

- [x] 请求数据，css 布局
- [x] 知识小册页面

## 2021.9.5
1. 文章阅读
-  [后端技术选型](https://juejin.cn/book/6844733825164148744/section/6844733825205927949)
- [开发环境配置](https://juejin.cn/book/6844733825164148744/section/6844733825210122247)

2. 源码阅读
 
3. leecode 刷题
- [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)

4. 项目进度 
- [x] 系统设置数据获取、排版

## 2021.9.3
1. 文章阅读
-  [面试题](https://juejin.cn/book/6844733825164148744/section/6844733825205927944)

2. 源码阅读
 
3. leecode 刷题
- [字符串转换整数 (atoi)](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnoilh/)

4. 项目进度 
- [x] 用户管理启用、授权实现


## 2021.9.2
1. 文章阅读
-  [Egg.js 构建 GraphQL Api 服务](https://juejin.cn/book/6844733825164148744/section/6844733825134624776)
- [GraphQL API 介绍](https://juejin.cn/book/6844733825164148744/section/6844733825201733645)

2. 源码阅读
 
3. leecode 刷题
- [外观数列](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnpvdm/)

4. 项目进度 
- [x] 页面管理优化
- [x] 知识小册优化
## 2021.9.1
1. 文章阅读
-  [元素面板篇 - 技巧集合](https://juejin.cn/book/6844733783166418958/section/6844733783216766989)
2. 源码阅读
 
3. leecode 刷题
- [合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array/)
4. 项目进度 
- [x] 聊天室排版和功能

## 2021.8.31
1. 文章阅读
- [网络安全](https://jasonandjay.github.io/study/zh/standard/Security.html#%E7%BD%91%E7%BB%9C%E5%AE%89%E5%85%A8)

2. 源码阅读
 
3. leecode 刷题
- [无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/)
4. 项目进度 
- [x] 登录注册功能和排版
- [x] 系统设置排版


## 2021.8.30
1. 文章阅读
-  [面试题](https://jasonandjay.github.io/study/zh/book/%E7%A7%BB%E5%8A%A8%E7%AB%AF%E7%AF%87.html)

2. 源码阅读
 
3. leecode 刷题
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

4. 项目进度 
- [x] 管理后台划分
- [x] 登录功能排版

## 2021.8.29
1. 文章阅读
-  [Jenkins详细教程](https://www.jianshu.com/p/5f671aca2b5a)

2. 源码阅读
 
3. leecode 刷题
- [旋转图像](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhhkv/)

4. 项目进度 
- [x] jenkins自动打包上线
- [x] 八维后台路由配置

## 2021.8.27
1. 文章阅读
-  [高级类型之交叉类型](https://juejin.cn/book/6844733813021491207/section/6844733813126348814)

2. 源码阅读
 
3. leecode 刷题
- [旋转图像](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhhkv/)

4. 项目进度 
- [x] 项目上线
- [x] 项目优化
## 2021.8.26
1. 文章阅读
-  [路由传参的三种方法](https://www.cnblogs.com/tzwbk/p/12462879.html)

2. 源码阅读
 
3. leecode 刷题
- [无重复字符的最长子串](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/)
4. 项目进度 
- [x] 原生路由封装
## 2021.8.25
1. 文章阅读
-  [前端浏览器缓存知识梳理](https://juejin.cn/post/6947936223126093861)

2. 源码阅读
 
3. leecode 刷题
- [最大子序和](https://leetcode-cn.com/problems/maximum-subarray/)
4. 项目进度 
- [x] 上线项目刷浏览量
- [x] 浏览器缓存

## 2021.8.24
1. 文章阅读
-  [软件测试工具-什么是xftp](https://www.zhihu.com/zvideo/1391778964151582720)

2. 源码阅读
 
3. leecode 刷题
- [计数质数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnzlu6/)
- [3的幂](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnsdi2/)
4. 项目进度 
- [x] 归档详情完善
- [x] 打包上线

## 2021.8.23
1. 文章阅读
-  [React Hooks性能优化](https://juejin.cn/post/6977258085903515685)

2. 源码阅读
 
3. leecode 刷题
- [加一](https://leetcode-cn.com/problems/plus-one/)
- [合并两个有序数组](https://leetcode-cn.com/problems/merge-sorted-array/)
4. 项目进度 
- [x] 详情评论排版功能
- [x] 点赞功能实现和响应式
- [x] 点击海报下载分析


## 2021.8.22
1. 文章阅读
-  [技巧集合](https://juejin.cn/book/6844733783166418958/section/6844733783216766989)

2. 源码阅读
 
3. leecode 刷题
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
4. 项目进度 
- [x] 详情评论排版功能
- [x] 点赞功能
- [x] 目录楼层效果




## 2021.8.20
1. 文章阅读
- [前端术语-Hack](https://jasonandjay.github.io/study/zh/book/%E5%89%8D%E7%AB%AF%E6%9C%AF%E8%AF%AD%E7%AF%87.html)

- [前端术语-MVC](https://jasonandjay.github.io/study/zh/book/%E5%89%8D%E7%AB%AF%E6%9C%AF%E8%AF%AD%E7%AF%87.html)

2. 源码阅读
 
3. leecode 刷题
- [删除数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
4. 项目进度 
- [x] 详情点赞排版
- [x] 详情目录排版


## 2021.8.19
1. 文章阅读
-  [UmiJS开发技巧](https://juejin.cn/post/6844903848553742350)

2. 源码阅读
 
3. leecode 刷题
- [整数反转](https://leetcode-cn.com/problems/reverse-integer/)

4. 项目进度 
- [x] 详情评论排版
- [x] 图片放大拷贝


## 2021.8.18
1. 文章阅读
-  [2021 前端面试 | “HTML + CSS + JS”专题](https://juejin.cn/post/6844903848553742350)

2. 源码阅读
 
3. leecode 刷题
- [爬楼梯](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn854d/)

4. 项目进度 
- [x] 详情跳转
- [x] 详情样式布局
- [x] 详情功能实现


## 2021.8.17
1. 文章阅读
-  [Typescript 的原始类型](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)

2. 源码阅读
 
3. leecode 刷题
- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)

4. 项目进度 
- [x] 响应式布局
- [x] 背景更换，样式优化
- [x] 详情页面的开展
## 2021.8.16
1. 文章阅读
- [【JS干货分享 | 建议收藏】挑战最短时间带你走进JS（一）](https://juejin.cn/post/6996478115488727053)

2. 源码阅读
 
3. leecode 刷题
- [数组去重 ](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnoilh/)

4. 项目进度 
- [x] 项目排版完成
- [x] 背景色更换

## 2021.8.15
1. 文章阅读
- [你真的弄懂 React 了吗？](https://juejin.cn/post/6996478115488727053)

2. 源码阅读
 
3. leecode 刷题
- [字符串转换整数 ](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnoilh/)
- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

4. 项目进度 
- [x] 项目合并
- [x] 页面的渲染
- [x] 实现功能


## 2021.8.13
1. 文章阅读
- [一文带你看懂 UmiJS ](https://juejin.cn/post/6844904197331091464)

2. 源码阅读
 
3. leecode 刷题
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)
- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnx13t/)

4. 项目进度 
- [x] 项目的框架搭建
- [x] '归档'的数据请求
- [x] 页面的渲染



## 2021.8.12

1. 文章阅读

- [死磕 36 个 JS 手写题](https://mp.weixin.qq.com/s/sDZudDS2jn8PZrSAtkicTg?from=groupmessage&isappinstalled=0&scene=1&clicktime=1628244019&enterid=1628244019)

2. 源码阅读
 
3. leecode 刷题

- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

4. 项目进度 
- [x] UmiJS的使用
- [x] 项目任务分布

## 2021.8.11

1. 文章阅读

- [你不知道的 Chrome 调试技巧](https://juejin.cn/book/6844733783166418958)

- [我在工作中是如何使用 git 的](https://juejin.cn/post/6974184935804534815)

2. 源码阅读

3. leecode 刷题
- 删除排序数组中的重复项
- 加一

4. 项目进度

# 刘泽彪
## 2021.9.5
1. 文章阅读

-  [推荐 10 个很“哇塞”的Web“资源”给前端工友，收藏等于学会~](https://juejin.cn/post/7003114103094902792?utm_source=gold_browser_extension)
2. 源码阅读
 
3. leecode 刷题

4. 项目进度 
- [x] 优化功能
## 2021.9.3


1. 文章阅读

-  [巧用CSS filter，让你的网站更加酷炫！](https://juejin.cn/post/7002829486806794276?utm_source=gold_browser_extension)
2. 源码阅读
 
3. leecode 刷题

4. 项目进度 
- [x] 答辩
## 2021.9.2


1. 文章阅读

-  [web前端从零入门到实战（3万字长文）](https://juejin.cn/post/7004350348827885581?utm_source=gold_browser_extension)
2. 源码阅读
 
3. leecode 刷题

4. 项目进度 
- [x] 标签管理功能及排版
## 2021.9.1


1. 文章阅读

-  [web前端从零入门到实战（3万字长文）](https://juejin.cn/post/7004350348827885581?utm_source=gold_browser_extension)
2. 源码阅读
 
3. leecode 刷题

4. 项目进度 
- [x] 分类管理功能及排版
## 2021.8.31


1. 文章阅读

-  [web前端从零入门到实战（3万字长文）](https://juejin.cn/post/7004350348827885581?utm_source=gold_browser_extension)
2. 源码阅读
 
3. leecode 刷题

4. 项目进度 
- [x] 所有文章功能及排版

## 2021.8.30
1. 文章阅读

-  [【前端可视化】如何在React中优雅的使用ECharts🍉](https://juejin.cn/post/7000551946029858830)
2. 源码阅读
 
3. leecode 刷题

4. 项目进度 
- [x] 工作台排版
- [x] 画布柱形图
- [x] 快速导航
## 2021.8.29
1. 文章阅读

-  [CSS复合选择器、元素显示模式、背景】前端小抄](https://www.cnblogs.com/JERRY-Z-J-R/p/15208214.html?utm_source=gold_browser_extension)
2. 源码阅读
 
3. leecode 刷题
4. 项目进度 
- [x] 项目答辩


## 2021.8.27
1. 文章阅读

-  [面试题](https://jasonandjay.github.io/study/zh/book/%E7%A7%BB%E5%8A%A8%E7%AB%AF%E7%AF%87.html)
2. 源码阅读
 
3. leecode 刷题
- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)

4. 项目进度 
- [x] 手动搭建
- [x] 项目上线

## 2021.8.26
1. 文章阅读

-  [面试题](https://jasonandjay.github.io/study/zh/book/%E7%A7%BB%E5%8A%A8%E7%AB%AF%E7%AF%87.html)
2. 源码阅读
 
3. leecode 刷题
- [缺失数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)

4. 项目进度 
- [x] 手动搭建
  

## 2021.8.25
1. 文章阅读

-  [面试题](https://jasonandjay.github.io/study/zh/book/%E7%A7%BB%E5%8A%A8%E7%AB%AF%E7%AF%87.html)
2. 源码阅读
 
3. leecode 刷题
- [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)

4. 项目进度 
- [x] 百度统计
## 2021.8.24
1. 文章阅读

-  [元素面板篇 - 技巧集合](https://juejin.cn/book/6844733783166418958/section/6844733783216766989)
2. 源码阅读
 
3. leecode 刷题
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)

4. 项目进度 
- [x] 头部响应式
  
## 2021.8.23
1. 文章阅读

-  [console篇 - console中骚操作](https://juejin.cn/book/6844733783166418958/section/6844733783216766983)
2. 源码阅读
 
3. leecode 刷题
- [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)

4. 项目进度 
- [x] 留言板验证
- [x] 登录
- [x] 回复  


## 2021.8.22
1. 文章阅读

-  [typescript 高级用法1](https://juejin.cn/post/6998564257583333407?utm_source=gold_browser_extension)
2. 源码阅读
 
3. leecode 刷题
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)

4. 项目进度 
- [x] 优化代码
- [x] 优化组件 



## 2021.8.20
1. 文章阅读

- [动态路由](https://umijs.org/zh-CN/docs/convention-routing)

2. 源码阅读
 
3. leecode 刷题

4. 项目进度 
- [x] 中英文转换  
- [x] 太阳 月亮 动效




## 2021.8.19
1. 文章阅读
-  [【利器篇】前端40+精选VSCode插件，总有几个你未拥有！](https://juejin.cn/post/6997186741866070023?utm_source=gold_browser_extension)

2. 源码阅读
 
3. leecode 刷题
- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

4. 项目进度 
- [x] 优化


## 2021.8.18
1. 文章阅读
-  [2021 前端面试 | “HTML + CSS + JS”专题](https://juejin.cn/post/6844903848553742350)

2. 源码阅读
 
3. leecode 刷题
   
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnumcr/)

4. 项目进度 

- [x]  拆分组件


## 2021.8.17
1. 文章阅读
-  [【JS干货分享 | 建议收藏】挑战最短时间带你走进JS（六）](https://juejin.cn/post/6997220640759496734?utm_source=gold_browser_extension)
-  [【JS干货分享 | 建议收藏】挑战最短时间带你走进JS（五）](https://juejin.cn/post/6996819069504585736)
2. 源码阅读
 
3. leecode 刷题
    数组去重
4. 项目进度 
- [x] 小楼又清新 留言板分页
- [x] 小楼又清新 优化排版
  
## 2021.8.16
1. 文章阅读
- [【JS干货分享 | 建议收藏】挑战最短时间带你走进JS（四）](https://juejin.cn/post/6996434668908183566)
- [【JS干货分享 | 建议收藏】挑战最短时间带你走进JS（三）](https://juejin.cn/post/6985456953661063204) 

1. 源码阅读

2. LeetBook 刷题

3. 项目进度

- [x] 小楼又清新 夜间模式
- [x] 小楼又清新 返回顶部
  
## 2021.8.15

1. 文章阅读

- [【JS干货分享 | 建议收藏】挑战最短时间带你走进JS（一）](https://juejin.cn/post/6985072343257677855)

- [【JS干货分享 | 建议收藏】挑战最短时间带你走进JS（二）](https://juejin.cn/post/6987241984154927134)

2. 源码阅读

3. LeetBook 刷题

- [旋转图像](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

1. 项目进度

- [x] 小楼又清风 大部分排版功能

## 2021.8.13

1. 文章阅读

- [UmiJS开发技巧](https://juejin.cn/post/6844904198329335822)

- [从0到1教你搭建前端团队的组件系统（高级进阶必备）](https://juejin.cn/post/6844904068431740936)

2. 源码阅读

3. LeetBook 刷题

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

1. 项目进度

- [x] 小楼又清风 文章部分排版功能
- [x] 小楼又清风 留言板部分排版功能


## 2021.8.12

1. 文章阅读

- [多人开发的 Git 流程](https://juejin.cn/post/6844903423603638280)

- [多年 Git 使用心得 & 常见问题整理](https://juejin.cn/post/6844904191203213326#heading-36)

2. 源码阅读

3. LeetBook 刷题

4. 项目进度

- [x] UmiJS   
- [x] Hooks
- [x] TAPD

## 2021.8.11

1. 文章阅读

- [git 命令速查](https://jasonandjay.github.io/study/zh/standard/Start.html#git%E7%8E%AF%E5%A2%83)

- [「一劳永逸」一张脑图带你掌握Git命令](https://juejin.cn/post/6869519303864123399)

2. 源码阅读

3. LeetBook 刷题

- [删除排序数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

4. 项目进度

- [x] git clone (url)    克隆
- [x] git branch 分支名    新建分支
- [x] git checkout  分支名    切换分支
- [x] git pull origin  分支名  合并分支
- [x] git push  上传




# 李依萌
## 2021.9.16
1. 文章阅读
-  [ WXML与WXSS](https://juejin.cn/book/6897486502482149376/section/6897515160534319108)
- [ 链接和图片](https://juejin.cn/book/6897486502482149376/section/6897521451906105357)
2. 源码阅读
 
3. leecode 刷题
- [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

4. 项目进度 
- [x] 我的页面排版
- [x] 加入推广功能
- [x] 订单跳转
- [x] 优惠券跳转
## 2021.9.15
1. 文章阅读
-  [Vuex框架原理与源码分析](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)
2. 源码阅读
 
3. leecode 刷题
- [实现 Trie (前缀树)](https://leetcode-cn.com/leetbook/read/top-interview-questions/xaeate/)

4. 项目进度 
- [x] 小程序框架搭建
- [x] 美味大牌优惠排版
- [ ] 美味我的排版
## 2021.9.14
1. 文章阅读
-  [云开发快速入门](https://juejin.cn/book/6897486502482149376/section/6897486502213713935)
- [项目的创建与配置](https://juejin.cn/book/6897486502482149376/section/6897514181193367552)

2. 源码阅读
 
3. leecode 刷题
- [单词拆分](https://leetcode-cn.com/leetbook/read/top-interview-questions/xa503c/)

4. 项目进度 
- [x] 小程序框架搭建
- [x] 小程序项目分配 
## 2021.9.13
1. 文章阅读
-  [云开发资源的管理](https://juejin.cn/book/6897486502482149376/section/6897514477772603407)
- [云函数的配置与部署](https://juejin.cn/book/6897486502482149376/section/6897514180945903631)

2. 源码阅读
 
3. leecode 刷题
- [分割回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions/xaxi62/)

4. 项目进度 
- [x] 八维实训平台
## 2021.9.12
1. 文章阅读
-  [云开发能力体验](https://juejin.cn/book/6897486502482149376/section/6897249500750938125)
- [开始一个云开发项目](https://juejin.cn/book/6897486502482149376/section/6897207289544245255)

2. 源码阅读
 
3. leecode 刷题
- [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions/xah8k6/)

4. 项目进度 
- [x] 八维实训平台
## 2021.9.10
1. 文章阅读
-  [小程序页面开发](https://juejin.cn/book/6897486502482149376/section/6897521362991218696)
- [WXML与WXSS](https://juejin.cn/book/6897486502482149376/section/6897515160534319108)

2. 源码阅读
 
3. leecode 刷题
- [鸡蛋掉落](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmup75/)

4. 项目进度 
- [x] 八维实训平台

## 2021.9.9
1. 文章阅读
-  [链接和图片](https://juejin.cn/book/6897486502482149376/section/6897521451906105357)
- [WeUI](https://juejin.cn/book/6897486502482149376/section/6897429744376086540)

2. 源码阅读
 
3. leecode 刷题
- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmi2l7/)

4. 项目进度 
- [x] 八维实训平台

## 2021.9.8
1. 文章阅读
-  [渐变与动画](https://juejin.cn/book/6897486502482149376/section/6897521652888764419)
- [数据绑定](https://juejin.cn/book/6897486502482149376/section/6897514590091870223)

2. 源码阅读
 
3. leecode 刷题
- [搜索二维矩阵 II](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmlwi1/)

4. 项目进度 
- [x] 八维实训平台

## 2021.9.7
1. 文章阅读
-  [列表渲染](https://juejin.cn/book/6897486502482149376/section/6897521557188149262)
- [条件渲染](https://juejin.cn/book/6897486502482149376/section/6897522081152532487)

2. 源码阅读
 
3. leecode 刷题
- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)

4. 项目进度 
- [x] 八维实训平台

## 2021.9.6
1. 文章阅读
-  [小程序的组件](https://juejin.cn/book/6897486502482149376/section/6897521271580393487)
- [优化与部署上线](https://juejin.cn/book/6897486502482149376/section/6897522166300934151)

2. 源码阅读
 
3. leecode 刷题
- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm0u83/)

4. 项目进度 
- [x] 八维实训平台

## 2021.9.5
1. 文章阅读
-  [后端技术选型](https://juejin.cn/book/6844733825164148744/section/6844733825205927949)
- [开发环境配置](https://juejin.cn/book/6844733825164148744/section/6844733825210122247)

2. 源码阅读
 
3. leecode 刷题
- [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)

4. 项目进度 
- [x] 页面管理 多选框选中 下线 删除 功能 
- [x] 知识小册反选功能
## 2021.9.3
1. 文章阅读
-  [TypeScript 开发基础知识](https://juejin.cn/book/6844733825164148744/section/6844733825205927944)
- [TypeScript 的使用](https://juejin.cn/book/6844733825164148744/section/6844733825205927943)

2. 源码阅读
 
3. leecode 刷题
- [字符串转换整数 (atoi)](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnoilh/)

4. 项目进度 
- [x] 页面管理 多选框选中 下线 删除 功能 
- [x] 知识小册 图片发布/草稿 删除 功能
## 2021.9.2
1. 文章阅读
-  [Egg.js 构建 GraphQL Api 服务](https://juejin.cn/book/6844733825164148744/section/6844733825134624776)
- [GraphQL API 介绍](https://juejin.cn/book/6844733825164148744/section/6844733825201733645)

2. 源码阅读
 
3. leecode 刷题
- [外观数列](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnpvdm/)

4. 项目进度 
- [x] 页面管理优化
- [x] 知识小册优化
## 2021.9.1
1. 文章阅读
-  [Anime.js](https://juejin.cn/book/6844733773792313357/section/6844733773880229901)
- [Anime SVG Morphing 动画](https://juejin.cn/book/6844733773792313357/section/6844733773884424200)

2. 源码阅读
 
3. leecode 刷题
- [有效的字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)

4. 项目进度 
- [x] 海报管理排版
- [x] 知识小册排版
- [x] 组件复用
## 2021.8.31
1. 文章阅读
-  [SVG 描边动画](https://juejin.cn/book/6844733773792313357/section/6844733773876035598)
- [SVG 蒙版动画](https://juejin.cn/book/6844733773792313357/section/6844733773876035591)

2. 源码阅读
 
3. leecode 刷题
- [字符串转换整数 (atoi)](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnoilh/)

4. 项目进度 
- [x] 页面管理排版
- [x] 知识小册排版
- [x] 搜索封装组件 

## 2021.8.30
1. 文章阅读
-  [SVG概述](https://juejin.cn/book/6844733773792313357/section/6844733773859258376)
- [SVG 文件导出与优化](https://juejin.cn/book/6844733773792313357/section/6844733773871857672)

2. 源码阅读
 
3. leecode 刷题
- [旋转图像](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhhkv/)

4. 项目进度 
- [x] 页面管理排版
- [ ] 知识小册排版

## 2021.8.29
1. 文章阅读
-  [高级装饰器](https://juejin.cn/book/6844733813021491207/section/6844733813130526727)
- [Reflect Metadata](https://juejin.cn/book/6844733813021491207/section/6844733813130526733)

2. 源码阅读
 
3. leecode 刷题
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)

4. 项目进度 
- [x] 八维管理后台路由搭建

## 2021.8.27
1. 文章阅读
-  [可辨识联合类型](https://juejin.cn/book/6844733813021491207/section/6844733813126332423)
- [装饰器](https://juejin.cn/book/6844733813021491207/section/6844733813126332424)

2. 源码阅读
 
3. leecode 刷题
- [有效的数独](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2f9gg/)

4. 项目进度 
- [x] 项目上线
- [ ] 项目优化

## 2021.8.26
1. 文章阅读
-  [类型兼容性](https://juejin.cn/book/6844733813021491207/section/6844733813126332430)
- [高级类型之交叉类型联合类型类型别名](https://juejin.cn/book/6844733813021491207/section/6844733813126348814)
2. 源码阅读
 
3. leecode 刷题
- [最长公共前缀](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnmav1/)

4. 项目进度 
- [x] 项目上线
- [ ] 项目优化

## 2021.8.25
1. 文章阅读
-  [泛型（generic）的妙用](https://juejin.cn/book/6844733813021491207/section/6844733813122154504)
- [类型断言与类型守卫](https://juejin.cn/book/6844733813021491207/section/6844733813122138119)
2. 源码阅读
 
3. leecode 刷题
- [两个数组的交集 II](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/)

4. 项目进度 
- [x] 项目上线
- [ ] 项目优化

## 2021.8.24
1. 文章阅读
-  [类(Class)](https://juejin.cn/book/6844733813021491207/section/6844733813117943821))
- [函数(Function)](https://juejin.cn/book/6844733813021491207/section/6844733813122138120)
2. 源码阅读
 
3. leecode 刷题
- [两数之和](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2jrse/)

4. 项目进度 
- [x] 项目上线
- [ ] 项目优化

## 2021.8.23
1. 文章阅读
-  [Typescript 中其他常见类型](https://juejin.cn/book/6844733813021491207/section/6844733813113765902)
- [接口(interface)](https://juejin.cn/book/6844733813021491207/section/6844733813117943816)
2. 源码阅读
 
3. leecode 刷题
- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

4. 项目进度 
- [x] 头部排版
- [x] 响应式布局

## 2021.8.22
1. 文章阅读
-  [深入浅出TypeScript：从基础知识到类型编程](https://juejin.cn/book/6844733813021491207/section/6844733813113765902)
- [TypeScript 与 React 实战(组件篇下)](https://juejin.cn/book/6844733813021491207/section/6844733813134721032)
2. 源码阅读
 
3. leecode 刷题
- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)

4. 项目进度 
- [x] 关于页面 夜间排版

## 2021.8.20
1. 文章阅读
-  [Drawer 篇 - Drawer 常识](https://juejin.cn/book/6844733783166418958/section/6844733783220961294)

2. 源码阅读
 
3. leecode 刷题
- [位1的个数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn1m0i/)

4. 项目进度 
- [x] 关于页面 发布留言功能
- [x] 楼侧右侧排版
- [x] 优化排版

## 2021.8.19
1. 文章阅读
-  [元素面板篇 - 颜色选择器](https://juejin.cn/book/6844733783166418958/section/6844733783220944909)

2. 源码阅读
 
3. leecode 刷题
- [实现 strStr()](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnr003/)

4. 项目进度 
- [x] 关于页面 发布留言功能
- [x] 模糊搜索功能
- [x] 精细排版

## 2021.8.18
1. 文章阅读
-  [技巧集合](https://juejin.cn/book/6844733783166418958/section/6844733783216766989)

2. 源码阅读
 
3. leecode 刷题
- [寻找重复数](https://leetcode-cn.com/leetbook/read/top-interview-questions/xabtn6/)

4. 项目进度 
- [ ] 关于页面 发布留言功能
- [x] 关于页面 回复功能
- [x] 关于页面 优化排版
- [x] 关于页面 跳详情功能
- [x] 关于页面鼠标滑过加高亮

## 2021.8.17
1. 文章阅读
-  [Network 篇 - Network 的骚操作](https://juejin.cn/book/6844733783166418958/section/6844733783216750605)

2. 源码阅读
 
3. leecode 刷题
- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)
- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm0u83/)

4. 项目进度 
- [ ] 关于页面 发布留言功能
- [x] 关于页面 回复功能
- [x] 关于页面 优化排版

## 2021.8.16

1. 文章阅读
- [自定义格式转换器](https://juejin.cn/book/6844733783166418958/section/6844733783212589063)
- [对象&方法](https://juejin.cn/book/6844733783166418958/section/6844733783216750600) 
- [Typescript 的原始类型](https://juejin.cn/book/6844733813021491207/section/6844733813113749518)

2. 源码阅读

3. LeetBook 刷题
- [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)
- [字符串中的第一个唯一字符](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/)

4. 项目进度
- [x] 小楼又清新 关于页面排版
- [x] 小楼又清新 关于页面部分功能
- [x] 小组合并
- [x] 封装推荐阅读组件

## 2021.8.15

1. 文章阅读
- [深入浅出TypeScript：从基础知识到类型编程](https://juejin.cn/book/6844733813021491207/section/6844733813088583687)
- [到底为什么要学习 TypeScript？](https://juejin.cn/book/6844733813021491207/section/6844733813109555207) 
- [console 篇 - console.log 的 "bug" ?](https://juejin.cn/book/6844733783166418958/section/6844733783208361998)

2. 源码阅读

3. LeetBook 刷题
- [计数质数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnzlu6/)
- [3的幂](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnsdi2/)

4. 项目进度
- [x] 小楼又清新 关于页面渲染
- [ ] 小楼又清新 关于页面分页
- [x] 小组合并

## 2021.8.13

1. 文章阅读
- [umj开发技巧](https://juejin.cn/post/6844904198329335822)
- [通用篇 - copying & saving](https://juejin.cn/book/6844733783166418958/section/6844733783204167693)

2. 源码阅读

3. LeetBook 刷题
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- [整数反转](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

1. 项目进度
- [x] 小楼又清风 框架搭建
- [x] 小楼又清风 关于部分排版

## 2021.8.12

1. 文章阅读
- [Egg.js 构建 GraphQL Api 服务](https://juejin.cn/book/6844733825164148744/section/6844733825134624776)
- [使用 Command](https://juejin.cn/book/6844733783166418958/section/6844733783204167688)

2. 源码阅读

3. leetcode 刷题
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)
- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)

4. 项目进度
- [x] git分支
- [x] TAPD分配项目
- [x] UmiJS创建项目
- [x] Hooks

## 2021.8.11

1. leetcode刷题：
- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)
3. leetcode 刷题
- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

- [只出现一次的数字](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/)

4. 项目进度

- [x] git分支
- [x] TAPD分配项目
- [x] UmiJS创建项目
- [x] Hooks

# 刘朋

## 2021.9.21

1.leetcode刷题：

- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

2.阅读

- [面试题]

3.源码阅读

- [vuex源码](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)

4.项目

- [x] 美味不用等

## 2021.9.19

1.leetCode刷题

- [整数反转](https://leetcode-cn.com/problems/reverse-integer/)
   
- [两数之和](https://leetcode-cn.com/problems/two-sum/)

2.阅读

- [面试题]

3.源码阅读

- [vuex源码](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)

4.项目

## 2021.9.17

1. leetCode刷题

- [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

- [反转链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnhm6/)

2. 阅读

- [面试题]

3. 源码阅读

- [vuex源码](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)

4.项目进度

- [x] 微信小程序 tab切换+渲染

## 2021.9.16

1.leetCode刷题

- [3的幂数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnsdi2/)

- [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)

2.阅读

- [面试题]

3.源码阅读

- [vuex源码](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)

4.项目进度

- [x]微信小程序 获取数据渲染+排版

## 2021.9.15

1.leetCode刷题

- [3的幂数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnsdi2/)

- [反转链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnhm6/)

2.阅读

- [面试题] 

3.源码阅读

- [vuex源码](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)

4.项目进度

- [x] 美味不等人  路由排版

## 2021.9.14

1.leetCode刷题

- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)

- [计数指数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnzlu6/)

2.阅读

- [面试题]

3.源码阅读

- [vuex源码](https://tech.meituan.com/2017/04/27/vuex-code-analysis.html?utm_source=pocket_mylist)

4.项目进度

- [x]下载抓包工具

- [x]学会应用抓包工具

## 2021.9.13

1.leetCode刷题

- [3的幂数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnsdi2/)

- [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)

2.阅读

3.源码阅读

4.项目进度

- [x] 微信小程序搭建 了解基本用法

## 2021.9.12

1.leetCode刷题

- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)

- [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

2.阅读

- [面试题]

3.源码阅读

4.项目进度

- [x] 小程序的搭建

## 2021.9.10

1.leetCode刷题

- [计数指数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnzlu6/)

- [3的幂数](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnsdi2/)

2.阅读

-  [面试题]

3.源码阅读

4.项目进度

- [x] 微信小程序 

## 2021.9.9

1. leetCode刷题

- [删除链表的倒数第N个节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn2925/)

- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)

2. 阅读

面试题

3. 源码阅读

4. 项目进度

- [x] 支付

## 2021.9.8

1. leetCode刷题

- [删除链表中的节点](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnarn7/)

- [反转链表](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnhm6/)

2. 阅读

- [面试题]

3. 源码阅读

4. 项目进度

- [x] 邮件渲染

## 2021.9.5

1. leetCode刷题

- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)

2. 阅读

- [面试题]

3. 源码阅读

4. 项目进度

- [x] 新建文章

## 2021.9.3

1.leetcode刷题：

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmi2l7/)

- [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions/xah8k6/)

2.阅读

- [面试题]

3.源码阅读

4.项目进度

- [x] 新建文章跳转 排版

- [x] 新建文章-协同编辑器 跳转排版

## 2021.9.2

1.leetcode刷题：

- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

2.阅读

- [前后端交互](https://jasonandjay.github.io/study/zh/standard/Cooperation.html#_1-%E5%89%8D%E7%AB%AF%E8%AF%B7%E6%B1%82%E6%95%B0%E6%8D%AEurl%E7%94%B1%E8%B0%81%E6%9D%A5%E5%86%99)

3.源码阅读

4.项目进度

- [x] 新建文章

- [x] 新建页面

## 2021.9.1

1.leetCode

- [只出现一次的数](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm0u83/)

- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)

2.阅读

- [前后端交互](https://jasonandjay.github.io/study/zh/standard/Cooperation.html#_1-%E5%89%8D%E7%AB%AF%E8%AF%B7%E6%B1%82%E6%95%B0%E6%8D%AEurl%E7%94%B1%E8%B0%81%E6%9D%A5%E5%86%99)

3.源码阅读

4.项目进度

- [x] 删除功能

- [x] 搜索功能

## 2021.8.31

1.leetcode刷题：

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmi2l7/)

- [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions/xah8k6/)

2.阅读

- [前后端交互](https://jasonandjay.github.io/study/zh/standard/Cooperation.html#_1-%E5%89%8D%E7%AB%AF%E8%AF%B7%E6%B1%82%E6%95%B0%E6%8D%AEurl%E7%94%B1%E8%B0%81%E6%9D%A5%E5%86%99)

3.源码阅读

4.项目进度

- [x] 获取数据

- [x] 优化表格排版


## 2021.8.30

1.leetcode刷题：

- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

2.阅读

- [单页面应用](https://jasonandjay.github.io/study/zh/standard/Spa.html)

3.源码阅读

4.项目进度

- [x] 后台管理 排版

## 2021.8.29

1.leetCode刷题

- [乘积最大子数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmk3rv/)

- [两个数组的交集2](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmcbym/)

2.阅读

- [项目流程](https://jasonandjay.github.io/study/zh/standard/Project.html#%E5%9B%BE%E8%A7%A3)

3.源码阅读

4.项目进度

- [x] 管理后台 拉取代码

- [x] 管理后台 渲染页面

## 2021.8.27

1.leetCode刷题

- [合并两个有序数组](https://leetcode-cn.com/leetbook/read/top-interview-questions/xmi2l7/)

- [验证回文串](https://leetcode-cn.com/leetbook/read/top-interview-questions/xah8k6/)

2.阅读

- [项目流程](https://jasonandjay.github.io/study/zh/standard/Project.html#%E5%9B%BE%E8%A7%A3)

3.源码阅读

4.项目进度

- [x] 埋点 统计

- [x] 路由懒加载

## 2021.8.26

1.leetcode刷题

- [只出现一次的数](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm0u83/)

- [多数元素](https://leetcode-cn.com/leetbook/read/top-interview-questions/xm77tm/)

2.阅读

- [单页面应用](https://jasonandjay.github.io/study/zh/standard/Spa.html)

3.源码阅读

4.项目进度

- [x] hash简约路由

## 2021.8.25

1.leetcode刷题

- [找不同](https://leetcode-cn.com/problems/find-the-difference/submissions/) 

2.阅读

- [浏览器缓存](https://jasonandjay.github.io/study/zh/standard/Cache.html)

3.源码阅读

4.项目进度

- [x] 小楼又清风 打包上传

- [x] 小楼又清风 百度统计


## 2021.8.24

1.leetcode刷题

- [删除数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

2.阅读

- [前端兼容问题](https://jasonandjay.github.io/study/zh/standard/Compatibility.html#%E5%A6%82%E4%BD%95%E5%A4%84%E7%90%86%E5%85%BC%E5%AE%B9%E9%97%AE%E9%A2%98)

3.源码阅读

4.项目进度

- [x] 小楼又清风 头部样式

- [x] 小楼又清风 详情分享

## 2021.8.23

1.leetcode刷题

- [搜索插入的位置](https://leetcode-cn.com/problems/search-insert-position/)

- [回文数](https://leetcode-cn.com/problems/palindrome-number/)

2.阅读

- [前端兼容问题](https://jasonandjay.github.io/study/zh/standard/Compatibility.html#%E5%A6%82%E4%BD%95%E5%A4%84%E7%90%86%E5%85%BC%E5%AE%B9%E9%97%AE%E9%A2%98)

3.源码阅读

4.项目进度

- [x] 小楼又清风 完全样式

- [x] 小楼又清风 点赞排版

- [x] 小楼又清风 合并代码

## 2021.8.22

1.leetcode刷题 

- [移除元素](https://leetcode-cn.com/problems/remove-element/)

2.阅读

- [自定义格式转换器](https://juejin.cn/book/6844733783166418958/section/6844733783212589063)

- [转换&格式](https://juejin.cn/book/6844733783166418958/section/6844733783216750600)

3.源码阅读

4.项目进度

- [x] 小楼又清风 海报

- [x] 小楼又清风 合并代码


## 2021.8.20

1.leetcode刷题 

- [删除数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

2.文章阅读

- [前端术语-W3C](https://jasonandjay.github.io/study/zh/book/%E5%89%8D%E7%AB%AF%E6%9C%AF%E8%AF%AD%E7%AF%87.html)

- [前端术语-SPA](https://jasonandjay.github.io/study/zh/book/%E5%89%8D%E7%AB%AF%E6%9C%AF%E8%AF%AD%E7%AF%87.html
)
3.源码阅读

4.项目进度

- [x] 小楼又清风 约定式路由

- [x] 小楼又清风 知识小册小目录详情

## 2021.8.19

1.leetcode刷题 
- [删除数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

- [两数之和](https://leetcode-cn.com/problems/two-sum/)

2.文章阅读
- [前端术语-Hack](https://jasonandjay.github.io/study/zh/book/%E5%89%8D%E7%AB%AF%E6%9C%AF%E8%AF%AD%E7%AF%87.html)

- [前端术语-MVC](https://jasonandjay.github.io/study/zh/book/%E5%89%8D%E7%AB%AF%E6%9C%AF%E8%AF%AD%E7%AF%87.html)

3.源码阅读

4.项目进度

- [x]小楼又清风 小册第一层详情

- [x]小楼又清风 小册第二层详情渲染

## 2021.8.18

1.leetcode刷题

- [有效字母异位词](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/)

- [验证回文字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/)

2.文章阅读

- [前端术语-W3C](https://jasonandjay.github.io/study/zh/book/%E5%89%8D%E7%AB%AF%E6%9C%AF%E8%AF%AD%E7%AF%87.html)

- [前端术语-SPA](https://jasonandjay.github.io/study/zh/book/%E5%89%8D%E7%AB%AF%E6%9C%AF%E8%AF%AD%E7%AF%87.html)

3.源码阅读

4.项目进度

- [X] 小楼又清风 明暗主题切换

- [x] 小楼又清风 详情渲染

## 2021.8.17

1.leetcode刷题

- [删除数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)
- [3的幂](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnsdi2/)

2.文章阅读

- [自定义格式转换器](https://juejin.cn/book/6844733783166418958/section/6844733783212589063)
- [UmiJS开发技巧](https://juejin.cn/post/6844904198329335822)
- [跨域问题](https://jasonandjay.github.io/study/zh/book/Ajax%E7%AF%87.html) 

3.源码阅读

4.项目进度

- [x] 小楼又清风 右侧卡片布局+高亮

- [x] 小楼又清风 整体合并效果

- [ ] 小楼又清风 跳详情

## 2021.8.16

1.leetcode刷题

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

- [字符串中第一个唯一字符](https://leetcode-cn.com/problems/first-unique-character-in-a-string/submissions/)

- [找不同](https://leetcode-cn.com/problems/find-the-difference/submissions/) 

2.阅读
- [异步的console.log](https://juejin.cn/book/6844733783166418958/section/6844733783212556302)
3.源码阅读

4.项目进度
- [x] 小楼又清风 知识小册渲染
- [ ] 小楼又清风 跳详情
- [x] 小楼又清风 小组合代码 
## 2021.8.15

1.leetcode刷题

2.阅读

-[console.log(忍者打印)](https://juejin.cn/book/6844733783166418958/section/6844733783212572686)

3.源码阅读

4.项目进度

- [X] 小楼又清风

- [x] 和代码

## 2021.8.13

1.leetcode刷题：

- [移动零](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/)

- [反转字符串](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnhbqj/)

2.文章阅读

- 掘金 小册-你不知道的 Chrome 调试技巧

- [console.log](https://juejin.cn/book/6844733783166418958/section/6844733783208361998)

3.源码阅读

4.项目进度 

- [小楼又清风]

- [x] 知识小册渲染

- [ ] 样式的学习

- [ ] 年月日


## 2021.8.12

 1.leetcode刷题：

- [删除数组中的重复项](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/)

- [存在重复元素](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/)

- [加一](https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2cv1c/)

 2. 文章阅读

- 掘金 小册-你不知道的 Chrome 调试技巧

- [1.写在前面](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
    
- [2.通用篇-copying&saving](https://juejin.cn/book/6844733783166418958/section/6844733783204167693)
   
- [3.通用篇-快捷键和通用技巧](https://juejin.cn/book/6844733783166418958/section/6844733783204167687)

3. 源码阅读

4. 项目进度

- [x] git分支

- [x] TAPD分配项目

- [x] UmiJS创建项目

- [x] Hooks

## 2021.8.11

1.leetcode刷题：

- [回文数](https://leetcode-cn.com/problems/palindrome-number/submissions/)
   
- [整数反转](https://leetcode-cn.com/problems/reverse-integer/)
   
- [两数之和](https://leetcode-cn.com/problems/two-sum/)

2.文章阅读

- 掘金 小册-你不知道的 Chrome 调试技巧

- [1.写在前面](https://juejin.cn/book/6844733783166418958/section/6844733783187390477)
    
- [2.通用篇-copying&saving](https://juejin.cn/book/6844733783166418958/section/6844733783204167693)
   
- [3.通用篇-快捷键和通用技巧](https://juejin.cn/book/6844733783166418958/section/6844733783204167687)

3.源码阅读

4. 项目进度

- [ ] umi+ts

- [x] 项目流程

- [x] TABD 

