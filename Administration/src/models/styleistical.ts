
import { getStyle,deteleStyle } from "@/services";
import { IStyle } from "@/types/styleistical"
import { makeAutoObservable } from "mobx"
import {message} from "antd"
class styleistical {
    // 定义变量
    stylelist:IStyle[]=[];
//   deleteStyle: any;
    constructor(){
        makeAutoObservable(this)
    }
    //获取搜索列表
    async getStyle(page:number,params={}){
        let result=await getStyle(page,params)
        console.log(result);
        if(result.data){
            this.stylelist=result.data[0]
        }    
    }
    // 删除数据
    async deteleStyle(id: string[]){
        message.loading('操作中');
        Promise.all(id.map(id=>deteleStyle(id)))
        .then(res=>{           
            message.destroy();
            message.success('批量删除成功');
            this.getStyle(1)
            
        })
        .catch(err=>{
            message.success('批量删除成功');
            
        })
    }
}

export default styleistical

