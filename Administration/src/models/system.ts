
import { getSetting, getSystem } from '@/services'
import { SystemItem } from '@/types'
import { makeAutoObservable, runInAction } from 'mobx'
class System {
    SystemDate: Partial<SystemItem> = {}
    constructor() {
        makeAutoObservable(this)
    }
    //系统设置
    async getSystem() {
        let res = await getSystem()
        console.log(res);
        if (res.data) {
            runInAction(() => {
                this.SystemDate = res.data
            })
        }
    }

    //更改系统设置
    async getSetting(values: Partial<SystemItem>) {
        let res = await getSetting(values)
        console.log(res);
        return res
    }

}
export default System