import { login } from '@/services';
import { ILoginForm, IUserInfo } from '@/types';
import { removeToken } from '@/utils';
import { makeAutoObservable } from 'mobx';

class User {
    isLogin = false;
    userInfo: Partial<IUserInfo> = { };
    static Search: any;
    static user: any;
    constructor() {
        makeAutoObservable(this);
    }

    async login(data: ILoginForm) {
        let result = await login(data);
        console.log('login result...', result);
        localStorage.setItem('user',JSON.stringify(result))
        if (result.data) {
            this.isLogin = true;
            this.userInfo = result.data;
        }
        return result.data;
    }
    logout() {
        this.isLogin = false;
        this.userInfo = { };
        removeToken();
    }

}

export default User;
