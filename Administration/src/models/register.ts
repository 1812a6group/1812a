import { register } from '@/services';
import { IRegisterForm } from '@/types';
import { makeAutoObservable } from 'mobx';
//注册
class Register {
    constructor() {
        makeAutoObservable(this);
    }

    async register(data: IRegisterForm) {
        let result = await register(data);
        return result;
    }

}

export default Register;
