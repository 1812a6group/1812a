
import { getManage, getManageId } from '@/services'
import { ManageItem } from '@/types'
import { makeAutoObservable, runInAction } from 'mobx'
class Manage {
    ManageDate: ManageItem[] = []
    ManageId: Partial<ManageItem> = {}
    constructor() {
        makeAutoObservable(this)
    }
    //用户管理
    async getManage(page: number, params = {}) {
        let res = await getManage(page, params)
        console.log(res.data[0]);
        if (res.data) {
            runInAction(() => {
                this.ManageDate = res.data[0]
            })
        }
    }
    //用户管理状态
    async getManageId(value: Partial<ManageItem> = {}) {
        let res = await getManageId(value)
        console.log(res);
        if (res.data) {
            runInAction(() => {
                this.ManageId = res.data
            })
        }

    }
}
export default Manage