import { getsearch,deleteSearch } from "@/services"
import { ISearch } from "@/types/search"
import { makeAutoObservable, runInAction } from "mobx"
import {message} from 'antd';
class Search {
    Sdata:ISearch[]=[];
    searchcount=1;
    constructor(){
        makeAutoObservable(this)
    }
    // 获取搜索列表
    async getsearch(page:number,params={}){
        let res=await getsearch(page,params)
        console.log(res); 
        if(res.data){
            runInAction(()=>{
                this.Sdata=res.data[0];
                this.searchcount=res.data[1]
            })
        }   
    }

    // 删除数据

    async deleteSearch(id: string[]){
        message.loading('操作中');
        Promise.all(id.map(id=>deleteSearch(id)))
        .then(res=>{           
            message.destroy();
            message.success('批量删除成功');
            this.getsearch(1)
            
        })
        .catch(err=>{
            message.success('批量删除成功');
            
        })
    }
}

export default Search

