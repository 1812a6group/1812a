import {
  addCategory,
  delCategory,
  editCategory,
  getArticle,
  getCategory,
  addTag,
  delTag,
  editTag,
  getTag,
  editRecommended,
  getView,
  delArticle,
  publishArticle,
  drafthArticle,
  sRecommended,
  fRecommended,
  publish,
} from '@/services';
import { IArticle, ICategory, ITag } from '@/types';
import { makeAutoObservable, runInAction } from 'mobx';
class Article {
  // 最新文章
  data: IArticle[] = [];
  // 所有分类
  CategoryData: ICategory[] = [];
  // 所有标签
  TagData: ITag[] = [];
  constructor() {
    makeAutoObservable(this);
  }

   // 发布文章
   async publish(data: Partial<IArticle>){
    let result = await publish(data);
    return result.data;
}

  // 最新文章
  async getArticle() {
    let res = await getArticle();
    if (res.data) {
      runInAction(() => {
        this.data = res.data[0];
      });
    }
  }
  // 发布文章
  async drafthArticle(id: string) {
    await drafthArticle(id);
    this.getArticle();
  }
  // 草稿文章
  async publishArticle(id: string) {
    await publishArticle(id);
    this.getArticle();
  }
  // 删除文章
  async delArticle(id: string) {
    await delArticle(id);
    this.getArticle();
  }
  // 查看访问推荐
  async getView(id: string) {
    await getView(id);
  }

  // 改变首焦推荐
  async editRecommended(id: string, isRecommended: boolean) {
    await editRecommended(id, isRecommended);
    this.getArticle();
  }

  // 首焦推荐
  async sRecommended(id: string) {
    await sRecommended(id);
    this.getArticle();
  }

  // 撤销首焦
  async fRecommended(id: string) {
    await fRecommended(id);
    this.getArticle();
  }

  // 所有分类
  async getCategory() {
    let res = await getCategory();
    if (res.data) {
      runInAction(() => {
        this.CategoryData = res.data;
      });
    }
  }

  // 添加分类
  async addCategory(label: string, value: string) {
    await addCategory(label, value);
    this.getCategory();
  }

  // 修改分类
  async editCategory(id: string, label: string, value: string) {
    await editCategory(id, label, value);
    this.getCategory();
  }

  // 删除分类
  async delCategory(id: string) {
    await delCategory(id);
    this.getCategory();
  }

  // 所有标签
  async getTag() {
    let res = await getTag();
    if (res.data) {
      runInAction(() => {
        this.TagData = res.data;
      });
    }
  }

  // 添加标签
  async addTag(label: string, value: string) {
    await addTag(label, value);
    this.getTag();
  }

  // 修改标签
  async editTag(id: string, label: string, value: string) {
    await editTag(id, label, value);
    this.getTag();
  }

  // 删除标签
  async delTag(id: string) {
    await delTag(id);
    this.getTag();
  }
}
export default Article;
