import { PageManagement } from '@/types';
import { getPageManageMent, getSearchPage, getDeletePage, getPublish } from '@/services';
import { makeAutoObservable, runInAction } from 'mobx';
import { message } from 'antd';
class Page {
    pageList: PageManagement[] = [];
    count = 1;
    constructor() {
        makeAutoObservable(this);
    }
    async getPageManageMent(page = 1) {
        let res = await getPageManageMent(1);
        if (res.data) {
            runInAction(() => {
                this.pageList = res.data[0];
                this.count = res.data[1];
            })
        }
    }
    async getSearchPage(value: PageManagement) {
        let res = await getSearchPage(value);
        if (res.statusCode === 200) {
            runInAction(() => {
                this.pageList = res.data[0];
                this.count = res.data[1];
            })
        }
    }
    async getDeletePage(ids: string[]) {
        message.loading('操作中');
        Promise.all(ids.map(id => getDeletePage(id)))
            .then(res => {
                message.destroy();
                message.success('批量删除成功');
                this.getPageManageMent();
            })
            .catch(err => {
                message.success('批量删除成功');
                this.getPageManageMent();
            })
    }
    async getPublish(ids: string[], status: string) {
        message.loading('操作中');
        Promise.all(ids.map(id => getPublish(id, status)))
            .then(res => {
                message.destroy();
                message.success('操作成功');
                this.getPageManageMent();
            })
            .catch(err => {
                message.success('操作成功');
                this.getPageManageMent();
            })
    }
}

export default Page;

