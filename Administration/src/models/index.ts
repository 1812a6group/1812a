import Comments from './comments';
import User from './user';
import Page from './pagemanagement';
import Know from './knowledge';
import Search from "./search";
import Styleistical from "./styleistical";
import Manage from './manage';
import Register from './register';
import Article from './article';
import System from './system';
import tag from "./tag";
export default {
    Comments: new Comments,
    user: new User,
    Page: new Page,
    Know: new Know,
    Search: new Search,
    Styleistical: new Styleistical,
    Manage: new Manage,
    Article: new Article,
    tag: new tag,
    Register: new Register,
    System: new System,
}

