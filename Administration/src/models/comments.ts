import { agree, delComment, delMail, getComment, getFile, getMail } from '@/services'
import { IComments, IFile } from '@/types/comments'
import { makeAutoObservable, runInAction } from 'mobx'
import { message } from 'antd'
class Comments {
    CommentDate: IComments[] = []
    count = 0
    FileData: IFile[] = []
    FileCount = 1
    MailData = []
    MailCount = 0
    constructor() {
        makeAutoObservable(this)
    }
    //获取评论数据
    async getComment(page: number, value = {}) {
        let res = await getComment(page, value)
        if (res.data) {
            runInAction(() => {
                this.CommentDate = res.data[0]
                this.count = res.data[1]
            })
        }
    }
    //删除评论数据
    async delComment(ids: string[]) {
        message.loading('操作中')
        Promise.all(ids.map(id => delComment(id))).then(res => {
            message.destroy();
            message.success('删除成功');
            this.getComment(1)
        }).catch(err => {
            message.success('删除成功');
            this.getComment(1);
        })
    }
    //评论的通过 拒绝
    async agree(ids: string[], value: boolean) {
        Promise.all(ids.map(id => agree(id, value))).then(res => {
            if (value) {
                message.success('评论已通过')
                this.getComment(1)
            } else {
                message.success('评论已拒绝')
                this.getComment(1)
            }
        }).catch(err => {
            message.success('操作失败')
        })
    }
    //文件管理数据
    async getFile(page = 1, value = {}, pageSize: number) {
        let res = await getFile(page, value, pageSize)
        if (res.data) {
            runInAction(() => {
                this.FileData = res.data[0]
                this.FileCount = res.data[1]
            })
        }
    }
    //邮件管理数据
    async getMail(page = 1, value = {}) {
        let res = await getMail(page, value)
        if (res.data) {
            runInAction(() => {
                this.MailData = res.data[0]
                this.MailCount = res.data[1]
            })
        }
    }
    async delMail(ids: string[]) {
        message.loading('操作中')
        Promise.all(ids.map(id => delMail(id))).then(res => {
            message.destroy();
            message.success('删除成功');
            this.getMail(1)
        }).catch(err => {
            message.success('删除成功');
            this.getMail(1);
        })
    }
}
export default Comments