import { KnewledgeItem } from '@/types';
import { getKnowledge, getKnowDelete, getKnowpublish, getKnowStatus, getKnowNewFile } from '@/services';
import { makeAutoObservable, runInAction } from 'mobx';
import { message } from 'antd';

class Know {
    knowList: KnewledgeItem[] = [];
    knowtotal = 0;
    constructor() {
        makeAutoObservable(this);
    }
    async getKnowledge(page = 1) {
        let res = await getKnowledge(1);
        if (res.data) {
            runInAction(() => {
                this.knowList = res.data[0];
                this.knowtotal = res.data[1];
            })
        }
    }
    async getKnowDelete(id: string) {
        let res = await getKnowDelete(id);
        if (res.data) {
            message.success('删除成功');
            this.getKnowledge();
        } else {
            message.error('删除失败');
        }
    }
    async getKnowpublish(id: string, status: string) {
        let res = await getKnowpublish(id, status);
        if (res.data) {
            this.getKnowledge()
        }
    }
    async getKnowStatus(value: KnewledgeItem) {
        let res = await getKnowStatus(value);
        if (res.data) {
            runInAction(() => {
                this.knowList = res.data[0];
                this.knowtotal = res.data[1];
            })
        }
    }
    async getKnowNewFile(value: KnewledgeItem) {
        let res = await getKnowNewFile(value);
        if (res.data) {
            message.success('创建成功')
            this.getKnowledge()
        }
    }
}
export default Know;