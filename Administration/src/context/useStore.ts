import { useContext } from 'react';
import StateContext from './stateContext'

export default function () {
    return useContext(StateContext);
}
