import React from 'react';
import { Form, Input, Button, Select } from 'antd';
import styles from './index.less';
import { PageState, PageInput, PageStatus } from '@/types/search';
interface Props {
    pageInput: PageInput[],
    pageState?: PageState[],
    SearchBtn(value: any): void,
    pageStatus?: PageStatus
}
const Search: React.FC<Props> = (props) => {
    const { Option } = Select;
    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };
    const [form] = Form.useForm();

    const onGenderChange = (value: string) => {
    };

    const onFinish = (values: any) => {

        props.SearchBtn(values)
    };

    const onReset = () => {
        form.resetFields();
    };

    return (
        <div className={styles.page_search}>
            <Form {...layout} form={form} name="control-hooks" onFinish={onFinish} >
                <div className={styles.page_inp}>
                    {
                        props.pageInput.map(item => {
                            return <Form.Item name={item.name} label={item.label} key={item.name} className={styles.searchinput}>
                                <Input placeholder={item.placeholder} />
                            </Form.Item>
                        })
                    }
                    {
                        props.pageState && <Form.Item name={props.pageStatus!.statusname} label="状态">
                            <Select
                                onChange={onGenderChange}
                                allowClear
                                className={styles.search_select}
                            >
                                {
                                    props.pageState.map(item => {
                                        return <Option value={item.pass} key={item.state}>{item.state}</Option>
                                    })
                                }
                            </Select>
                        </Form.Item>
                    }

                </div>
                <Form.Item
                    noStyle
                    shouldUpdate={(prevValues, currentValues) => prevValues.gender !== currentValues.gender}
                >
                    {({ getFieldValue }) =>
                        getFieldValue('gender') === 'other' ? (
                            <Form.Item name="customizeGender" label="Customize Gender" rules={[{ required: true }]}>
                                <Input />
                            </Form.Item>
                        ) : null
                    }
                </Form.Item>
                <div className={styles.page_btn}>
                    <Button type="primary" htmlType="submit">
                        搜索
                    </Button>
                    <Button htmlType="button" onClick={onReset}>
                        重置
                    </Button>
                </div>
            </Form>
        </div>
    )
}
export default Search;