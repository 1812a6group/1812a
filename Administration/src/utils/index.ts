import Cookie from 'js-cookie';

const key = 'authorization';
// 设置登录态yarn
export function setToken(value: string) {
    let now = +new Date();
    let expires = new Date(now + 24 * 60 * 60 * 1000);
    Cookie.set(key, value, { expires });
}

// 获取登陆态
export function getToken() {
    return 'Bearer ' + Cookie.get(key);
}

// 移除登陆态
export function removeToken() {
    Cookie.remove(key);
}
