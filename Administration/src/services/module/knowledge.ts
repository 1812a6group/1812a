
import { KnewledgeItem } from "@/types";
import { request } from "umi";
//获取数据
export function getKnowledge(page = 1, pageSize = 12) {
    return request('api/knowledge', {
        params: {
            page,
            pageSize
        }
    })
}
//知识小册删除
export function getKnowDelete(id: string) {
    return request(`api/knowledge/${id}`, {
        method: 'DELETE'
    })
}
//知识小册发布/下线
export function getKnowpublish(id: string, status: string) {
    return request(`api/knowledge/${id}`, {
        method: 'PATCH',
        data: {
            status
        },
    })
}
//知识小册搜索
export function getKnowStatus(value: KnewledgeItem) {
    return request('api/knowledge', {
        params: value
    })
}
//知识小册新建
export function getKnowNewFile(data: Partial<KnewledgeItem> = {}) {
    return request('api/knowledge/book', {
        method: 'POST',
        data
    })
}