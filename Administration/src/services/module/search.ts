import { ISearch } from "@/types";
import { request } from "umi";

export function getsearch(page:number,params:Partial<ISearch>={},pageSize=12,){
    return request(`api/search?page=${page}&pageSize=${pageSize}`,{params})
}

export function deleteSearch(id: string){
    return request(`api/search/${id}`, {
        method: 'DELETE'
    })
}


