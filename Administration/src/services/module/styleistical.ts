import { IStyle } from "@/types/styleistical";
import { request } from "umi";

export function getStyle(page:number,params:Partial<IStyle>={},pageSize=12){
    return request(`api/view?page=${page}&pageSize=${pageSize}`,{params})
}

// api/view/f691ed86-c822-4765-96e7-6446fb16457b
export function deteleStyle(id: string){
    return request(`api/view/${id}`, {
        method: 'DELETE'
    })
}