import { ManageItem } from '@/types';
import { request } from 'umi';

//用户管理数据
export function getManage(page = 1, params: Partial<ManageItem> = {}, pageSize = 12) {
    return request(`api/user?page=${page}&pageSize=${pageSize}`, { params })
}

//更改状态
export function getManageId(value: Partial<ManageItem> = {}) {
    return request("api/user/update",
        {
            method: "POST",
            data: value,
        }
    )
}