import { IArticle } from '@/types/article';
import { request } from 'umi';

// 发布文章
export function publish(data: Partial<IArticle>){
  return request('api/article', {
      method: 'POST',
      data
  });
}

// 获取最新文章
export function getArticle(page = 1, pageSize = 12) {
  return request('api/article', {
    page,
    pageSize,
  });
}
// 发布文章
export function drafthArticle(id: string) {
  return request(`api/article/${id}`, {
    data: {
      status: 'draft',
    },
    method: 'PATCH',
  });
}
// 草稿文章
export function publishArticle(id: string) {
  return request(`api/article/${id}`, {
    data: {
      status: 'publish',
    },
    method: 'PATCH',
  });
}

// 删除文章
export function delArticle(id: string) {
  return request(`api/article/${id}`, {
    method: 'DELETE',
  });
}
// 改变首焦推荐
export function editRecommended(id: string, isRecommended: boolean) {
  return request(`api/article/${id}`, {
    data: {
      isRecommended: isRecommended,
    },
    method: 'PATCH',
  });
}
// 首焦推荐
export function sRecommended(id: string) {
  return request(`api/article/${id}`, {
    data: {
      isRecommended: true,
    },
    method: 'PATCH',
  });
}

// 撤销首焦
export function fRecommended(id: string) {
  return request(`api/article/${id}`, {
    data: {
      isRecommended: false,
    },
    method: 'PATCH',
  });
}

// 查看访问
export function getView(id: string) {
  return request(`api/view/url?url=creationad.shbwyz.co%2Farticle%${id}`, {});
}

// 获取所有分类
export function getCategory() {
  return request('api/category');
}

// 添加分类
export function addCategory(label: string, value: string) {
  return request('api/category', {
    data: {
      label,
      value,
    },
    method: 'post',
  });
}
// 修改分类
export function editCategory(id: string, label: string, value: string) {
  return request(`api/category/${id}`, {
    data: {
      label,
      value,
    },
    method: 'PATCH',
  });
}

//   删除分类
export function delCategory(id: string) {
  return request(`api/category/${id}`, {
    method: 'DELETE',
  });
}

// 获取所有标签
export function getTag() {
  return request('api/tag');
}

// 添加标签
export function addTag(label: string, value: string) {
  return request('api/tag', {
    data: {
      label,
      value,
    },
    method: 'post',
  });
}
// 修改标签
export function editTag(id: string, label: string, value: string) {
  return request(`api/tag/${id}`, {
    data: {
      label,
      value,
    },
    method: 'PATCH',
  });
}

//   删除标签
export function delTag(id: string) {
  return request(`api/tag/${id}`, {
    method: 'DELETE',
  });
}
