import { SystemItem } from "@/types";
import { request } from "umi";

//系统设置
export function getSystem() {
    return request("api/setting/get", {
        method: "POST",
    })
}

//更改内容
export function getSetting(values: Partial<SystemItem>) {
    return request("api/setting", {
        method: "OPTIONS",
        values: values,
    })
}