
import { PageManagement } from '@/types';
import { request } from 'umi';
//页面管理数据
export function getPageManageMent(page: number, pageSize = 12) {
    return request('api/page', {
        params: {
            page,
            pageSize
        }
    });
};
//页面管理搜索
export function getSearchPage(value: PageManagement) {
    return request('api/page', {
        params: value
    });
};
//页面管理删除
export function getDeletePage(id: string) {
    return request(`api/page/${id}`, {
        method: 'DELETE'
    });
};
//页面管理发布/下线
export function getPublish(id: string, status: string) {
    return request(`api/page/${id}`, {
        method: 'PATCH',
        data: {
            status
        }
    })
}