import { ICommentsItem, IFileItem, Ireply } from '@/types/comments'
import { request } from 'umi'
//获取评论管理数据
export function getComment(page = 1, params: Partial<ICommentsItem> = {}, pageSize = 12) {
    return request(`api/comment?page=${page}&pageSize=${pageSize}`, { params })
}
//删除评论数据
export function delComment(id: string) {
    return request(`api/comment/${id}`, {
        method: 'DELETE'
    })
}
//通过 拒绝 评论数据
export function agree(id: string, pass: boolean) {
    return request(`api/comment/${id}`, {
        method: 'PATCH',
        data: {
            pass
        }
    })
}
//获取文件管理数据
export function getFile(page = 1, params: Partial<IFileItem> = {}, pageSize: number) {
    return request(`api/file?page=${page}&pageSize=${pageSize}`, { params })
}
//获取邮件数据
export function getMail(page = 1, params = {}, pageSize = 12) {
    return request(`api/smtp?page=${page}&pageSize=${pageSize}`, {
        params
    })
}
export function delMail(id: string) {
    return request(`api/smtp/${id}`, {
        method: 'DELETE'
    })
}