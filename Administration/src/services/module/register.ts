import { IRegisterForm } from "@/types";
import { request } from "umi";
//注册
export function register(data: IRegisterForm) {
    return request('api/user/register', {
        method: 'POST',
        data
    })
}
