import React, { useEffect, useState, Key } from 'react';
import styles from './index.less';
import Search from '@/components/search';
import { Table, Space, Badge, Button, Drawer, Popconfirm, message, Modal } from 'antd';
import { ReloadOutlined, PlusOutlined } from '@ant-design/icons';
import useStore from '@/context/useStore';
import moment from 'moment';
import { observer } from 'mobx-react-lite';
const pagestate = [{
    state: "已发布",
    pass: 'publish',
}, {
    state: "草稿",
    pass: 'draft',
}]
const pageinput = [{
    name: "name",
    label: "名称",
    placeholder: "请输入页面名称"
}, {
    name: "path",
    label: "路径",
    placeholder: "请输入页面路径"
}]
const pageStatus = { statusname: 'status' }

const PageManagement: React.FC = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const store = useStore();
    const [page, setpage] = useState(1);
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])
    useEffect(() => {
        store.Page.getPageManageMent(page);
    }, [page]);
    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    function SearchBtn(value: any) {
        store.Page.getSearchPage(value);
    };
    const columns = [
        {
            title: '名称',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '路径',
            dataIndex: 'path',
            key: 'path',
        },
        {
            title: '顺序',
            dataIndex: 'order',
            key: 'order',
        }, {
            title: '阅读量',
            dataIndex: 'views',
            render: (text: string, record: any) => {
                return <Badge count={text} showZero style={{ backgroundColor: '#52c41a' }} />
            },
        }, {
            title: '状态',
            dataIndex: 'status',
            render: (text: string) => {
                return <>{text === "publish" ? <Badge status="success" text="已发布" /> : <Badge status="warning" text="草稿" />}</>
            }
        }, {
            title: '发布时间',
            dataIndex: 'publishAt',
            render: (text: string, record: any) => {
                return <div>{moment(text).format("YYYY-MM-DD HH:mm:ss")}</div>
            },
        },
        {
            title: '操作',
            key: 'action',
            render: (text: string, record: any) => (
                <Space size="middle">
                    <a>编辑 </a>
                    {record.status === "draft" ? <a onClick={() => store.Page.getPublish([record.id], "publish")
                    }>发布</a> : <a onClick={() => store.Page.getPublish([record.id], "draft")
                    }>下线</a>}
                    <a onClick={showModal}>查看访问</a>
                    <Modal title="访问统计" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} footer={false} width={640} className={styles.page_content_modal}>
                        <p>Some contents...</p>
                        <p>Some contents...</p>
                        <p>Some contents...</p>
                    </Modal>
                    <Popconfirm
                        title="确定删除这个文章？"
                        onConfirm={() => store.Page.getDeletePage([record.id])
                        }
                        okText="Yes"
                        cancelText="No"
                    >
                        <a href="#">删除</a>
                    </Popconfirm>
                </Space >
            ),
        },
    ];
    const rowSelection = {
        onChange: (selectedRowKeys: React.Key[],) => {
            setSelectedRowKeys(selectedRowKeys);
        }
    };
    return (<div className={styles.page}>
        <Search pageStatus={pageStatus} pageInput={pageinput} pageState={pagestate} SearchBtn={SearchBtn} />
        <div className={styles.page_content}>
            <div className={styles.table}>
                <div className={styles.table_btn}>
                    <div className={styles.table_btnleft}>
                        {
                            selectedRowKeys.length ? <><Button style={{ marginRight: 8 }} onClick={() => store.Page.getPublish(selectedRowKeys as string[], "publish")}>发布</Button>
                                <Button style={{ marginRight: 8 }} onClick={() => store.Page.getPublish(selectedRowKeys as string[], "draft")}>下线</Button>
                                <Popconfirm
                                    title="确定删除这个文章？"
                                    onConfirm={() => store.Page.getDeletePage(selectedRowKeys as string[])
                                    }
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <Button>删除</Button>
                                </Popconfirm>
                            </> : ''
                        }

                    </div>
                    <div className={styles.table_btnright}>
                        <Button type="primary" icon={<PlusOutlined />}>
                            新建
                        </Button>
                        <ReloadOutlined style={{ marginLeft: 12 }} onClick={() => store.Page.getPageManageMent(page)} />
                    </div>
                </div>
                <Table
                    rowSelection={{
                        ...rowSelection,
                    }}
                    columns={columns}
                    dataSource={store.Page.pageList}
                    pagination={{ showSizeChanger: true }}
                    rowKey="id"
                />
                {
                    store.Page.count ? <span className={styles.total}>共{store.Page.count}条</span> : ''
                }
            </div>
        </div>
    </div>
    )
}
export default observer(PageManagement);