import  React,{ useEffect, useState } from 'react';
import useStore from '@/context/useStore';
import { 
  Form,
  Row,
  Col,
  Input,
  Button,
  Select,
  Card,
  Table,
  Space,
  Tag,
  Badge,
  Popconfirm,} from "antd"
import { ReloadOutlined } from '@ant-design/icons';
import { IArticle, ICategory, ITag } from '@/types';
import { observer } from 'mobx-react-lite';


const { Option } = Select;
const index: React.FC = (_props) => {
  const color = [
    'magenta',
    'red',
    'volcano',
    'orange',
    'lime',
    'green',
    'cyan',
    'blue',
    'purple',
  ];
  const store = useStore();
  const [form] = Form.useForm();
  const [flag, isflag] = useState(false);
  const [selectedRowKeys , setSelectedRowKeys] = useState<React.Key[]>([])
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[]) => {
      setSelectedRowKeys(selectedRowKeys)
      selectedRowKeys.length > 0 ? isflag(true) : isflag(false);
    },
  };
  const columns: any = [
    {
      title: '标题',
      width: 200,
      dataIndex: 'title',
      fixed: 'left',
    },
    {
      title: '状态',
      dataIndex: 'status',
      width: 100,
      render: (status: string) => {
        return status == 'publish' ? '发布' : '草稿';
      },
    },
    {
      title: '分类',
      dataIndex: 'category',
      width: 100,
      render: (category: ICategory) => {
        if (category) {
          return (
            <Tag color={color[Number(category.id[9])]}>{category.label}</Tag>
          );
        }
        return '';
      },
    },
    {
      title: '标签',
      dataIndex: 'tags',
      render: (tags: ITag[]) => {
        return tags.map((item, index) => {
          return (
            <Tag key={index} color={color[Number(item.id[10])]}>
              {item.label}
            </Tag>
          );
        });
      },
      width: 200,
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      width: 120,
      render: (views: number) => {
        return (
          <Badge count={views} style={{ background: '#52c41a' }} showZero />
        );
      },
    },
    {
      title: '喜欢数',
      dataIndex: 'likes',
      width: 120,
      render: (likes: number) => {
        return (
          <Badge count={likes} style={{ background: '#EB2F96' }} showZero />
        );
      },
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      width: 200,
    },
    {
      title: '操作',
      key: 'operation',
      fixed: 'right',
      width: 300,
      render: (item: IArticle) => (
        <Space>
          <a>编辑</a>
          <a
            onClick={() => {
              store.Article.editRecommended(item.id, !item.isRecommended);
            }}
          >
            {item.isRecommended ? '撤销首焦' : '首焦推荐'}
          </a>
          <a
            onClick={() => {
              store.Article.getView(item.id);
            }}
          >
            查看访问
          </a>
          <Popconfirm
            title="你确定要删除吗？"
            okText="确认"
            cancelText="取消"
            onConfirm={(_onConfirm) => {
              store.Article.delArticle(item.id);
            }}
          >
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];

  // 获取所有分类/数据
  useEffect(() => {
    store.Article.getCategory();
  }, []);
  useEffect(() => {
    store.Article.getArticle();
  }, []);
  // 搜索按键的值
  const onFinish = (values: any) => {
    console.log('Received values of form: ', values);
  };
  const getFields = () => {
    const children = [];

    children.push(
      <Form.Item style={{ width: 250 }} name="标题" label="标题">
        <Input placeholder="请输入文章标题" />
      </Form.Item>,
      <Form.Item
        style={{ width: 230, marginLeft: '10%' }}
        name="状态"
        label="状态"
      >
        <Select>
          <Option value="已发布">已发布</Option>
          <Option value="草稿">草稿</Option>
        </Select>
      </Form.Item>,
      <Form.Item
        style={{ width: 230, marginLeft: '10%' }}
        name="分类"
        label="分类"
      >
        <Select>
          {store.Article.CategoryData.map((item:any, index:any) => {
            return (
              <Option value={item.label} key={index}>
                {item.label}
              </Option>
            );
          })}
        </Select>
      </Form.Item>,
    );

    return children;
  };

  return (
    <div>
      
      <Card
        title={
          flag ? (
            <>
              <Button style={{marginRight:'10px'}} onClick={()=>{
                  selectedRowKeys.forEach(item=>{
                    store.Article.publishArticle(item as string)
                  })
                
              }}>发布</Button>
              <Button style={{marginRight:'10px'}} onClick={()=>{
                  selectedRowKeys.forEach(item=>{
                    store.Article.drafthArticle(item as string)
                  })
                
              }}>草稿</Button>
              <Button style={{marginRight:'10px'}}  onClick={()=>{
                  selectedRowKeys.forEach(item=>{
                    store.Article.sRecommended(item as string)
                  })
                
              }}>首焦推荐</Button>
              <Button style={{marginRight:'10px'}}  onClick={()=>{
                  selectedRowKeys.forEach(item=>{
                    store.Article.fRecommended(item as string)
                  })
                
              }}>撤销首焦</Button>
              <Popconfirm style={{marginRight:'10px'}}
                title="你确定要删除吗？"
                okText="确认"
                cancelText="取消"
                onConfirm={(_onConfirm) => {
                  selectedRowKeys.forEach(item=>{
                    store.Article.delArticle(item as string)
                  })
                }}
              >
                <Button danger>
                  删除
                </Button>
              </Popconfirm>
            </>
          ) : (
            ''
          )
        }
        extra={
          <>
            {' '}
            <Button type="primary" style={{ marginRight: '10px' }}>
              {' '}
              + 新建
            </Button>{' '}
            <ReloadOutlined />{' '}
          </>
        }
      >
      
        <Table
          
          rowSelection={rowSelection}
          columns={columns}
          dataSource={store.Article.data.map((item: { key: any; id: any; }) => {
            item.key = item.id;
            return item;
          })}
          rowKey="id"
          pagination={{
            showSizeChanger: true,
            pageSizeOptions: ['8数', '12', '24', '36'],
            showTotal: () => `共${store.Article.data.length}`,
            
          }}
        />
       
      </Card>
    </div>
  );
};
export default observer(index);
