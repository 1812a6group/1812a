import useStore from '@/context/useStore';
import React, { useEffect, useState, useRef } from 'react';
import { Card, Form, Input, Button, Tag, Popconfirm, message } from 'antd';
import styles from '../classify/index.less';
import { observer } from 'mobx-react-lite';
const index: React.FC = (props) => {
  const [visible, setvisible] = useState(true);
  const [editClassify, seteditClassify] = useState('');
  const [editClassifyValue, seteditClassifyValue] = useState('');
  const [addClassify, setaddClassify] = useState('');
  const [addClassifyValue, setaddClassifyValue] = useState('');
  const [id, setid] = useState('');
  const store = useStore();

  const onConfirm = () => {
    store.Article.delTag(id);
    message.success('确定删除成功');

    setvisible(true);
  };

  // 获取所有标签数据
  useEffect(() => {
    store.Article.getTag();
  }, []);

  return (
    <div style={{ display: 'flex' }}>
      {visible ? (
        <Card title="添加标签" style={{ width: '37.5%' }}>
          <Form>
            <Form.Item>
              <Input
                placeholder="请输入标签名称"
                value={addClassify}
                onChange={(e) => {
                  setaddClassify(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item>
              <Input
                placeholder="输入标签值（请输入英文，作为路由使用）"
                value={addClassifyValue}
                onChange={(e) => {
                  setaddClassifyValue(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                onClick={() => {
                  store.Article.addTag(addClassify, addClassifyValue);
                  setaddClassify('');
                  setaddClassifyValue('');
                  message.success('添加标签成功');
                }}
              >
                保存
              </Button>
            </Form.Item>
          </Form>
        </Card>
      ) : (
        <Card title="管理标签" style={{ width: '37.5%' }}>
          <Form>
            <Form.Item>
              <Input
                placeholder="请输入标签名称"
                value={editClassify}
                onChange={(e) => {
                  seteditClassify(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item>
              <Input
                placeholder="输入标签值（请输入英文，作为路由使用）"
                value={editClassifyValue}
                onChange={(e) => {
                  seteditClassifyValue(e.target.value);
                }}
              />
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                onClick={() => {
                  store.Article.editTag(
                    id,
                    editClassify,
                    editClassifyValue,
                  );
                  setvisible(true);
                  message.success('更新标签成功');
                }}
              >
                更新
              </Button>
              <Button
                type="dashed"
                style={{ borderLeft: 'none' }}
                onClick={() => {
                  setvisible(true);
                }}
              >
                返回添加
              </Button>
              <Popconfirm
                title="你确定要删除吗？"
                okText="确认"
                cancelText="取消"
                onConfirm={onConfirm}
              >
                <Button danger style={{ float: 'right' }}>
                  删除
                </Button>
              </Popconfirm>
            </Form.Item>
          </Form>
        </Card>
      )}

      <Card title="所有标签" style={{ width: '59.5%', marginLeft: '3%' }}>
        {store.Article.TagData.map((item, index) => {
          return (
            <Tag
              key={index}
              className={styles.Tag}
              onClick={() => {
                setid(item.id);
                seteditClassify(item.label);
                seteditClassifyValue(item.value);
                setvisible(false);
              }}
            >
              {item.label}
            </Tag>
          );
        })}
      </Card>
    </div>
  );
};
export default observer(index);
