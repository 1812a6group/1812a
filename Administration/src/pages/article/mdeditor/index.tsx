import Editor from 'for-editor'
import { useEffect, useState } from 'react';
import styles from './index.less';
import { message, Dropdown, Menu, Drawer, Form, Input, Button, Select, Popconfirm } from 'antd';
import useStore from '@/context/useStore';
import { makeHtml, makeToc } from '@/utils/markdown';
import { EllipsisOutlined, } from '@ant-design/icons';
import { history } from "umi"
const ArticleUpdateEditor = () => {
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [settingDrawer, setSettingDrawer] = useState(false);
    const [cover, setCover] = useState('');

    const store = useStore();
    const [form] = Form.useForm();

    useEffect(() => {
        store.Article.getCategory();
        store.tag.getTagList();
    }, [])

    // 发布或者更新文章
    async function submit() {
        let values: { [key: string]: string | boolean } = form.getFieldsValue();
        if (!title) {
            message.warn('请输入文章标题');
            return;
        } else {
            message.warn("发布成功")
        }
        // 添加文章的md
        values.content = content;
        // 添加文章内容的html
        values.html = makeHtml(content);
        // 添加文章的toc
        values.toc = JSON.stringify(makeToc(values.html as string));
        // 添加文章的标题
        values.title = title;
        // 添加文章状态
        values.status = 'publish';

        let result = await store.Article.publish(values);
        // if(result){
        //     history.reqlace(`/article/editor/${result.id}`)
        // }
        console.log('values...', result);
    }
    const toolbar = {
        h1: true, // h1
        h2: true, // h2
        h3: true, // h3
        h4: true, // h4
        img: true, // 图片
        link: true, // 链接
        code: true, // 代码块
        preview: true, // 预览
        expand: true, // 全屏
        /* v0.0.9 */
        undo: true, // 撤销
        redo: true, // 重做
        save: true, // 保存
        /* v0.2.3 */
        subfield: true, // 单双栏模式
    }
    const editorMenu = <Menu>
        <Menu.Item>查看</Menu.Item>
        <Menu.Item onClick={() => setSettingDrawer(true)}>设置</Menu.Item>
        <Menu.Item>保存草稿</Menu.Item>
        <Menu.Item>删除</Menu.Item>
    </Menu>

    console.log('form..', form.getFieldsValue());
    return <div>
        <header>
            <div>
                <Popconfirm
                    title="确认关闭？如果有内容变更，请先保存。"
                    okText="确定"
                    cancelText="取消"
                    onConfirm={() => history.replace('/article')}
                >
                    <button className={styles.btn}>X</button>
                </Popconfirm>
                <input type="text"
                    value={title}
                    onChange={e => setTitle(e.target.value)}
                    className={styles.inp}
                />
            </div>
            <div>
                <button onClick={submit} className={styles.btn2}>发布</button>
                <Dropdown overlay={editorMenu} >
                    <EllipsisOutlined />
                </Dropdown>
            </div>

        </header>
        <section className={styles.editor}>
            <iframe src="https://jasonandjay.com/editor/static/"></iframe>
            {/* <Editor
                value={content}
                toolbar={toolbar}
                onChange={value => setContent(value)}
            /> */}
        </section>
        <Drawer width="35%" title="文章设置" placement="right" onClose={() => setSettingDrawer(false)} visible={settingDrawer}>
            <Form
                form={form}
            >
                <Form.Item name="summary" label="文章摘要">
                    <Input.TextArea></Input.TextArea>
                </Form.Item>
                <Form.Item name="password" label="访问密码">
                    <Input.Password placeholder="" />
                </Form.Item>
                <Form.Item name="totalAmount" label="付费查看">
                    <Input.Password placeholder="" />
                </Form.Item>
                <Form.Item name="isCommentable" label="开启评论">
                    <Input type="checkbox" placeholder="" />
                </Form.Item>
                <Form.Item name="isRecommended" label="首页推荐">
                    <Input type="checkbox" placeholder="" />
                </Form.Item>
                <Form.Item name="category" label="选择分类">
                    <Select>{
                        store.Article.CategoryData.map(item => {
                            return <Select.Option value={item.id}>{item.label}</Select.Option>
                        })
                    }</Select>
                </Form.Item>
                <Form.Item name="tags" label="选择标签">
                    <Select>{
                        store.tag.tagList.map(item => {
                            return <Select.Option value={item.id}>{item.label}</Select.Option>
                        })
                    }</Select>
                </Form.Item>
                <img src={cover} alt="" />
                <Form.Item name="cover" label="文章封面">
                    <Input type="text" placeholder="" onChange={e => setCover(e.target.value)} />
                </Form.Item>
                <Button onClick={() => {
                    let values = form.getFieldsValue();
                    form.setFieldsValue({ ...values, cover: '' })
                    setCover('');
                }}>移除</Button>
                <Button htmlType="submit">确认</Button>
            </Form>
        </Drawer>
    </div>
}

export default ArticleUpdateEditor;


