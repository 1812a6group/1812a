import React, { Key, useEffect, useState } from 'react'
import { Table, Button, message } from 'antd';
import styles from './index.less';
import { Form, Row, Col, Input, Select, } from 'antd';
import { observer } from 'mobx-react-lite'
import useStore from '@/context/useStore';
import { ManageItem } from '@/types';
import moment from 'moment';
import { RedoOutlined } from '@ant-design/icons';
const { Option } = Select;

const Manage: React.FC = (props) => {
    const columns = [
        {
            title: '账户',
            dataIndex: 'name',

        },
        {
            title: '邮箱',
            dataIndex: 'email',

        },
        {
            title: '角色',
            dataIndex: 'role',
            render: (item: string) => {
                return <div>
                    {item === "admin" ? "管理员" : "访客"}
                </div>
            }
        },
        {
            title: '状态',
            dataIndex: 'status',
            render: (item: string) => {
                return <div>
                    {item === "locked" ? "已锁定" : "可用"}
                </div>
            }
        },
        {
            title: '注册日期',
            dataIndex: 'updateAt',
            render: (item: ManageItem) => {
                return <div>
                    {moment(item.updateAt).format()}
                </div>
            }
        },
        {
            title: '操作',
            // dataIndex: 'operation',
            render: (item: ManageItem) => {
                console.log(item.role);
                return <div>
                    {item.status === "active" ? <a>启用</a> : <a>禁用</a>}
                    <div className={styles.zhong}></div>
                    {item.role === "visitor" ? <a>授权</a> : <a>解除授权</a>}
                </div>
            }

        },
    ];
    const [loading, setloading] = useState(false)
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])
    const [arrId, setArrId] = useState([])
    const [page, setPage] = useState(1)
    const store = useStore()
    useEffect(() => {
        store.Manage.getManage(page)
    }, [page])

    const [form] = Form.useForm();
    //搜索
    const onFinish = (values: string) => {
        console.log(values);

        store.Manage.getManage(page, values)
    };

    //下拉框
    function handleChange(value: string) {
        console.log(`xxxx ${value}`);
        // store.Manage.getManage(page, value)

    }
    // 表格选中操作
    const onSelectChange = (selectedRowKeys: any, selectedRows: ManageItem[]) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        setSelectedRowKeys(selectedRowKeys);
        setArrId(selectedRowKeys)
    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };

    //保存选中的数据

    let newArr: ManageItem[] = []

    //复选框的启用..多个启用
    const multipleStart = () => {
        //保存选中的数据
        store.Manage.ManageDate.filter(item => {

            arrId.map((item2) => {
                if (item.id === item2) {
                    return newArr.push(item)
                }
            })
        })
        newArr.forEach(async (item, index) => {
            await store.Manage.getManageId({ id: item.id, avatar: item.avatar, email: item.email, createAt: item.createAt, name: item.name, role: item.role, status: `active`, updateAt: item.updateAt })
        })
        setTimeout(() => {
            store.Manage.getManage(page)
        }, 100);
        message.success('操作成功')
    }

    //禁用
    const onDisabled = () => {
        //保存选中的数据
        store.Manage.ManageDate.filter(item => {

            arrId.map((item2) => {
                if (item.id === item2) {
                    return newArr.push(item)
                }
            })
        })
        newArr.forEach(async (item, index) => {
            await store.Manage.getManageId({ id: item.id, avatar: item.avatar, email: item.email, createAt: item.createAt, name: item.name, role: item.role, status: `locked`, updateAt: item.updateAt })
        })
        setTimeout(() => {
            store.Manage.getManage(page)
        }, 100);
        message.success('操作成功')
    }

    //授权
    const onauthorization = () => {
        //保存选中的数据
        store.Manage.ManageDate.filter(item => {

            arrId.map((item2) => {
                if (item.id === item2) {
                    return newArr.push(item)
                }
            })
        })
        newArr.forEach(async (item, index) => {
            await store.Manage.getManageId({ id: item.id, avatar: item.avatar, email: item.email, createAt: item.createAt, name: item.name, role: `admin`, status: item.status, updateAt: item.updateAt })
        })
        setTimeout(() => {
            store.Manage.getManage(page)
        }, 100);
        message.success('操作成功')
    }

    //解除授权
    const onrelieve = () => {
        //保存选中的数据
        store.Manage.ManageDate.filter(item => {

            arrId.map((item2) => {
                if (item.id === item2) {
                    return newArr.push(item)
                }
            })
        })
        newArr.forEach(async (item, index) => {
            await store.Manage.getManageId({ id: item.id, avatar: item.avatar, email: item.email, createAt: item.createAt, name: item.name, role: `visitor`, status: item.status, updateAt: item.updateAt })
        })
        setTimeout(() => {
            store.Manage.getManage(page)
        }, 100);
        message.success('操作成功')
    }
    const hasSelected = selectedRowKeys.length > 0;
    return (
        <div className={styles.mian}>
            <Form form={form} name="advanced_search" className={styles.form} onFinish={onFinish} key='form'>
                <Row key='row'>
                    <Form.Item name='name' key='name' label='账户' className={styles.formItem}>
                        <Input placeholder="请输入用户账户" />
                    </Form.Item>
                    <Form.Item name='email' key='email' label='邮箱' className={styles.formItem}>
                        <Input placeholder="请输入账户邮箱" />
                    </Form.Item>
                    <Form.Item label="角色" className={styles.formItem} key='js' name="role">
                        <Select style={{ width: 200 }}>
                            <Select.Option value="admin" key='1'>管理员</Select.Option>
                            <Select.Option value="visitor" key='2'>访客</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label="状态" className={styles.formItem} key='zt' name="status">
                        <Select style={{ width: 200 }} >
                            <Select.Option value="locked" key='1'>锁定</Select.Option>
                            <Select.Option value="active" key='2'>可用</Select.Option>
                        </Select>
                    </Form.Item>
                </Row>
                <Row key='row1'>
                    <Col span={24} style={{ textAlign: 'right' }} key='col'>
                        <Button type="primary" htmlType="submit">搜索</Button>
                        <Button style={{ margin: '0 8px' }} onClick={() => { form.resetFields(); }}>重置</Button>
                    </Col>
                </Row>
            </Form>
            <div className={styles.table}>
                <div style={{ marginBottom: 15 }} className={styles.table_top}>

                    <div>
                        {hasSelected ? <div>
                            <Button onClick={multipleStart} style={{ marginRight: 8 }}>启 用</Button>
                            <Button onClick={onDisabled} style={{ marginRight: 8 }}>禁 用</Button>
                            <Button onClick={onrelieve} style={{ marginRight: 8 }}>解除授权</Button>
                            <Button onClick={onauthorization} style={{ marginRight: 8 }}>授 权</Button>
                        </div> : null}
                    </div>
                    <div className={styles.table_bottom}>
                        <span onClick={() => {
                            console.log(111);
                        }}
                        ><RedoOutlined /></span>
                    </div>
                </div>

                <Table
                    rowSelection={rowSelection}
                    // loading={Boolean(store.Manage.ManageDate.length)}
                    columns={columns}
                    dataSource={store.Manage.ManageDate}
                    rowKey="id"

                    pagination={{ showSizeChanger: true, pageSizeOptions: ['8', '12', '24', '36'] }}

                />

            </div>
        </div >
    )
}
export default observer(Manage)
