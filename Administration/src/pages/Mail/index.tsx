import React, { useEffect, useState } from 'react'
import Search from '@/components/search'
import { Table, Button } from 'antd';
import { ReloadOutlined } from '@ant-design/icons'
import styles from './index.less'
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import moment from 'moment'
import { IMail } from '@/types';
const pageinput = [{
  label: "发件人",
  name: 'from',
  placeholder: "请输入发件人"
},
{
  label: "收件人",
  name: 'to',
  placeholder: "请输入收件人"
}, {
  label: '主题',
  name: 'subject',
  placeholder: '请输入主题'
}]

const index: React.FC = (props) => {
  const [page, setPage] = useState(1)
  const [params, setParams] = useState({});
  const [flag, isflag] = useState(false);
  const store = useStore()
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([])
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[]) => {
      setSelectedRowKeys(selectedRowKeys)
      selectedRowKeys.length > 0 ? isflag(true) : isflag(false);
    },
  };
  function SearchBtn(value: any) {
    let params: { [key: string]: string } = {};
    for (let key in value) {
      value[key] && (params[key] = value[key])
    }
    setParams(params)
  }
  useEffect(() => {
    store.Comments.getMail(page, params)
  }, [])
  const columns = [
    {
      title: '发件人',
      dataIndex: 'from',
    },
    {
      title: '收件人',
      dataIndex: 'to',
    },
    {
      title: '主题',
      dataIndex: 'subject',
    },
    {
      title: '发送时间',
      dataIndex: 'createAt',
      render: (text: IMail) => {
        return moment(text.createAt).format("YYYY-MM-DD HH:mm:ss")
      }
    },
    {
      title: '操作',
      dataIndex: 'address',
      render: (item: IMail) => {
        return <Button type='link' onClick={() => store.Comments.delMail([item.id])}>删除</Button>
      }
    }
  ];
  return <div>
    <Search pageInput={pageinput} SearchBtn={SearchBtn} />
    <div className={styles.box}>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <div></div>
        <div> <ReloadOutlined /></div>
      </div>
      <Table columns={columns} rowSelection={rowSelection} dataSource={store.Comments.MailData} />
    </div>

  </div>
}
export default observer(index)