import { Card, List, Upload, message, Drawer, Button, Row, Col, Space } from 'antd'
import { InboxOutlined } from '@ant-design/icons';
const { Dragger } = Upload;
import Search from "@/components/search";
import React, { useEffect, useState } from "react"
import useStore from '@/context/useStore';
import styles from './index.less'
import { observer } from 'mobx-react-lite'
import moment from 'moment'
import { IFile } from '@/types';
import { copyText } from '@/utils/copy'
import ImageView from '@/components/ImageView';
const { Meta } = Card;
const pageinput = [{
    label: "文件名称",
    name: 'originalname',
    placeholder: "请输入文件名称"
}, {
    label: "文件类型",
    name: 'type',
    placeholder: "请输入文件类型"
}]
const props = {
    name: 'file',
    multiple: true,
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info: { file: { name?: any; status?: any; }; fileList: any; }) {
        const { status } = info.file;
        if (status !== 'uploading') {
            console.log(info.file, info.fileList);
        }
        if (status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
        } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
        }
    },
    onDrop(e: { dataTransfer: { files: any; }; }) {
        console.log('Dropped files', e.dataTransfer.files);
    },
}
const index: React.FC = () => {
    //页面渲染
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(12)
    const [params, setParams] = useState({});
    const [data, setData] = useState({} as IFile)
    const store = useStore()
    useEffect(() => {
        store.Comments.getFile(page, params, pageSize)
    }, [page, params])
    function SearchBtn(value: any) {
        let params: { [key: string]: string } = {};
        for (let key in value) {
            value[key] && (params[key] = value[key])
        }
        setParams(params)
    }
    //抽屉
    const [visible, setVisible] = useState(false);
    const showDrawer = (item: IFile) => {
        setData(item)
        setVisible(true);
    };
    const onClose = () => {
        setVisible(false);
    };
    return <div>
        <div style={{ marginBottom: '24px' }}>
            <Dragger {...props} style={{ background: '#fff' }}>
                <p className="ant-upload-drag-icon"><InboxOutlined /> </p>
                <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                <p className="ant-upload-hint">文件将上传到 OSS, 如未配置请先配置</p>
            </Dragger>
        </div>
        <Search pageInput={pageinput} SearchBtn={SearchBtn} />
        <List
            grid={{ gutter: 16, column: 4 }}
            dataSource={store.Comments.FileData}
            pagination={{
                total: store.Comments.FileCount,
                onChange: page => {
                    setPage(page)
                },
                onShowSizeChange: (current, size) => {
                    console.log(size);

                    setPageSize(size)
                },
                pageSizeOptions: ['8', '12', '24', '36'],
                showSizeChanger: true,
                defaultPageSize: 12,
                showTotal: total => `共 ${total}条`
            }}
            renderItem={item => (
                <div onClick={() => showDrawer(item)} className={styles.list}>
                    <List.Item>
                        <Card
                            hoverable={true}
                            cover={
                                <div className={styles.box}>
                                    <img
                                        alt="example"
                                        src={item.url}
                                    />
                                </div>
                            }
                        >
                            <Meta
                                title={item.originalname}
                                description={`上传于${moment(item.createAt).format("YYYY-MM-DD HH:mm:ss")}`}
                            />
                        </Card>
                    </List.Item>

                </div>

            )}
        />
        <Drawer
            title="文件信息"
            width={640}
            closable={true}
            onClose={onClose}
            visible={visible}
        >
            <ImageView>
                <div className={styles.image}>
                    <img src={data.url} alt="" />
                </div>
            </ImageView>
            <Row>
                <Col span={12}>
                    <div className={styles.warp}>
                        <p>文件名称:</p>
                        <div>{data.originalname} </div>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <div className={styles.warp}>
                        <p>存储路径:</p>
                        <div>{data.filename}</div>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <div className={styles.warp}>
                        <p>文件类型:</p>
                        <div>{data.type}</div>
                    </div>
                </Col>
                <Col span={12}>
                    <div className={styles.warp}>
                        <p>文件大小:</p>
                        <div>{(data.size / 1024).toFixed(2)}KB</div>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col span={24}>
                    <div className={styles.warp}>
                        <p>访问链接:</p>
                        <div onClick={() => { copyText(data.url) }}>
                            <div className={styles.url}>
                                {data.url}
                            </div>
                            <Button type='link'>复制</Button>
                        </div>
                    </div>
                </Col>
            </Row>
            <br />
            <div className={styles.mask_btn}>
                <Button onClick={onClose} style={{ marginRight: '8px' }}>关闭</Button>
                <Button danger ghost>删除</Button>
            </div>
        </Drawer>
    </div>
}
export default observer(index)