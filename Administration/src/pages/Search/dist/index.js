"use strict";
exports.__esModule = true;
var useStore_1 = require("@/context/useStore");
var icons_1 = require("@ant-design/icons");
var antd_1 = require("antd");
var mobx_react_lite_1 = require("mobx-react-lite");
var react_1 = require("react");
var times_1 = require("@/utils/times");
var index_less_1 = require("./index.less");
var handleDelete = function (key) {
    // const dataSource = [...this.state.dataSource];
    // this.setState({ dataSource: dataSource.filter(item => item.key !== key) });
};
var Search = function (props) {
    var store = useStore_1["default"]();
    var _a = react_1.useState(1), page = _a[0], setPage = _a[1];
    var _b = react_1.useState(false), flag = _b[0], setflag = _b[1];
    var _c = react_1.useState([]), selectedRowKeys = _c[0], setSelectedRowKeys = _c[1];
    // 复选框
    function onSelectChange(selectedRowKeys, selectedRows) {
        setSelectedRowKeys(selectedRowKeys);
    }
    var rowSelection = {
        selectedRowKeys: selectedRowKeys,
        onChange: onSelectChange
    };
    // 获取搜索列表
    react_1.useEffect(function () {
        store.Search.getsearch(page);
        console.log(page);
    }, [page]);
    var columns = [
        {
            title: '搜索词',
            dataIndex: 'keyword'
        },
        {
            title: '搜索量',
            dataIndex: 'count',
            render: function (text) { return (react_1["default"].createElement(antd_1.Badge, { className: "site-badge-count-109", count: text, style: { backgroundColor: '#52c41a' } })); }
        },
        {
            title: '搜索时间',
            dataIndex: 'createAt',
            render: function (text) { return (react_1["default"].createElement(react_1["default"].Fragment, null, times_1.fomatTime(text))); }
        },
        {
            title: '操作',
            key: 'action',
            render: function (_, record) { return (react_1["default"].createElement(antd_1.Popconfirm, { title: "\u786E\u8BA4\u5220\u9664\u8FD9\u4E2A\u641C\u7D22\u8BB0\u5F55\uFF1F", onConfirm: function () { return handleDelete(record.key); }, okText: "\u786E\u5B9A", cancelText: "\u53D6\u6D88" },
                react_1["default"].createElement("a", null, "\u5220\u9664"))); }
        },
    ];
    return react_1["default"].createElement("div", null,
        react_1["default"].createElement("div", { className: index_less_1["default"].cont },
            react_1["default"].createElement("div", { className: index_less_1["default"].cont_1 },
                react_1["default"].createElement("div", { className: index_less_1["default"].cont_input },
                    react_1["default"].createElement(antd_1.Form
                    // form={form}
                    // onFinish={submit}
                    , { 
                        // form={form}
                        // onFinish={submit}
                        className: index_less_1["default"].Form_input },
                        react_1["default"].createElement(antd_1.Form.Item, { label: "\u7C7B\u578B", name: "from", className: index_less_1["default"].Form_item },
                            react_1["default"].createElement(antd_1.Input, { type: "text", placeholder: "\u8BF7\u8F93\u5165\u7C7B\u578B" })),
                        react_1["default"].createElement(antd_1.Form.Item, { label: "\u641C\u7D22\u8BCD", name: "from", className: index_less_1["default"].Form_item },
                            react_1["default"].createElement(antd_1.Input, { type: "text", placeholder: "\u8BF7\u8F93\u5165\u641C\u7D22\u8BCD" })),
                        react_1["default"].createElement(antd_1.Form.Item, { label: "\u641C\u7D22\u91CF", name: "from", className: index_less_1["default"].Form_item },
                            react_1["default"].createElement(antd_1.Input, { type: "text", placeholder: "\u8BF7\u8F93\u5165\u641C\u7D22\u91CF" })))),
                react_1["default"].createElement("div", { className: index_less_1["default"].cont_btn },
                    react_1["default"].createElement("button", { className: index_less_1["default"].cont_btn_01 }, "\u641C\u7D22"),
                    react_1["default"].createElement("button", { className: index_less_1["default"].cont_btn_02 }, "\u91CD\u7F6E"))),
            react_1["default"].createElement("div", { className: index_less_1["default"].cont_2 },
                react_1["default"].createElement("div", { className: index_less_1["default"].cont_head },
                    react_1["default"].createElement("span", null,
                        react_1["default"].createElement(antd_1.Popconfirm, { title: "\u786E\u8BA4\u5220\u9664\uFF1F", 
                            // onConfirm={() => handleDelete(record.key)}
                            okText: "\u786E\u5B9A", cancelText: "\u53D6\u6D88" }, selectedRowKeys.length ? react_1["default"].createElement(antd_1.Button, { danger: true }, "\u5220\u9664") : '')),
                    react_1["default"].createElement("span", null,
                        react_1["default"].createElement(antd_1.Tooltip, { title: "\u5237\u65B0" },
                            react_1["default"].createElement(icons_1.ReloadOutlined, null)))),
                react_1["default"].createElement("div", null,
                    react_1["default"].createElement(antd_1.Table, { columns: columns, dataSource: store.Search.Sdata, rowSelection: rowSelection, rowKey: "id", pagination: { showSizeChanger: true } })))));
};
exports["default"] = mobx_react_lite_1.observer(Search);
