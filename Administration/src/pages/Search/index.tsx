import useStore from '@/context/useStore';
import { ReloadOutlined } from '@ant-design/icons';
import { Table, Badge, Button, Popconfirm, Tooltip, Form, Input,ConfigProvider } from 'antd';
import { observer } from 'mobx-react-lite';
import React,{ useState, useEffect, Key } from "react";
import { fomatTime } from "@/utils/times";
import styles from "./index.less";
import { ISearch } from '@/types';
import zhCN from 'antd/lib/locale/zh_CN';
const Search: React.FC = (props) => {
    const store = useStore();
    const [page, setPage] = useState(1)
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])
    const [params, setParams] = useState({})
    const [form] = Form.useForm();

    // 获取搜索列表
    useEffect(() => {
        store.Search.getsearch(page, params)
        console.log(page);
    }, [page, params]);
    // 条件查询
    function submit() {
        let values = form.getFieldsValue();
        let params: { [key: string]: string } = {};
        for (let key in values) {
            values[key] && (params[key] = values[key]);
        }
        setParams(params);
        setPage(1);
        console.log("搜索成功...");
    }
    // 复选框
    function onSelectChange(selectedRowKeys: Key[], selectedRows: ISearch[]) {
        setSelectedRowKeys(selectedRowKeys);
    }
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange
    }
    const columns = [
        {
            title: '搜索词',
            dataIndex: 'keyword',
        },
        {
            title: '搜索量',
            dataIndex: 'count',
            render: (text: any,) => (
                <Badge
                    className="site-badge-count-109"
                    count={text}
                    style={{ backgroundColor: '#52c41a' }}
                />
            ),
        },
        {
            title: '搜索时间',
            dataIndex: 'createAt',
            render: (text: any) => (
                <>{fomatTime(text)}</>
            ),
        },
        {
            title: '操作',
            key: 'action',
            render: (item: ISearch) => (
                <Popconfirm title="确认删除这个搜索记录？"
                    onConfirm={() => store.Search.deleteSearch([item.id])}
                    okText="确定"
                    cancelText="取消"
                >
                    <a>删除</a>
                </Popconfirm>
            ),
        },
    ];
    return <div>
        <div className={styles.cont}>
            <div className={styles.cont_1}>
                <div className={styles.cont_input}>
                    <Form
                        form={form}
                        onFinish={submit}
                        className={styles.Form_input}
                    >
                        <Form.Item
                            label="类型"
                            name="from"
                            className={styles.Form_item}>
                            <Input type="text" placeholder="请输入类型" />
                        </Form.Item>
                        <Form.Item
                            label="搜索词"
                            name="keyword"
                            className={styles.Form_item}>
                            <Input type="text" placeholder="请输入搜索词" />
                        </Form.Item>
                        <Form.Item
                            label="搜索量"
                            name="count"
                            className={styles.Form_item}>
                            <Input type="text" placeholder="请输入搜索量" />
                        </Form.Item>
                        <div className={styles.cont_btn}>
                            <Button className={styles.cont_btn_01} htmlType="submit">搜索</Button>
                            <Button className={styles.cont_btn_02} htmlType="reset">重置</Button>
                        </div>
                    </Form>
                </div>
            </div>
            <div className={styles.cont_2}>
                <div className={styles.cont_head}>
                    <span>
                        <Popconfirm title="确认删除？"
                            onConfirm={() => store.Search.deleteSearch(selectedRowKeys as string[])}
                            okText="确定"
                            cancelText="取消"
                        >
                            {selectedRowKeys.length ? <Button danger>删除</Button> : ''}
                        </Popconfirm>
                    </span>
                    <span>
                        <Tooltip title="刷新"><ReloadOutlined onClick={() => {
                            setPage(1);
                            setParams({ ...params });
                            console.log("刷新...");
                        }} /></Tooltip>
                    </span>
                </div>
                <div>
                    {/* ConfigProvider 分页转成中文 */}
                    <ConfigProvider locale={zhCN}>
                        <Table
                        columns={columns}
                        dataSource={store.Search.Sdata}
                        rowSelection={rowSelection}
                        rowKey="id"
                        pagination={{
                            showSizeChanger: true,
                            showTotal: (total => `共${store.Search.searchcount}条`)
                        }}
                    />
                    </ConfigProvider>
                    
                </div>
            </div>
        </div>
    </div>
}
export default observer(Search)
