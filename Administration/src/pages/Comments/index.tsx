import React, { Key, useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite'
import useStore from '@/context/useStore';
import Search from '@/components/search';
import { styles } from './index.less'
import moment from 'moment'
import { Table, Button, Badge, Popover, Popconfirm, Space, Modal, Input } from 'antd'
import { IComments } from '@/types';
//搜索组件的传值
const pagestate = [
    {
        state: "未通过",
        pass: 0
    },
    {
        state: "已通过",
        pass: 1
    }
]
const pageStatus = { statusname: 'pass' }
const pageinput = [{
    label: "称呼",
    name: 'name',
    placeholder: "请输入页面称呼"
}, {
    label: "Email",
    name: 'email',
    placeholder: "请输入联系方式"
}]
const index: React.FC = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [val, setVal] = useState('')
    const [page, setPage] = useState(1)
    const [params, setParams] = useState({});
    const [current, setCurrent] = useState<IComments>();
    const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])
    const store = useStore()
    //表格数据
    const columns: any = [
        {
            title: '状态',
            dataIndex: 'pass',
            fixed: 'left',
            render: (text: string) => <>{text ? <span> <Badge status="success" />通过</span> : <span><Badge status="warning" />未通过</span>}</>,
        },
        {
            title: '称呼',
            dataIndex: 'name',

        },
        {
            title: '联系方式',
            dataIndex: 'email',
        },
        {
            title: '原始内容',
            dataIndex: 'content',
            render: (text: string) => <Popover content={text} title="评论详情-原始内容">
                <a >查看内容</a>
            </Popover>,
        },
        {
            title: 'HTML内容',
            dataIndex: 'content',
            render: (text: string) => <Popover content={text} title="评论详情-原始内容">
                <a >查看内容</a>
            </Popover>,
        },
        {
            title: '管理文章',
            dataIndex: 'url',
            render: (text: string) => <Popover content={<iframe
                src={`creationad.shbwyz.co${text}`}
            />} title="页面预览">
                <a href="#">文章</a>
            </Popover>,
        },
        {
            title: '创建时间',
            key: 'updateAt',
            dataIndex: 'updateAt',
            width: 200,
            render: (text: IComments) => moment(text.updateAt).format("YYYY-MM-DD HH:mm:ss")
        },
        {
            title: '父级评论'
        },
        {
            title: '操作',
            fixed: 'right',
            width: 300,
            render: (item: IComments) => <Space size='small'>
                <Button type="link" onClick={() => store.Comments.agree([item.id], true)} >通过</Button>
                <Button type="link" onClick={() => store.Comments.agree([item.id], false)}>拒绝</Button>
                <Button type="link" onClick={() => {
                    setIsModalVisible(true);
                    setCurrent(item)
                }
                }>回复</Button>
                <Popconfirm title="确认删除这个评论？" okText="Yes" cancelText="No" onConfirm={(e) => store.Comments.delComment([item.id])}>
                    <Button type="link">删除</Button>
                </Popconfirm>
            </Space>
        },
    ];
    function onSelectChange(selectedRowKeys: Key[]) {
        setSelectedRowKeys(selectedRowKeys)
    };

    useEffect(() => {
        store.Comments.getComment(page, params)
    }, [page, params])
    //搜索
    function SearchBtn(value: any) {
        let params: { [key: string]: string } = {};
        let obj: any = {}
        for (let key in value) {
            value[key] && (obj[key] = value[key] * 1 + "")
        }
        setParams(obj)
    }
    return <div>
        <Search pageStatus={pageStatus} pageInput={pageinput} pageState={pagestate} SearchBtn={SearchBtn} />
        <div style={{ background: ' rgb(255, 255, 255)', padding: '24px 12px' }}>
            <div style={{ marginBottom: 16 }}>
                {
                    selectedRowKeys.length > 0 && <Button onClick={() => store.Comments.agree(selectedRowKeys as string[], true)}>
                        通过
                    </Button>
                }
                <span style={{ marginLeft: 8 }}>
                    {selectedRowKeys.length > 0 && <Button onClick={() => store.Comments.agree(selectedRowKeys as string[], false)} >
                        拒绝
                    </Button>}
                </span>
                <span style={{ marginLeft: 8 }}>
                    {selectedRowKeys.length > 0 && <Button danger onClick={() => store.Comments.delComment(selectedRowKeys as string[])}>
                        删除
                    </Button>}
                </span>
            </div>
            <Table
                columns={columns}
                rowSelection={{
                    selectedRowKeys,
                    onChange: onSelectChange,
                }}
                dataSource={store.Comments.CommentDate}
                rowKey='id'
                scroll={{ x: 1500 }}
            />
        </div>
        <Modal title={`回复'${current?.name}'`}
            visible={isModalVisible}
            footer={
                <div>
                    <Button onClick={() => setIsModalVisible(false)}>取消</Button>
                    <Button onClick={() => console.log(val)}>确定</Button>
                </div>
            }>
            <Input.TextArea rows={4} value={val} onChange={(e) => setVal(e.target.value)} />
        </Modal>
    </div >
}
export default observer(index)