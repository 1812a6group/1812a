import React from 'react'
import useStore from '@/context/useStore';

import { IRegisterForm } from '@/types';
import { Form, Input, Button } from 'antd'
import { Link, useHistory, useLocation } from 'umi'
import { observer } from 'mobx-react-lite'
import { Modal, Space } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
const { confirm } = Modal;
import styles from './index.less'


const register: React.FC = (props) => {
    const store = useStore();
    const history = useHistory();
    const location = useLocation();
    console.log('store...', store, location);
    async function submit(values: IRegisterForm) {
        let result = await store.Register.register(values)
    }
    function showConfirm() {
        confirm({
            title: '注册成功',
            icon: <ExclamationCircleOutlined />,
            content: '是否跳转至登录？',
            onOk() {
                history.push("/login")
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }

    return <div className={styles.login}>
        <div className={styles.login_top}>
            <div className={styles.login_top_block}>
                <div></div>
            </div>
            <div className={styles.login_top_block}>
                <div className={styles.login_systemlogin}>
                    <h2>访客注册</h2>
                    <Form
                        onFinish={submit}
                        initialValues={{ name: 'admin', password: 'admin' }}
                    >
                        <Form.Item
                            name="name"
                            label="账号"
                            rules={[
                                { required: true, message: '请输入用户名' },
                                { pattern: /\w{5,12}/, message: '请输入合法的用户名' }
                            ]}
                        >
                            <Input type="text" placeholder="请输入用户名" />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            label="密码"
                            rules={[{ required: true, message: '请输入密码' }]}
                        >
                            <Input type="password" placeholder="请输入密码" />
                        </Form.Item>
                        <Form.Item
                            name="confirm"
                            label="确定"
                            rules={[{ required: true, message: '请再次输入密码' }]}
                        >
                            <Input type="confirm" placeholder="请再次输入密码" />
                        </Form.Item>
                        <Button htmlType="submit" className={styles.login_button} onClick={showConfirm}>注册</Button>
                        <p>
                            Or
                            <Link to="/login">去登录</Link>
                        </p>
                    </Form>

                </div>
            </div>
        </div>
        <ul className={styles.login_bottom}>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
    </div>
}
export default observer(register)