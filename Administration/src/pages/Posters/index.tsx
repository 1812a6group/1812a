import Search from '@/components/search';
import React from 'react';
import styles from './index.less';
import { Upload, message } from 'antd';
import { InboxOutlined } from '@ant-design/icons';
const index: React.FC = (Props) => {
    const { Dragger } = Upload;
    const pageinput = [{
        name: "name",
        label: "文件名称",
        placeholder: "请输入文件名称"
    }]
    function SearchBtn(value: string) {
        console.log('value', value);
    }
    const props = {
        name: 'file',
        multiple: true,
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        onChange(info: any) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
        onDrop(e: any) {
            console.log('Dropped files', e.dataTransfer.files);
        },
    };
    return (<div>
        <div className={styles.postContent}>
            <Dragger {...props}>
                <p className="ant-upload-drag-icon">
                    <InboxOutlined />
                </p>
                <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                <p className="ant-upload-hint">
                    点击选择文件或将文件拖拽到此处
                </p>
            </Dragger>
        </div>
        <Search pageInput={pageinput} SearchBtn={SearchBtn} />
    </div>
    )
}
export default index