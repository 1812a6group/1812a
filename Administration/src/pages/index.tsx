import useStore from '@/context/useStore';
import { Card, Row, Col, List, Skeleton } from 'antd';
import * as echarts from "echarts";
import React, { RefObject, useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import styles from "./index.less";

let eChartsRef: RefObject<HTMLDivElement> = React.createRef()

const indexPage: React.FC = (props) => {
  const store = useStore()
  useEffect(() => {
    const chart = echarts.init(eChartsRef.current!);   //echart初始化容器
    let option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross',
          crossStyle: {
            color: '#999',
            width: "100%",
            height: "100%"
          }
        }
      },
      toolbox: {
        feature: {
          dataView: { show: true, readOnly: false },
          magicType: { show: true, type: ['line', 'bar'] },
          restore: { show: true },
          saveAsImage: { show: true }
        }
      },
      legend: {
        data: ['评论数', '访问量']
      },
      xAxis: [
        {
          type: 'category',
          data: ['Mon', 'Tue', 'Web', 'Thu', 'Fri', 'Sta', 'Sun'],
          axisPointer: {
            type: 'shadow',
          }
        }
      ],
      yAxis: [
        {
          type: 'value',
          name: '每周客户访问指标',
          min: 0,
          max: 400,
          interval: 100,
          axisLabel: {
            formatter: '{value}',
          }
        },
        {
          type: 'value',
          min: 0,
          max: 400,
          interval: 100,
        },
      ],
      series: [
        {
          name: '评论数',
          type: 'bar',
          data: [110, 200, 150, 90, 80, 105, 120],
          color: '#C23531'
        },
        {
          name: '访问量',
          type: 'line',
          yAxisIndex: 1,
          data: [380, 295, 340, 110, 100, 120, 160]
        }


      ]
    };

    chart.setOption(option);
    window.onresize = function () {
      chart.resize()
    }
  }, []);
  let [loading, setloading] = useState(false)

  let list = [
    {
      gender: "male",
      name: {
        title: "Mr",
        first: "Marcus",
        last: "White"
      },
      email: "marcus.white@example.com",
      nat: "NZ"
    }
  ]
  useEffect(() => {
    store.Article.getArticle()
  }, [])


  return (
    <div className={styles.workbench}>
      <Card title="面板导航" style={{ width: '100%', height: '500px' }}>
        <div ref={eChartsRef} style={{ width: '100%', height: '400px' }}>

        </div>
      </Card>

      <Card className={styles.Card_Top} title="快速导航" style={{ width: '100%' }}>
        <Row gutter={[16, 24]} >
          <Col span={4} style={{ textAlign: 'center' }}><NavLink style={{ color: '#000' }} to='/article'>文章管理</NavLink></Col>
          <Col span={4} style={{ textAlign: 'center' }}><NavLink style={{ color: '#000' }} to='/comment'>评论管理</NavLink></Col>
          <Col span={4} style={{ textAlign: 'center' }}><NavLink style={{ color: '#000' }} to='/file'>文件管理</NavLink></Col>
          <Col span={4} style={{ textAlign: 'center' }}><NavLink style={{ color: '#000' }} to='/user'>用户管理</NavLink></Col>
          <Col span={4} style={{ textAlign: 'center' }}><NavLink style={{ color: '#000' }} to='/Styleistical'>访问管理</NavLink></Col>
          <Col span={4} style={{ textAlign: 'center' }}><NavLink style={{ color: '#000' }} to='/system'>系统设置</NavLink></Col>
        </Row>
      </Card>
      <Card className={styles.Card_Top} title="最新文章" style={{ width: '100%' }} extra={<NavLink to="/article">全部文章</NavLink>}>
        {
          console.log(store.Article.data)

        }
        {
          store.Article.data.map((item, index) => {


            return <Card.Grid key={index} style={{ textAlign: 'center', width: '33.3%' }}>

              <div style={{ width: '100%', height: '110px' }}>
                <img src={item.cover!} alt="文章封面" style={{ width: '100%', height: '100%' }} />
              </div>

              <p> {item.title}</p>
            </Card.Grid>
          })
        }


      </Card>

      <Card className={styles.Card_Top} title="最新评论" style={{ width: '100%' }} extra={<NavLink to="/comment">全部评论</NavLink>}>
        <List
          className="demo-loadmore-list"
          itemLayout="horizontal"

          dataSource={list}
          renderItem={item => (
            <List.Item
              actions={[<a key="list-loadmore-edit">edit</a>, <a key="list-loadmore-more">more</a>]}
            >
              <Skeleton avatar title={false} loading={loading} active>
                <List.Item.Meta

                  title={<a href="https://ant.design">{123}</a>}
                  description="Ant Design, a design language for background applications, is refined by Ant UED Team"
                />
                <div>content</div>
              </Skeleton>
            </List.Item>
          )}
        />
      </Card>

    </div>
  );
}
export default indexPage

