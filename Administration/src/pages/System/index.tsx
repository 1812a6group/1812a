import React, { useEffect, useState } from 'react'
import { Tabs } from 'antd';
import { Form, Col, Input, Button } from 'antd';

import { Alert } from 'antd';

import styles from './index.less';
import useStore from '@/context/useStore';
import { SystemItem } from '@/types';
const { TabPane } = Tabs;
const { TextArea } = Input;

const system: React.FC = (props) => {
   let newTabIndex = 0;
    const store = useStore()
    const [form] = Form.useForm();
    useEffect(() => {
        store.System.getSystem()
    }, [])
    const initialPanes = [
        { title: 'Tab 1', content: 'Content of Tab 1', key: '1' },
        { title: 'Tab 2', content: 'Content of Tab 2', key: '2' },
        {
            title: 'Tab 3',
            content: 'Content of Tab 3',
            key: '3',
            closable: false,
        },
    ];
    const onchangetab = (activeKey: string) => {
        // useEffect(() => {
        //     activeKey
        // }, [activeKey])
        console.log(111);
    }

    const onFinish = (values: Partial<SystemItem>) => {
        store.System.getSetting(values)
    };
    function callback(key: string) {
        console.log(key);
    }
    const [activeKey, setActiveKey] = useState(initialPanes[0].key)
    const [panes, setPanes] = useState(initialPanes)
    const onChange = (activeKey: any) => {
        setActiveKey(activeKey);
        // console.log(activeKey);
    };
    const onEdit = (targetKey: any, action: string | number) => {
        if (action === "add") {
            showModal()

        } else if (action === "remove") {
            if (window.confirm("确认删除吗")) {
                remove(targetKey)
            }
        }
        // console.log(action,targetKey);
    };

    // const add = () => {
    //     const activeKey = `newTab${newTabIndex++}+1`;
    //     const newPanes = [...panes];
    //     console.log(newPanes);

    //     newPanes.push({
    //         title: value, content: <MonacoEditor
    //             height={450}
    //             language='json'
    //             options={options as any}
    //             value={'{}'}
    //         />, key: new Date() + 'x'
    //     } as any);
    //     setPanes(newPanes);
    //     setActiveKey(activeKey)
    //     setValue('')
    // };
    const remove = (targetKey: any) => {

        let newActiveKey = activeKey;
        let lastIndex = 0;
        panes.forEach((pane, i) => {
            if (pane.key === targetKey) {
                lastIndex = i - 1;
            }
        });
        const newPanes = panes.filter(pane => pane.key !== targetKey);
        if (newPanes.length && newActiveKey === targetKey) {
            if (lastIndex >= 0) {
                newActiveKey = newPanes[lastIndex].key;
            } else {
                newActiveKey = newPanes[0].key;
            }
        }
        setPanes(newPanes);
        setActiveKey(activeKey)
    }

    //弹框
    let val: string
    const [isModalVisible, setIsModalVisible] = useState(false);
    // const setVal = (e: React.ChangeEvent<HTMLInputElement>) => {
    //     setValue(e.target.value)
    // }
    const showModal = () => {
        setIsModalVisible(true);
        // ref.current ? ref.current!.value = ' ' : setValue(' ')
    };

    // const handleOk = () => {
    //     setIsModalVisible(false);
    //     if (value !== '') {
    //         add()
    //     }

    // };

    const handleCancel = () => {
        setIsModalVisible(false);
    };



    return <div style={{
        backgroundColor: "#fff",
        padding: 16
    }}>
        <Tabs defaultActiveKey="1" onChange={callback} tabPosition="left">
            <TabPane tab="系统设置" key="1">
                <Form form={form} name="advanced_search" className={styles.form} onFinish={onFinish} layout="vertical" key='form'>
                    <Form.Item name='systemUrl' key='systemUrl' label='系统地址'>
                        <Input placeholder="请输入系统地址" defaultValue={store.System.SystemDate.systemUrl} />
                    </Form.Item>
                    <Form.Item name='adminSystemUrl' key='adminSystemUrl' label='后台地址'>
                        <Input placeholder="请输入后台地址" defaultValue={store.System.SystemDate.adminSystemUrl} />
                    </Form.Item>
                    <Form.Item name='systemTitle' key='systemTitle' label='系统标题'>
                        <Input placeholder="请输入系统标题" defaultValue={store.System.SystemDate.systemTitle} />
                    </Form.Item>
                    <Form.Item name='systemLogo' key='systemLogo' label='Logo'>
                        <Input placeholder="请输入Logo" defaultValue={store.System.SystemDate.systemLogo} />
                    </Form.Item>
                    <Form.Item name='systemFavicon' key='systemFavicon' label='Favicon'>
                        <Input placeholder="Favicon" defaultValue={store.System.SystemDate.systemFavicon} />
                    </Form.Item>

                    <Form.Item name='systemFooterInfo' key='systemFooterInfo' label='页脚信息'>
                        <TextArea rows={6} placeholder="请输入页脚信息" defaultValue={store.System.SystemDate.systemFooterInfo} />
                    </Form.Item>
                    <Col span={24} key='col'>
                        <Button type="primary" htmlType="submit">保存</Button>
                    </Col>

                </Form>
            </TabPane>
            <TabPane tab="国际化设置" key="2">
                <Tabs
                    type="editable-card"
                    onChange={onChange}
                    activeKey={activeKey}
                    onEdit={onEdit}
                >
                    {panes.map(pane => (
                        <TabPane tab={pane.title} key={pane.key} closable={pane.closable}>
                            {pane.content}
                        </TabPane>
                    ))}
                </Tabs>
            </TabPane>
            <TabPane tab="SEO设置" key="3">
                <Form form={form} name="advanced_search" className={styles.form} onFinish={onFinish} layout="vertical" key='form'>
                    <Form.Item name='seoKeyword' key='seoKeyword' label='关键词'>
                        <Input placeholder="请输入关键词" defaultValue={store.System.SystemDate.seoKeyword} />
                    </Form.Item>

                    <Form.Item name='seoDesc' key='seoDesc' label='描述信息'>
                        <TextArea rows={6} placeholder="请输入描述信息" defaultValue={store.System.SystemDate.seoDesc} />
                    </Form.Item>
                    <Col span={24} key='col'>
                        <Button type="primary" htmlType="submit">保存</Button>
                    </Col>
                </Form>
            </TabPane>
            <TabPane tab="数据统计" key="4">
                <Form form={form} name="advanced_search" className={styles.form} onFinish={onFinish} layout="vertical" key='form'>
                    <Form.Item name='baiduAnalyticsId' key='baiduAnalyticsId' label='百度统计'>
                        <Input placeholder="请输入百度统计" defaultValue={store.System.SystemDate.baiduAnalyticsId} />
                    </Form.Item>

                    <Form.Item name='googleAnalyticsId' key='googleAnalyticsId' label='描述信息'>
                        <TextArea rows={6} placeholder="请输入描述信息" defaultValue={store.System.SystemDate.googleAnalyticsId} />
                    </Form.Item>
                    <Col span={24} key='col'>
                        <Button type="primary" htmlType="submit">保存</Button>
                    </Col>
                </Form>
            </TabPane>
            <TabPane tab="OSS设置" key="5">
                <Alert
                    message="说明"
                    description="请在编辑器中输入您的 oss 配置，并添加 type 字段区分"
                    type="info"
                    showIcon
                />
                {store.System.SystemDate.oss}
            </TabPane>
            <TabPane tab="SMTP服务" key="6">
                <Form form={form} name="advanced_search" className={styles.form} onFinish={onFinish} layout="vertical" key='form'>
                    <Form.Item name='smtpHost' key='smtpHost' label='SMTP 地址'>
                        <Input placeholder="请输入SMTP 地址" defaultValue={store.System.SystemDate.smtpHost} />
                    </Form.Item>
                    <Form.Item name='smtpPort' key='smtpPort' label='SMTP 端口（强制使用 SSL 连接）'>
                        <Input placeholder="请输入SMTP 端口" defaultValue={store.System.SystemDate.smtpPort} />
                    </Form.Item>
                    <Form.Item name='smtpUser' key='smtpUser' label='SMTP 用户'>
                        <Input placeholder="请输入SMTP 用户" defaultValue={store.System.SystemDate.smtpUser} />
                    </Form.Item>
                    <Form.Item name='smtpPass' key='smtpPass' label='SMTP 密码'>
                        <Input placeholder="请输入SMTP密码" defaultValue={store.System.SystemDate.smtpPass} />
                    </Form.Item>
                    <Form.Item name='smtpFromUser' key='smtpFromUser' label='发件人'>
                        <Input placeholder="请输入发件人" defaultValue={store.System.SystemDate.smtpFromUser} />
                    </Form.Item>

                    <Col span={24} key='col'>
                        <Button type="primary" htmlType="submit">保存</Button>
                    </Col>

                </Form>
            </TabPane>
        </Tabs>
    </div >
}
export default system