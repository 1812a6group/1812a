import React from 'react'
import useStore from '@/context/useStore';
import { ILoginForm } from '@/types';
import { Form, Input, Button } from 'antd'
import { Link, useHistory, useLocation } from 'umi'
import { observer } from 'mobx-react-lite'
import styles from './index.less'
// import classnames from 'classnames'
import { setToken } from '@/utils';

interface ILocationQuery {
    query: {
        from: string;
    }
}

const login: React.FC = (props) => {
    const store = useStore();
    const history = useHistory();
    const location = useLocation();
    console.log('store...', store, location);
    async function submit(values: ILoginForm) {
        let result = await store.user.login(values)
        if (Boolean(result)) {
            let redirect = '/';

            if ((location as unknown as ILocationQuery).query.from) {
                redirect = decodeURIComponent((location as unknown as ILocationQuery).query.from);
            }
            localStorage.setItem("user", JSON.stringify(result));
            setToken(result.token)
            history.replace(redirect);
        }
    }


    return <div className={styles.login}>
        <div className={styles.login_top}>
            <div className={styles.login_top_block}>
                <div></div>
            </div>
            <div className={styles.login_top_block}>
                <div className={styles.login_systemlogin}>
                    <h2>系统登录</h2>
                    <Form
                        onFinish={submit}
                        initialValues={{ name: 'admin', password: 'admin' }}
                    >
                        <Form.Item
                            name="name"
                            label="账号"
                            rules={[
                                { required: true, message: '请输入用户名' },
                                { pattern: /\w{5,12}/, message: '请输入合法的用户名' }
                            ]}
                        >
                            <Input type="text" placeholder="请输入用户名" />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            label="密码"
                            rules={[{ required: true, message: '请输入密码' }]}
                        >
                            <Input type="password" placeholder="请输入密码" />
                        </Form.Item>
                        <Button htmlType="submit" className={styles.login_button}>登陆</Button>
                        <p className={styles.login_p}>
                            Or
                            <Link to="/register">注册用户</Link>
                        </p>
                    </Form>

                </div>
            </div>
        </div>
        <ul className={styles.login_bottom}>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
    </div>
}
export default observer(login)