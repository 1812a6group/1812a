import { ReloadOutlined } from '@ant-design/icons'
import { Table, Badge, Popconfirm, Tooltip, Button, Form, Input,ConfigProvider } from 'antd'
import React, { useState } from 'react';
import { fomatTime } from '@/utils/times';
import styles from "./index.less"
import { useEffect, Key } from 'react';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import { IStyle } from '@/types/styleistical';
import zhCN from 'antd/lib/locale/zh_CN';
const index: React.FC = (props) => {
  const store = useStore()
  const [page, setPage] = useState(1)
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([])
  const [params, setParams] = useState({})
  const [form] = Form.useForm();
  // 获取访问列表数据
  useEffect(() => {
    store.Styleistical.getStyle(page, params)
  }, [page, params]);
  // 条件查询
  function submit() {
    let values = form.getFieldsValue();
    let params: { [key: string]: string } = {};
    for (let key in values) {
      values[key] && (params[key] = values[key]);
    }
    setParams(params);
    setPage(1);
    console.log("搜索成功...");
  }
  // 复选框
  function onSelectChange(selectedRowKeys: Key[], selectedRows: IStyle[]) {
    setSelectedRowKeys(selectedRowKeys);
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange
  }
  const columns = [
    {
      title: 'URL',
      width: 100,
      dataIndex: 'url',
      key: 'name',
      fixed: 'left',
      render: (text: any) => (<a href={`${text}`}>{text}</a>)
    },
    { title: 'IP', dataIndex: 'ip', key: '1' },
    { title: '浏览器', dataIndex: 'browser', key: '2' },
    { title: '内核', dataIndex: 'engine', key: '3' },
    { title: '操作系统', dataIndex: 'os', key: '4' },
    { title: '设备', dataIndex: 'address', key: '5' },
    { title: '地址', dataIndex: 'address', key: '6' },
    {
      title: '访问量', dataIndex: 'count', key: '7',
      render: (text: any,) => (
        <Badge
          className="site-badge-count-109"
          count={text}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '访问时间', dataIndex: 'createAt', key: '8', render: (text: any) => (
        <>{fomatTime(text)}</>
      ),
    },
    {
      title: '操作',
      key: 'operation',
      fixed: 'right',
      width: 100,
      render: (item: IStyle) => (
        <Popconfirm title="确认删除这个搜索记录？"
          onConfirm={() => store.Styleistical.deteleStyle([item.id])}
          okText="确定"
          cancelText="取消"
        >
          <a>删除</a>
        </Popconfirm>

      ),
    },
  ];
  return <div>
    <div className={styles.cont}>
      <div className={styles.cont_1}>
        <Form className={styles.cont_input}
          form={form}
          onFinish={submit}>
          <Form.Item
            label="IP"
            name="ip"
            className={styles.Form_item}>
            <Input type="text" placeholder="请输入IP地址" />
          </Form.Item>
          <Form.Item
            label="UA"
            name="from"
            className={styles.Form_item}>
            <Input type="text" placeholder="请输入ui" />
          </Form.Item>
          <Form.Item
            label="URL"
            name="url"
            className={styles.Form_item}>
            <Input type="text" placeholder="请输入url" />
          </Form.Item>
          <Form.Item
            label="地址"
            name="address"
            className={styles.Form_item}>
            <Input type="text" placeholder="请输入地址" />
          </Form.Item>
          <Form.Item
            label="浏览器"
            name="browser"
            className={styles.Form_item}>
            <Input type="text" placeholder="请输入浏览器" />
          </Form.Item><Form.Item
            label="内核"
            name="engine"
            className={styles.Form_item}>
            <Input type="text" placeholder="请输入内核" />
          </Form.Item><Form.Item
            label="OS"
            name="os"
            className={styles.Form_item}>
            <Input type="text" placeholder="请输入操作系统" />
          </Form.Item><Form.Item
            label="设备"
            name="address"
            className={styles.Form_item}>
            <Input type="text" placeholder="请输入设备" />
          </Form.Item>
          <div className={styles.cont_btn}>
            <Button className={styles.cont_btn_01} htmlType="submit">搜索</Button>
            <Button className={styles.cont_btn_02} htmlType="reset">重置</Button>
          </div>
        </Form>

      </div>
      <div className={styles.cont_2}>
        <div className={styles.cont_head}>
          <span>
            <Popconfirm title="确认删除？"
              onConfirm={() => store.Styleistical.deteleStyle(selectedRowKeys as string[])}
              okText="确定"
              cancelText="取消"
            >
              {selectedRowKeys.length ? <Button danger>删除</Button> : ''}
            </Popconfirm>
          </span>
          <span>
            <Tooltip title="刷新"><ReloadOutlined /></Tooltip>
          </span>
        </div>
        <div>
        <ConfigProvider locale={zhCN}>
          <Table
            columns={columns as any}
            dataSource={store.Styleistical.stylelist}
            rowSelection={rowSelection}
            pagination={{ 
              showSizeChanger: true,
              showTotal: (total => `共${store.Search.searchcount}条`)
            }}
            rowKey='id'
            scroll={{ x: 1300 }}
          />
        </ConfigProvider>
        </div>
      </div>
    </div>
  </div>
}
export default observer(index)