import React, { useEffect, useState } from 'react';
import styles from './index.less';
import Search from '@/components/search';
import { Button, Card, Tooltip, List, Popconfirm, Drawer, Input, Upload, Form, Switch, message } from 'antd';
import { PlusOutlined, EditOutlined, CloudUploadOutlined, SettingOutlined, DeleteOutlined, CloudDownloadOutlined, InboxOutlined, UploadOutlined } from '@ant-design/icons';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import { IComments, IFile } from '@/types';
import { KnewledgeItem } from '@/types';
import { useRef } from 'react';
const { Dragger } = Upload;
const { Meta } = Card;
const pageinput = [{
    name: 'title',
    label: "名称",
    placeholder: "请输入知识库名称"
}]
const pageStatus = { statusname: 'status' }
const pagestate = [{
    state: "已发布",
    pass: 'publish'
}, {
    state: "草稿",
    pass: 'draft'
}]

const pageinput2 = [{
    label: "文件名称",
    name: 'originalname',
    placeholder: "请输入文件名称"
}, {
    label: "文件类型",
    name: 'type',
    placeholder: "请输入文件类型"
}]
const Knowledge: React.FC = (props) => {
    const store = useStore();
    const [page, setPage] = useState(1);
    const [pageSize, setPageSize] = useState(12);
    const [visible, setVisible] = useState(false);
    const [childrenDrawer, setchildrenDrawer] = useState(false);
    const [data, setData] = useState({} as IFile);
    const [btnflag, setBtnflag] = useState(false);
    const [values, setValues] = useState<Partial<KnewledgeItem>>({})
    const ref = useRef<Partial<KnewledgeItem>>({})
    const changevalue = (e: string) => {
        if (e.length > 0) {
            setBtnflag(true)
        } else {
            setBtnflag(false)
        }
    }
    useEffect(() => {
        store.Comments.getFile(page)
    }, [page])

    useEffect(() => {

    }, [ref.current])
    useEffect(() => {
        store.Know.getKnowledge(page)
    }, [page]);
    const onFinish = (values: any) => {
        store.Know.getKnowNewFile(values);
    };

    const showChildrenDrawer = () => {
        setchildrenDrawer(!childrenDrawer)
    };

    const onChildrenDrawerClose = () => {
        setchildrenDrawer(!childrenDrawer)
    };
    const showDrawer2 = (item: IFile) => {
        setData(item)
        setVisible(true);
    };
    const showDrawer = () => {
        setVisible(!visible);
    };
    const showDrawer3 = async (item: KnewledgeItem) => {
        setVisible(!visible);
        ref.current = item
        // setValues(item.title)

        console.log(item.title, ref.current.title, '222222');
    };

    const onFinishFailed = () => {
        setVisible(!visible);

    }
    const onClose = () => {
        setVisible(!visible);

    };


    function SearchBtn(value: KnewledgeItem) {
        store.Know.getKnowStatus(value)
    };
    return <div className={styles.know}>
        <Search pageStatus={pageStatus} pageInput={pageinput} pageState={pagestate} SearchBtn={SearchBtn} />
        <div className={styles.know_content}>
            <div className={styles.know_content_right}>
                <Button type="primary" icon={<PlusOutlined />} onClick={showDrawer}>
                    新建
                </Button>
            </div>
            <List
                grid={{ gutter: 16, column: 4 }}
                dataSource={store.Know.knowList}
                pagination={{
                    total: store.Know.knowtotal,
                    onChange: page => {
                        setPage(page)
                    },
                    pageSizeOptions: ['8', '12', '24', '36'],
                    showSizeChanger: true,
                    defaultPageSize: 12,
                    showTotal: total => `共${total}条`
                }}
                renderItem={item => (
                    <List.Item>
                        <Card className={styles.content_img}
                            cover={
                                <div className={styles.content_imgbox}>
                                    <img
                                        alt={item.title}
                                        src={item.cover}
                                    />
                                </div>

                            }
                            actions={[
                                <EditOutlined key="edit" />,
                                item.status === "publish"
                                    ?
                                    <Tooltip title="设为草稿">
                                        <CloudDownloadOutlined onClick={() => store.Know.getKnowpublish(item.id, "draft")} />
                                    </Tooltip>
                                    :
                                    <Tooltip title="发布上线">
                                        <CloudUploadOutlined onClick={() => store.Know.getKnowpublish(item.id, "publish")} />
                                    </Tooltip>
                                ,
                                <SettingOutlined key="setting" onClick={() => showDrawer3(item)} />,
                                <Popconfirm
                                    title="确定删除？"
                                    onConfirm={() => store.Know.getKnowDelete(item.id)
                                    }
                                    okText="Yes"
                                    cancelText="No"
                                >
                                    <DeleteOutlined />
                                </Popconfirm>

                            ]}
                        >
                            <Meta
                                title={item.title}
                                description={item.summary}
                            />
                        </Card>
                    </List.Item>
                )}
            />
        </div >
        <>
            <Drawer title="新建知识库" placement="right" onClose={onClose} visible={visible} width={600}>
                <Form
                    name="basic"
                    // labelCol={{ span: 8 }}
                    // wrapperCol={{ span: 16 }}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"

                >
                    <Form.Item name='title' label="名称" colon={false} >
                        <Input onChange={e => changevalue(e.target.value)} defaultValue={ref.current.title} />
                    </Form.Item>
                    <Form.Item name='summary' label="描述" colon={false}>
                        <Input.TextArea value="ads" />
                    </Form.Item>
                    <Form.Item name="isCommentable" label="评论" valuePropName="checked" colon={false}>
                        <Switch />
                    </Form.Item>
                    <Form.Item label="封面" valuePropName="checked" colon={false} className={styles.know_newfile_inp}>
                        <Dragger {...props}>
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                            <p className="ant-upload-hint">
                                点击选择文件或将文件拖拽到此处
                            </p>
                        </Dragger>
                        <Input className={styles.know_newfile_btn} name="cover" />
                        <Button className={styles.know_newfile_btn} onClick={showChildrenDrawer}>选择文件</Button>
                        <Drawer title="文件选择" placement="right" onClose={onChildrenDrawerClose} visible={childrenDrawer} width={786}>
                            <Search pageInput={pageinput2} SearchBtn={SearchBtn} />
                            <div className={styles.know_upload}>
                                <Upload {...props} >
                                    <Button>上传文件</Button>
                                </Upload>
                            </div>
                            <List
                                grid={{ gutter: 16, column: 4 }}
                                dataSource={store.Comments.FileData}
                                pagination={{
                                    total: store.Comments.FileCount,
                                    onChange: page => {
                                        setPage(page)
                                    },
                                    onShowSizeChange: (current, size) => {
                                        setPageSize(size)
                                    },
                                    pageSizeOptions: ['8', '12', '24', '36'],
                                    showSizeChanger: true,
                                    defaultPageSize: 12,
                                    showTotal: total => `共 ${total}条`
                                }}
                                renderItem={item => (
                                    <div onClick={() => showDrawer2(item)} className={styles.list}>
                                        <List.Item>
                                            <Card
                                                cover={
                                                    <div className={styles.box}>
                                                        <img
                                                            alt="example"
                                                            src={item.url}
                                                        />
                                                    </div>
                                                }
                                            >
                                                <Meta
                                                    title={item.originalname}
                                                />
                                            </Card>
                                        </List.Item>
                                    </div>
                                )}
                            />
                        </Drawer>
                    </Form.Item>
                    <div className={styles.know_content_footbtn}>
                        <Button onClick={onClose}>
                            取消
                        </Button>
                        <Button type="primary" htmlType="submit" className={styles.know_content_btn2} disabled={btnflag ? false : true}>
                            创建
                        </Button>
                    </div>
                </Form>
            </Drawer>
        </>
    </div >
}
export default observer(Knowledge);