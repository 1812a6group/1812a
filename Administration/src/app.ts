import { RequestConfig, history } from "umi"
import { message } from 'antd';
import StateContext from '@/context/stateContext';
import store from '@/models';
import { getToken } from './utils';
import React from "react";

// 全局路由切换配置
const whiteList = ['/login', '/register']
export function onRouteChange({ matchedRoutes, routes, location }: any) {
    let authorization = getToken();

    if (authorization !== 'Bearer undefined') {
        if (location.pathname === '/login') {
            history.replace('/');
        }
    } else {
        if (whiteList.indexOf(location.pathname) === -1) {
            history.replace('/login?from=' + encodeURIComponent(location.pathname));
        }
    }
}
// 覆盖根组件
export function rootContainer(container: React.ReactNode) {
    return React.createElement(StateContext.Provider, { value: store }, container);
}
// 网络请求配置
let showError = false;
const baseUrl = 'https://creationapi.shbwyz.com/';
export const request: RequestConfig = {
    timeout: 100000,
    errorConfig: {
    },
    middlewares: [],
    requestInterceptors: [(url, options) => {
        let authorization = getToken();
        if (authorization) {
            options = { ...options, headers: { authorization } };
        }
        return {
            url: `${baseUrl}${url}`,
            options,
        };
    }],

    responseInterceptors: [response => {
        const codeMaps: { [key: number]: string } = {
            400: '用户已存在',
            403: '禁止访问',
            404: '找不到资源',
            500: '服务器内部错误',
            502: '网关错误。',
            503: '服务不可用，服务器暂时过载或维护。',
            504: '网关超时。',
        };
        if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1) {
            if (!showError) {
                showError = true;
                message.error({
                    content: codeMaps[response.status],
                    onClose: () => showError = false
                });
            }
        }
        return response;
    }],
};
