export interface IArticle {
  id: string;
  title: string;
  cover?: string;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  totalAmount?: string;
  isPay: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt?: string;
  updateAt?: string;
  tags: any[];
  category?: any;
key:string
}
export interface ICategory {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
export interface ITag {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}
