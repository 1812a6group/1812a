export interface ManageItem {
    id: string;
    name: string;
    avatar?: string;
    email?: any;
    role: string;
    status: string;
    createAt: string;
    updateAt: string;
}