export interface ISearch {
  id: string;
  type: string;
  keyword: string;
  count: number;
  createAt: string;
  updateAt: string;
}

export interface PageState {
  state: string,
  pass: number | string
}
export interface PageInput {
  name: string,
  placeholder: string,
  label: string
}
export interface PageStatus {
  statusname: string
}
