export interface IRegisterForm {
    name: string;
    password: string;
    confirm: string;
}