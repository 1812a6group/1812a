export interface IComments {
    id: string;
    name: string;
    email: string;
    content: string;
    html: string;
    pass: boolean;
    userAgent: string;
    hostId: string;
    url: string;
    parentCommentId?: string;
    replyUserName?: string;
    replyUserEmail?: string;
    createAt: string;
    updateAt: string;
}
export interface ICommentsItem {
    name: string,
    email: string,
    pass: number
}
//文件管理数据
export interface IFile {
    id: string;
    originalname: string;
    filename: string;
    type: string;
    size: number;
    url: string;
    createAt: string;
}
export interface IFileItem {
    originalname: string,
    type: string
}
export interface Ireply {
    name: string;
    email: string;
    content: string;
    parentCommentId: string;
    hostId: string;
    replyUserName: string;
    replyUserEmail: string;
    url: string;
    createByAdmin: boolean;
}
export interface IMail {
    id: string;
    from: string;
    to: string;
    subject: string;
    text: string;
    html: string;
    createAt: string;
}