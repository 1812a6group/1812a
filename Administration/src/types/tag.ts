export interface ITagItem {
    id: string;
    label: string;
    value: string;
    createAt: string;
    updateAt: string;
    articleCount: number;
  }
  