
export interface IStyle {
  id: string;
  ip: string;
  userAgent: string;
  url: string;
  count: number;
  address: string;
  browser: string;
  engine: string;
  os: string;
  device: string;
  createAt: string;
  updateAt: string;
}




