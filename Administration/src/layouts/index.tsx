
import { Layout, Avatar, Dropdown, Menu, Button, Breadcrumb } from 'antd';
import styles from './index.less';
const whiteList = ['/login', '/register', '/article/editor', '/article/mdeditor']
import { RouteComponentProps } from 'react-router-dom';
import './index.less';
import 'antd/dist/antd.less';
import { useLocation, NavLink, history } from 'umi';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  SnippetsOutlined,
  BookOutlined,
  AimOutlined,
  MessageOutlined,
  MailOutlined,
  FolderOpenOutlined,
  SearchOutlined,
  ProjectOutlined,
  UserOutlined,
  SettingOutlined,
  DashboardOutlined,
  FormOutlined,
  CopyOutlined,
  TagOutlined,
  GithubFilled,
  GithubOutlined,
  CopyrightOutlined
} from '@ant-design/icons'; //引入icon图标
import { removeToken, setToken } from '@/utils';
import useStore from '@/context/useStore';
import React, { useEffect, useState } from 'react';

const { SubMenu } = Menu;
const { Header, Footer, Sider, Content } = Layout;
const index: React.FC<RouteComponentProps> = (props) => {
  let store = useStore()
  const location = useLocation();
  console.log(location);
  useEffect(() => {
    store.Article.getArticle()
  }, [])
  const [collapsed, setCollapsed] = useState(false);

  function toggle() {
    setCollapsed(!collapsed);
  }
  if (whiteList.indexOf(location.pathname) !== -1) {
    return <>{props.children}</>

  }


  const menu = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
          个人中心
        </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
          用户管理
        </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
          系统设置
        </a>
      </Menu.Item>
      <Menu.Item>
        <span onClick={() => {
          removeToken()
          history.replace('/login?from=' + encodeURIComponent(location.pathname))
        }}>
          退出登录
        </span>
      </Menu.Item>
    </Menu>
  );
  const menus = [
    {
      icon: <DashboardOutlined />,
      link: '/',
      content: '工作台',
      id: 1,
      children: [
        {
          icon: <FormOutlined />,
          content: '文章管理',
          id: 2,
          children: [
            {
              icon: <FormOutlined />,
              link: '/article',
              content: '所有文章',
              id: 2,
              children: [
                {
                  icon: <CopyOutlined />,
                  link: '/article/classify',
                  content: '分类管理',
                  id: 2.1,
                },
                {
                  icon: <TagOutlined />,
                  link: '/article/label',
                  content: '标签管理',
                  id: 2.2,
                },
              ],
            },
          ],
        },
        {
          icon: <SnippetsOutlined />,
          link: '/pageManagement',
          content: '页面管理',
          id: 3,
        },
        {
          icon: <BookOutlined />,
          link: '/knowledge',
          content: '知识小册',
          id: 4,
        },
        {
          icon: <AimOutlined />,
          link: '/posters',
          content: '海报管理',
          id: 5,
        },
        {
          icon: <MessageOutlined />,
          link: '/comments',
          content: '评论管理',
          id: 6,
        },
        {
          icon: <MailOutlined />,
          link: '/mail',
          content: '邮件管理',
          id: 7,
        },
        {
          icon: <FolderOpenOutlined />,
          link: '/file',
          content: '文件管理',
          id: 8,
        },
        {
          icon: <SearchOutlined />,
          link: '/search',
          content: '搜索记录',
          id: 9,
        },
        {
          icon: <ProjectOutlined />,
          link: '/Styleistical',
          content: '访问统计',
          id: 10,
        },
        {
          icon: <UserOutlined />,
          link: '/Manage',
          content: '用户管理',
          id: 11,
        },
        {
          icon: <SettingOutlined />,
          link: '/system',
          content: '系统设置',
          id: 12,
        },
      ],
    },
  ];
  const menu1 = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href='/article/mdeditor'>
          新建文章-协同编辑器
        </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href='/article/editor'>
          新建文章
        </a>
      </Menu.Item>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href="/article/editor">
          新建页面
        </a>
      </Menu.Item>
    </Menu>
  )
  return (
    <Layout className={styles.container}>
      <Sider
        className={styles.Left}
        trigger={null}
        collapsible
        collapsed={collapsed}
      >
        <div className={styles.Left_top}>
          <img
            src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png"
            alt=""
          />
          <span className={collapsed ? "none" : ""}>管理后台</span>
        </div>
        <div className={styles.Left_newBtn}>
          <Dropdown overlay={menu1} placement="bottomLeft">
            <Button type="primary" size="large" style={{ width: '100%' }}>
              <span>+</span> <p className={collapsed ? "none" : "block"}>新建</p>
            </Button>
          </Dropdown>

        </div>
        <div className={styles.Left_bot}>
          <Menu mode="inline" theme="dark" defaultSelectedKeys={['1']}>
            <Menu.Item icon={menus[0].icon} key={menus[0].id}>
              <NavLink to={menus[0].link}>{menus[0].content} </NavLink>
            </Menu.Item>
            {menus[0].children.map((item, index) => {
              return item.children ? (
                <SubMenu key="sub1" icon={item.icon} title={item.content}>
                  <Menu.Item icon={item.icon}>
                    <NavLink to={item.children[0].link}>
                      {item.children[0].content}
                    </NavLink>
                  </Menu.Item>
                  {item.children[0].children.map((item1) => {
                    return (
                      <Menu.Item icon={item1.icon} key={item1.id}>
                        <NavLink to={item1.link}>{item1.content} </NavLink>
                      </Menu.Item>
                    );
                  })}
                </SubMenu>
              ) : (
                <Menu.Item icon={item.icon} key={item.id}>
                  <NavLink to={item.link}> {item.content}</NavLink>
                </Menu.Item>
              );
            })}
          </Menu>
        </div>
      </Sider>
      <Layout>
        <Header style={{ padding: 0, background: '#fff' }}>
          <div className={styles.header_left}>
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: 'trigger',
                onClick: toggle,
              },
            )}
          </div>
          <div className={styles.header_right}>
            <div><GithubFilled /></div>
            <div className={styles.header_user}>
              <Dropdown overlay={menu} placement="bottomLeft">
                <Avatar size={24} icon={<UserOutlined />} />
              </Dropdown>
              <span style={{ padding: 10 }}>Hi,{JSON.parse(localStorage.getItem("user")!).name}</span>
            </div>
          </div>
        </Header>
        <Content>
          <header className={styles.Content_header}>
            {
              props.location.pathname == '/' ? <div >
                <h1 className={styles.welcome_h1}>
                  您好 ，{JSON.parse(localStorage.getItem("user")!).name}
                </h1>
                <p >
                  您的角色： 管理员
                </p>
              </div> : <Breadcrumb className={styles.layouts_work}>
                <Breadcrumb.Item key="工作台" ><NavLink to='/'>工作台</NavLink></Breadcrumb.Item>
                {menus[0].children.map((item, index) => {
                  if (item.link == props.location.pathname) {
                    return (
                      <Breadcrumb.Item key={index}><NavLink to={item.link}>{item.content}</NavLink></Breadcrumb.Item>
                    );
                  }

                })}
                {menus[0].children[0].children?.map((item, index) => {
                  if (item.link == props.location.pathname) {
                    return (
                      <Breadcrumb.Item key={index}><NavLink to={item.link}>{item.content}</NavLink></Breadcrumb.Item>
                    );
                  }


                })}
                {menus[0].children[0].children![0].children.map((item, index) => {
                  if (item.link == props.location.pathname) {
                    return (
                      <>
                        <Breadcrumb.Item key={index + '1'}><NavLink to='/article'>所有文章</NavLink></Breadcrumb.Item>
                        <Breadcrumb.Item key={index}><NavLink to={item.link}>{item.content}</NavLink></Breadcrumb.Item>
                      </>
                    );
                  }

                })}
              </Breadcrumb>
            }
          </header>
          <main className={styles.Content_main}>{props.children}
            <Footer>
              <div className={styles.layouts_footer}>
                <a href="https://github.com/fantasticit/wipi" target="_blank" rel="noreferrer">
                  <GithubOutlined className={styles.layouts_icon} />
                </a>
                <p>
                  Copyright <span><CopyrightOutlined /></span> 2021 Designed by <a href="https://github.com/fantasticit/wipi" target="_blank" rel="noreferrer">Fantasticit.</a>
                </p>
              </div>
            </Footer>
          </main>
        </Content>

      </Layout>
    </Layout>
  );
};

export default index;
