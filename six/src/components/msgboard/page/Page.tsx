import { useState } from 'react';

import { Pagination } from 'antd';
interface Props {
  total: number;
  getPage(page: number): any;
  page: number;
}
const Page: React.FC<Props> = (props) => {
  return (
    <div
      style={{ opacity: 1 }}
      onClick={() => {
        window.scrollTo({
          top: 330,
          behavior: 'smooth',
        });
      }}
    >
      <div className="_3k5-guVzUUtXzu2skrNnws">
        <Pagination
          total={props.total - 1}
          pageSize={6}
          current={props.page}
          onChange={props.getPage(1)}
        />
      </div>
    </div>
  );
};

export default Page;
