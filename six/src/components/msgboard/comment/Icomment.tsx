import { useRef, useState } from 'react';
import './comment.less';
import { Button, Dropdown, Form, Input, message, Modal } from 'antd';
import { Picker } from 'emoji-mart';
import { useDispatch } from 'umi';
interface Props {
  flag: boolean;
  getFlag(flag: boolean): void;
  name?: string;
  id?: string;
  email?: string;
}
const IComment: React.FC<Props> = (props) => {
  const dispath = useDispatch();
  const inp: any = useRef(null);
  const usename: any = useRef(null);
  const email: any = useRef(null);
  const [isShow, setisShow] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [IsEmail, setIsEmail] = useState(false);
  const [IsNull, setIsNull] = useState(false);
  const menu = (
    <Picker
      onClick={(e: any) => {
        setisShow(true);
        inp.current.value += e.native;
      }}
    />
  );
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    if (IsEmail && IsNull) {
      localStorage.setItem(
        'user',
        JSON.stringify({
          name: usename.current.state.value,
          email: email.current.state.value,
        }),
      );
      message.success('设置成功');
    } else {
      message.error('格式错误');
    }

    setIsModalVisible(false);
  };

  const handleCancel = () => {
    console.log('取消');

    setIsModalVisible(false);
  };

  return (
    <div className="_13J4GEYHDRu5MejyviLpnU">
      <div className="N1BwmgIkZZbNoeNN_FRbs">
        <div className="TaXtx7NU2ZhkaBRHq0-9o">
          <div className="_3D8H76H-ABokj1dp7OR3a4"></div>
          <textarea
            ref={inp}
            readOnly={localStorage.user ? false : true}
            onClick={showModal}
            className="ant-input"
            onInput={(e: any) => {
           
              if (e.target.value) {
                setisShow(true);
              } else {
                setisShow(false);
              }
            }}
            placeholder={`回复  ${props.name}`}
          ></textarea>
        </div>
        <footer>
          <div>
            <Dropdown overlay={menu} trigger={['click']}>
              <span className="_12avHmKjidVRHTuf5Qbd4K">
                <svg viewBox="0 0 1024 1024" width="18px" height="18px">
                  <path
                    d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z"
                    fill="currentColor"
                  ></path>
                </svg>
                <span>表情</span>
              </span>
            </Dropdown>
          </div>
          <div>
            <Button
              className="page_up"
              size="small"
              onClick={() => {
                props.getFlag(props.flag);
              }}
            >
              收起
            </Button>

            <Button
              size="small"
              disabled={isShow ? false : true}
              type="primary"
              onClick={() => {
                dispath({
                  type: 'msgboard/setcomment',
                  payload: {
                    content: inp.current.value,
                    email: JSON.parse(localStorage.user).email,
                    hostId: '8d8b6492-32e5-44e5-b38b-9a479d1a94bd',
                    name: JSON.parse(localStorage.user).name,
                    parentCommentId: props.id,
                    replyUserEmail: props.email,
                    replyUserName: props.name,
                    url: '/page/msgboard',
                  },
                });
                inp.current.value = '';
                props.getFlag(!props.flag);
                message.success('评论成功，已提交审核');
              }}
            >
              发布
            </Button>
          </div>
        </footer>
      </div>
      {localStorage.user ? null : (
        <Modal
          title="Basic Modal"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={[
            <>
              <Button onClick={handleCancel}>取消</Button>,
              <Button type="primary" htmlType="submit" onClick={handleOk}>
                设置
              </Button>
              ,
            </>,
          ]}
        >
          <Form>
            名字
            <Form.Item
              name="nickname"
              label="Nickname"
              tooltip="What do you want others to call you?"
              rules={[
                {
                  required: true,
                  message: 'Please input your nickname!',
                  whitespace: true,
                },
              ]}
            >
              <Input
                ref={usename}
                onInput={(e) => {
                  if ((e.target as HTMLInputElement).value) {
                    setIsNull(true);
                  } else {
                    setIsNull(false);
                  }
                }}
              />
            </Form.Item>
            <Form.Item
              name="email"
              label="E-mail"
              rules={[
                {
                  type: 'email',
                  message: 'The input is not valid E-mail!',
                },
                {
                  required: true,
                  message: 'Please input your E-mail!',
                },
              ]}
            >
              <Input
                ref={email}
                onInput={(e) => {
                  const email = /^([a-zA-Z0-9]+[_|_|\-|.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|_|.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
                  setIsEmail(email.test((e.target as HTMLInputElement).value));
                }}
              />
            </Form.Item>
          </Form>
        </Modal>
      )}
    </div>
  );
};

export default IComment;
