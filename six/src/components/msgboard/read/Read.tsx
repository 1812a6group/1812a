
// import RecommenThreen from "@/components/recommend/RecommenThree";
import Scroll from "@/components/Scroll";
import { IRootState } from "@/types";
import { useState } from "react";
import { useSelector } from "umi";

const Read: React.FC = () => {
  const { recommend, count } = useSelector((state: IRootState) => state.article);
  let [page, setPage] = useState(1);
  function pullupLoader() {
    setPage(page => page + 1)
  }
  return (
    <div className="_3ZUDOCREZiU7HtQwpEsqZZ">
      <p className="_1hXJ8FkJwAyuM2-YNk-KH8">推荐阅读</p>
      <Scroll List={recommend} Count={count} page={page} pullupLoader={pullupLoader} />
    </div>
  );
};

export default Read;
