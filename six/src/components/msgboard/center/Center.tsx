import { IMsgboard } from '@/types';
import { useEffect, useState } from 'react';
import { useDispatch } from 'umi';
import Page from '../page/Page';
import Reply from '../reply/Reply';
import Comment from '../comment/comment';
interface Props {
  recommend: IMsgboard[];
  total: number;
}
const Conter: React.FC<Props> = (props) => {
  const dispath = useDispatch();
  let [page, setpage] = useState(1);
  useEffect(() => {
    dispath({
      type: 'msgboard/getMsgboar',
      payload: page,
    });
  }, [page]);
  const getPage = (page: number) => {
    setpage(page);
  };

  //   中间内容
  return (
    <div className="_10wT67E6AAzqcEqLI7jUbZ">
      <div className="_3GUiOO804xme2-agc9WfvC">
        <p className="_1hXJ8FkJwAyuM2-YNk-KH8">评论</p>
        <div className="_3E53JQDjSQhUYjr-yCJTky">
          <div id="js-comment-id">
            {/* 发布评论组件 */}
            <Comment />
            {props.recommend.length > 1 &&
              props.recommend.map((item, index) => {
                //   评论列表渲染接口
                return <Reply item={item} key={item.id} index={index} />;
              })}
            {/* 页码接口 */}
            <Page total={props.total} getPage={() => getPage} page={page} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Conter;
