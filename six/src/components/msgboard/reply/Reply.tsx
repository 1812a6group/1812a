import { IMsgboard } from '@/types';
import moment from 'moment';
import Answer from './Answer';
moment.locale('zh-cn');

interface Props {
  item: IMsgboard;
  index: number;
}

let arr: Array<string> = [];
for (var i = 0; i < 6; i++) {
  let macolor = Math.floor(Math.random() * 16777215).toString(16);
  arr.push(macolor);
}

let arr1: Array<string> = [];
for (var i = 0; i < 6; i++) {
  let macolor3 = Math.floor(Math.random() * 16777215).toString(16);
  arr1.push(macolor3);
}

const Reply: React.FC<Props> = (props) => {
  return (
    <div className="_1f7ebMJKow4NcuDVTBnvOc _3G7_JYLS4EIxTgILiJm_A5">
      <header>
        <span
          className="ant-avatar ant-avatar-circle"
          style={{ backgroundColor: `#${arr[props.index]}` }}
        >
          <span className="ant-avatar-string">{props.item.name[0]}</span>
        </span>
        <span className="_3xprCsLyoqZkArtvYiuAJU">
          <strong>{props.item.name}</strong>
        </span>
      </header>
      <main>
        <div>
          {props.item.content.includes('<p>') ? (
            <div dangerouslySetInnerHTML={{ __html: props.item.content }}></div>
          ) : (
            props.item.content
          )}
        </div>
      </main>
      <footer>
        <div className="_14zvEeg141WRt1wf5S37nf">
          <span>{props.item.userAgent} · </span>
          <time dateTime={props.item.createAt}>
            {moment(+new Date(props.item.createAt)).fromNow()}
          </time>

          <Answer
            name={props.item.name}
            id={props.item.id}
            email={props.item.email}
          />
        </div>

        <div>
          <div>
            {props.item?.children.map((item1, index1) => {
              return (
                <div
                  className="_1f7ebMJKow4NcuDVTBnvOc _3bwpwVonlSFg4ZJFyKC9Q3"
                  key={index1}
                >
                  <header>
                    <span
                      className="ant-avatar ant-avatar-circle"
                      style={{
                        backgroundColor: `#${arr1[props.index]}`,
                      }}
                    >
                      <span className="ant-avatar-string">{item1.name[0]}</span>
                    </span>
                    <span className="_3xprCsLyoqZkArtvYiuAJU">
                      <strong>{item1.name}</strong>
                      <span>回复</span>
                      <strong>{props.item.name}</strong>
                    </span>
                  </header>
                  <main>
                    <div>
                      {item1.content.includes('<p>') ? (
                        <div
                          dangerouslySetInnerHTML={{
                            __html: item1.content,
                          }}
                        ></div>
                      ) : (
                        item1.content
                      )}
                    </div>
                  </main>
                  <footer>
                    <div className="_14zvEeg141WRt1wf5S37nf">
                      <span>{item1.userAgent} · </span>
                      <time dateTime={item1.createAt}>
                        {moment(+new Date(item1.createAt)).fromNow()}
                      </time>
                      <Answer
                        name={item1.name}
                        id={item1.id}
                        email={item1.email}
                      />
                    </div>
                  </footer>
                </div>
              );
            })}
          </div>
        </div>
      </footer>
    </div>
  );
};

export default Reply;
