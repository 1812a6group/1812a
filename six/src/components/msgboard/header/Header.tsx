const Herder: React.FC = (props) => {
  //   头部内容
  return (
    <div className="container">
      <div className="_1TDituBtews_ULM98bSKdc">
        <div className="markdown">
          <h2 style={{ textAlign: 'center', margin: 0, border: 0 }}>留言板</h2>
          <p style={{ textAlign: 'center', margin: 0 }}>
            <strong>请勿灌水 🤖 </strong>
          </p>
          <p style={{textAlign: 'center', margin: 0}}><strong>垃圾评论过多，欢迎提供好的过滤（标记）算法</strong></p>
        </div>
      </div>
    </div>
  );
};

export default Herder;
