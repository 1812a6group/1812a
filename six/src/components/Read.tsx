import { IRootState } from '@/types';
import React, { useState } from 'react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'umi';
// import { useDispatch, useSelector } from 'umi'
const moment = require('moment');

moment.defineLocale('zh-cn', {
  relativeTime: {
    future: '%s内',

    past: '%s前',

    s: '几秒',

    m: '1 分钟',

    mm: '%d 分钟',

    h: '1 小时',

    hh: '%d 小时',

    d: '1 天',

    dd: '%d 天',

    M: '1 个月',

    MM: '%d 个月',

    y: '1 年',

    yy: '%d 年',
  },
});

const Read: React.FC = (props) => {
  let [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const { recommend } = useSelector((state: IRootState) => state.article);
  const { getlist } = useSelector((state: IRootState) => state.knowledge);
  useEffect(() => {
    dispatch({
      type: 'article/getRecommend',
    });
  }, []);
  useEffect(() => {
    dispatch({
      type: 'knowledge/getcategory',
    });
  }, [page]);
  return (
    <div className="box1">
      <div className="box2">
        <h4>推荐阅读</h4>
        <div className="box3">
          {recommend.map(
            (
              item: {
                title:
                | boolean
                | React.ReactChild
                | React.ReactFragment
                | React.ReactPortal
                | null
                | undefined;
                createAt: any;
              },
              index: React.Key | null | undefined,
            ) => {
              return (
                <li key={index}>
                  <span>{item.title}</span>.
                  <span className="span1">
                    {moment(`${item.createAt}`).fromNow()}
                  </span>
                </li>
              );
            },
          )}
        </div>
      </div>
      <div className="boxa">
        <h4>分类阅读</h4>
        <div className="boxb">
          {getlist.map((item, index) => {
            return (
              <li key={index}>
                <span>{item.label}</span>
                <span className="span2">共计{item.articleCount}篇文章</span>
              </li>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Read;
