import React, { useEffect, useState } from 'react'
import InfiniteScroll from 'react-infinite-scroll-component';
import { IArticleItem, IScroll } from '@/types';
import { NavLink, withRouter } from 'umi'
import { RouteComponentProps } from 'react-router-dom'
// import moment from 'moment'
import Styles from './index.less'
import Modal from '@/components/Mask'
import classnames from 'classnames'
import { EyeOutlined, HeartOutlined, ShareAltOutlined } from '@ant-design/icons';
import moment from 'moment';
interface Props extends RouteComponentProps {
    List: IScroll[];
    Count: number;
    page: number;
    pullupLoader(): void;
}
const Scroll: React.FC<Props> = (props) => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [item, setItem] = useState({})
    const showModal = (item: IArticleItem) => {
        setItem(item)
        setIsModalVisible(true);
    };
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    return (
        <div className={Styles.scroll}>
            {/* 上拉加载组件 */}
            <InfiniteScroll
                hasMore={props.Count > props.page * 12}
                loader={<h1></h1>}
                dataLength={props.List.length}
                next={props.pullupLoader}
            >
                {
                    props.List.map(item => {
                        return <NavLink to={`/article/${item.id}`} key={item.id} className={classnames(Styles.main, 'items')}>
                            <p>
                                <NavLink to={`/article/${item.id}`}><span className={Styles.title}>{item.title}</span>
                                </NavLink>  | &nbsp;
                                <span>{moment(+new Date(item.publishAt)).fromNow()}</span> | &nbsp;
                                <span>{item.category && item.category.label}</span>
                            </p>
                            <dl>
                                {item.cover && <dt>
                                    <img src={item.cover} alt="" />
                                </dt>}
                                <dd>
                                    <p className={Styles.main_top}>{item.summary}</p>
                                    <p className={Styles.main_bot}>
                                        <HeartOutlined /> <span>{item.likes}</span> ·
                                        <EyeOutlined /> <span>{item.views}</span> ·

                                        <span onClick={() => showModal(item)}>
                                            <ShareAltOutlined /> 分享
                                        </span>
                                    </p>
                                </dd>
                            </dl>
                        </NavLink>
                    })
                }
            </InfiniteScroll>
            {/* 弹窗 */}
            <Modal isModalVisible={isModalVisible} handleCancel={handleCancel} item={item} />
        </div>
    )
}
export default withRouter(Scroll)