import React from 'react'
import { NavLink } from 'umi';
import { Ilabel } from '../../types'
import style from './index.less'
interface Props {
    item: Ilabel

}
const Tagcom: React.FC<Props> = (props) => {
    return (
        <>
            <li>
                <NavLink to={`/tag/${props.item.value}`}>{props.item.label}[{props.item.articleCount}]</NavLink>
            </li>
        </>
    )
}
export default Tagcom