import { IScroll } from '@/types'
import React from 'react'
import moment from 'moment'
import { NavLink } from 'umi'
import Styles from './index.less'
interface Props {
    List: IScroll[]
}
const Right_top: React.FC<Props> = (props) => {
    moment.locale('zh-cn')
    return <div className={Styles.top}>
        <div className={Styles.title}>
            <span>推荐阅读</span>
        </div>
        <div className={Styles.top_main}>
            {
                props.List.map(item => {
                    return <li key={item.id}>
                        <NavLink to={`/article/${item.id}`}>
                            {item.title} · <span>{moment(+new Date(item.createAt!)).fromNow()}</span>
                        </NavLink>
                    </li>
                })
            }
        </div>
    </div>
}
export default Right_top