import './index.less'
import { IArticleItem } from '../../types/article'
import moment from 'moment'
import { NavLink } from 'umi'
interface Props {
  item: IArticleItem;
}
const Swiper: React.FC<Props> = (props) => {
  return (
    <NavLink to={`/article/${props.item.id}`}>
      <div className='swiper' style={{ background: `url(${props.item.cover})`, backgroundSize: '100%' }}>
        <div className='main'>
          <h2>{props.item.title}</h2>
          <p>
            <span>{moment(+new Date(props.item.createAt!)).fromNow()} · </span>
            <span>{props.item.views}次阅读</span>
          </p>
        </div>
      </div>
    </NavLink>
  )
}
export default Swiper
