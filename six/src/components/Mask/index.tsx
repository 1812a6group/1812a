import React, { useEffect, useState } from 'react'
import { Modal } from 'antd';
import { IArticleItem, } from '@/types';
import styles from './index.less'
import { NavLink, useHistory } from 'umi';
import QRCode from 'qrcode'
import { genePoster } from '@/services';
interface Props {
    isModalVisible: boolean

    handleCancel(): void;
    item: IArticleItem
}

const masks: React.FC<Props> = (props) => {
    // 二维码
    const path = useHistory()
    const [qrSrc, setQrSrc] = useState('')
    const [downloadSrc, setDownloadSrc] = useState('');
    const url = `https://blog.wipi.tech/${path.location.pathname}/${props.item.id}`
    useEffect(() => {
        if (props.item) {
            QRCode.toDataURL(url)
                .then((url) => {
                    setQrSrc(url)
                })
                .catch(err => {
                    console.log(err);
                })
        }
    }, [props.item])
    useEffect(() => {
        if (Object.keys(props.item).length && qrSrc) {
            genePoster({
                height: 861,
                html: document.querySelector('.ant-modal-content')?.innerHTML!,
                name: props.item.title!,
                pageUrl: `/article/${props.item.id}`,
                width: 391
            }).then(res => {
                setDownloadSrc(res.data.url);
            })
        }
    }, [props.item, qrSrc]);

    return (
        <>
            <Modal title="分享海报"
                visible={props.isModalVisible}
                onOk={() => window.location.href = downloadSrc}
                onCancel={props.handleCancel}
                cancelText='关闭'
                okText="下载"
                okType="danger"
            >
                <div className={styles.top}>
                    <img src={props.item.cover} alt="" />
                    <h3>
                        {
                            props.item.title
                        }
                    </h3>
                    <p>{props.item.summary}</p>
                </div>
                <dl className={styles.bot}>
                    <dt><img src={qrSrc} alt="" /></dt>
                    <dd>
                        <p>识别二维码查看文章</p>
                        <p> 原文分享自 <NavLink to={`/article/${props.item.id}`}>小楼又清风</NavLink></p>
                    </dd>
                </dl>
            </Modal>
        </>
    )
}
export default masks
