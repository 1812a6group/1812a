import { IArticleItem, IRootState, RootObject } from '@/types';

const moment = require('moment');
import styles from './read.less';

moment.defineLocale('zh-cn', {
  relativeTime: {
    future: '%s内',

    past: '%s前',

    s: '几秒',

    m: '1 分钟',

    mm: '%d 分钟',

    h: '1 小时',

    hh: '%d 小时',

    d: '1 天',

    dd: '%d 天',

    M: '1 个月',

    MM: '%d 个月',

    y: '1 年',

    yy: '%d 年',
  },
});
interface props {
  recommend: IArticleItem[];
  getlist: RootObject[];
}
const Read: React.FC<props> = (props) => {
  return (
    <aside className={styles.aside}>
      <div className={styles.sticky}>
        <div className={styles.sticky_top}>
          <div className={styles.sticky_top_top}>
            <span>推荐阅读</span>
          </div>
          <div className={styles.sticky_top_bottom}>
            {props.recommend &&
              props.recommend.map((item, index) => {
                return (
                  <ul key={index}>
                    <li>
                      <a>
                        <span>{item.title}</span>.
                        <span>{moment(`${item.createAt}`).fromNow()}</span>
                      </a>
                    </li>
                  </ul>
                );
              })}
          </div>
        </div>
        <div className={styles.sticky_bottom}>
          <div className={styles.sticky_bottom_top}>
            <span>文章阅读</span>
          </div>
          <div className={styles.sticky_bottom_bottom}>
            {props.getlist &&
              props.getlist.map((item, index) => {
                return (
                  <ul key={index} className="">
                    <li>
                      <a href="">
                        <span className="span3">{item.label}</span>
                        <span className="span4">
                          共计{item.articleCount}篇文章
                        </span>
                      </a>
                    </li>
                  </ul>
                );
              })}
          </div>
        </div>
      </div>
    </aside>
  );
};

export default Read;
