"use strict";
exports.__esModule = true;
var moment = require("moment");
require("./read1.less");
moment.defineLocale('zh-cn', {
  relativeTime: {
    future: '%s内',
    past: '%s前',
    s: '几秒',
    m: '1 分钟',
    mm: '%d 分钟',
    h: '1 小时',
    hh: '%d 小时',
    d: '1 天',
    dd: '%d 天',
    M: '1 个月',
    MM: '%d 个月',
    y: '1 年',
    yy: '%d 年'
  }
});
var Read = function (props) {
  return (React.createElement("div", {
      className: 'sticky'
    },
    React.createElement("div", {
        className: "ammbB8tpRTlzwGu9Q8GNE _3PpmFreY8xlC6Z2Y0lcyk-"
      },
      React.createElement("div", {
          className: "_1hh60i8CByuosza1JbgE0Q"
        },
        React.createElement("span", null, "\u63A8\u8350\u9605\u8BFB")),
      React.createElement("div", {
          className: 'ant-spin-nested-loading'
        },
        React.createElement("div", {
          className: "ant-spin-container"
        }, props.recommend && props.recommend.map(function (item, index) {
          return React.createElement("ul", {
              className: "_1PytTgSelvgOTMZR4ckInB",
              key: index
            },
            React.createElement("li", {
                className: 'sss'
              },
              React.createElement("span", {
                className: 'span2'
              }, item.title),
              ".",
              React.createElement("span", {
                className: 'span1'
              }, moment("" + item.createAt).fromNow())));
        })))),
    React.createElement("div", {
        className: "_3KFGvyCY0omWyEQOzioDDk"
      },
      React.createElement("div", {
          className: "_2b0LQ_j7dN-JQtnGkqmrDz"
        },
        React.createElement("span", null, "\u6587\u7AE0\u9605\u8BFB")),
      props.getlist && props.getlist.map(function (item, index) {
        return React.createElement("ul", {
            key: index,
            className: ""
          },
          React.createElement("li", {
              className: "xxx"
            },
            React.createElement("span", {
              className: 'span3'
            }, item.label),
            React.createElement("span", {
                className: 'span4'
              },
              "\u5171\u8BA1",
              item.articleCount,
              "\u7BC7\u6587\u7AE0")));
      }))));
};
exports["default"] = Read;
