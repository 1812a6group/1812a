import { ICategory } from '@/types'
import React from 'react'
import moment from 'moment'
import { NavLink } from 'umi'
import Styles from './index.less'
interface Props {
    List: ICategory[]
}
const Right_bot: React.FC<Props> = (props) => {
    moment.locale('zh-cn')
    return <div className={Styles.bottom}>
        <div className={Styles.title}>
            <span>文章标签</span>
        </div>
        <div className={Styles.bottom_main}>
            {
                props.List.map(item => {
                    return <li key={item.id}><NavLink to={`/category/${item.value}`}><span>{item.label}</span> <span>共计{item.articleCount}篇文章</span> </NavLink></li>
                })
            }
        </div>
    </div>
}
export default Right_bot