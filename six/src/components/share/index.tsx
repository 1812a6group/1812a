import React, { useEffect, useState } from 'react';
import { Modal } from 'antd';
import ReactDOM from 'react-dom';
import { IArticleItem } from '@/types';
import QRCode from 'qrcode'
import { genePoster } from '@/services';
import "./index.less"
interface IProps {
    item: IArticleItem
}
export const Share: React.FC<IProps> = ({ item }) => {
    const [qrSrc, setQrSrc] = useState('');
    const [downloadSrc, setDownloadSrc] = useState('');

    function cancelClick() {
        ReactDOM.unmountComponentAtNode(document.querySelector('#share-dialog')!);
    }
    const url = `https://blog.wipi.tech/article`;
    useEffect(() => {
        if (Object.keys(item)) {
            QRCode.toDataURL(url)
                .then((url: string) => {
                    setQrSrc(url)
                })
                .catch(err => {
                    console.error(err)
                })
        }
    }, [item]);
    // 生成海报
    useEffect(() => {
        if (Object.keys(item).length && qrSrc) {
            genePoster({
                height: 461,
                html: document.querySelector('.ant-modal-content')?.innerHTML!,
                name: item.title!,
                pageUrl: `/knowledge/${item.id}`,
                width: 391
            }).then(res => {
                setDownloadSrc(res.data.url);
            })
        }
    }, [item, qrSrc]);


    return <Modal
        visible={true}
        title="分享海报"
        okText="下载"
        cancelText="关闭"
        onOk={() => window.location.href = downloadSrc}
        onCancel={cancelClick}
        className='poster'
    >
        {item.cover && <img src={item.cover} />}
        <p className='hh2'>{item.title}</p>
        <p>{item.summary}</p>
        <div className='pp1'>
            <img src={qrSrc} alt="" />
            <div>
                <p>识别二维码查看文章</p>
                <p>原文分享自
                    <a href={url}>小楼又清风</a>
                </p>
            </div>
        </div>
    </Modal>

}
// 抛出
export default function share(item: IArticleItem) {
    let shareDialog = document.querySelector('#share-dialog');
    if (!shareDialog) {
        shareDialog = document.createElement('div');
        shareDialog.id = 'share-dialog';
        document.body.appendChild(shareDialog);
    }
    ReactDOM.render(<Share item={item} />, shareDialog);
}
