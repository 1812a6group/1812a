import { useDispatch, useSelector } from "@/.umi/plugin-dva/exports";
import React, { useState } from "react";
import styles from './index.less';
import { IRootState } from '@/types';
import { NavLink } from "umi";
interface Props {
    setearchbtn(): void
}
const SearchContent: React.FC<Props> = (props) => {
    const [value, setValue] = useState('');
    const dispatch = useDispatch();
    const { searchList } = useSelector((state: IRootState) => state.search);
    let setinput = (e: string) => {
        setValue(value => value = e);
    }
    let searchvalue = () => {
        if (value) {
            dispatch({
                type: 'search/getSearchList',
                payload: value
            })
        }
    }
    return <div className={styles.search_content}>
        <div className={styles.searchbox}>
            <header>
                <span className={styles.article_search}>文章搜索</span>
                <span className={styles.search_off}>
                    <span role="img" aria-label="close" className={styles.anticon} onClick={() => props.setearchbtn()}>
                        <svg viewBox="64 64 896 896" focusable="false" data-icon="close" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                            <path d="M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z"></path>
                        </svg>
                    </span>
                    <span>esc</span>
                </span>
            </header>
            <section>
                <span className={styles.searchinput}>
                    <span className={styles.searchinput_1}>
                        <input type="text" placeholder="输入关键字，搜索文章" className={styles.search_input} onChange={e => setinput(e.target.value)} />
                    </span>
                    <span className={styles.search_btn} onClick={() => searchvalue()}>
                        <svg viewBox="64 64 896 896">
                            <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
                        </svg>
                    </span>
                </span>
            </section>
        </div>
        <div className={styles.search_list}>
            <ul>
                {
                    searchList && searchList.map(item => {
                        return <li key={item.id}>
                            <NavLink to={`/article/${item.id}`} className={styles.search_listall} onClick={() => props.setearchbtn()}>{item.title}</NavLink>
                        </li>
                    })
                }
            </ul>
        </div>
    </div>
}

export default SearchContent;