import { getComment } from '@/services';
import { RootObject1 } from '@/types';
import { Effect, Reducer } from 'umi';

export interface AboutModelState {
  aboutlist: RootObject1[];
  aboutCount: number;
}

export interface AboutModelType {
  namespace: 'about';
  state: AboutModelState;
  effects: {
    getComment: Effect;
  };
  reducers: {
    save: Reducer<AboutModelState>;
  };
}

const IndexModel: AboutModelType = {
  namespace: 'about',

  state: {
    aboutlist: [],
    aboutCount: 0,
  },

  effects: {
    *getComment({ payload }, { call, put }) {
      let result = yield call(getComment, payload);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: {
            aboutlist: result.data[0],
            aboutCount: result.data[1],
          },
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default IndexModel;
