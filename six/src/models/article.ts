
import { IArticleItem, ICategory, Ilabel, ItemCatalog, ItemMask, IScroll } from '@/types';
import {

  getArticleList,
  getRecommend,
  getcategory,
  getArticleTitle,
  getArticleDateil,
  getArticleComment,
  getCategoryClassify,
  getTag,
  getTagClassify,
  getArticleLikes
} from '@/services';
import { Effect, Reducer } from 'umi';
import { IRootState } from '@/types';

export interface ArticleModelState {
  // 定义接口类型
  recommend: IScroll[];
  articleList: IScroll[];
  count: number;
  category: ICategory[];
  labelList: Ilabel[];
  categoryCount: number;
  catrgoryClassify: IScroll[];
  catrgoryClassifyNum: number;
  tagTitle: Ilabel[];
  tagList: IScroll[];
  tagCount: number
  articledateil: Partial<ItemMask>;
  articlecomment: [];
  articleCommentCount: number;
  catalog: ItemCatalog[];
  likeslist: number
}

export interface ArticleModelType {
  namespace: 'article';
  state: ArticleModelState;
  effects: {
    // 在 Effect 中 定义一下方法
    getRecommend: Effect;
    getArticleList: Effect;
    getCategory: Effect;
    getLabel: Effect;
    //详情
    getArticleDateil: Effect;
    getArticleComment: Effect;
    getCategoryClassify: Effect;
    getTag: Effect;
    getTagClassify: Effect;
    getArticleLikes: Effect
  };
  reducers: {
    save: Reducer<ArticleModelState>;
  };
}

const IndexModel: ArticleModelType = {
  namespace: 'article',
  state: {
    catalog: [],
    recommend: [],
    count: 0,
    category: [],
    categoryCount: 0,
    catrgoryClassify: [],
    catrgoryClassifyNum: 0,
    articleList: [],
    labelList: [],
    articledateil: {},
    articlecomment: [],
    articleCommentCount: 0,
    tagTitle: [],
    tagList: [],
    tagCount: 0,
    likeslist: 0
  },

  effects: {
    //获取首页数据
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { recommend: result.data },
        });
      }
    },
    //获取上拉加载数据
    *getArticleList({ payload }, { call, put, select }) {
      let articleList = yield select((state: IRootState) => state.article.articleList)
      let res = yield call(getArticleList, payload)
      if (res.statusCode === 200) {
        articleList =
          payload === 1 ? res.data[0] : [...articleList, ...res.data[0]];
        yield put({
          type: 'save',
          payload: {
            articleList,
            count: res.data[1],
          },
        });
      }
    },
    //获取文章
    *getCategory({ payload }, { call, put, select }) {
      let res = yield call(getcategory)
      if (res.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { category: res.data },
        });
      }
    },
    //获取文章分类
    *getCategoryClassify({ value, payload }, { call, put, select }) {
      let data = yield select((state: IRootState) => state.article.catrgoryClassify)
      let res = yield call(getCategoryClassify, value, payload)
      payload === 1 ? data = res.data[0] : data
      if (res.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { catrgoryClassify: data, catrgoryClassifyNum: res.data[1] }
        })
      }
    },
    //获取文章标签数据
    *getLabel({ payload }, { call, put }) {
      let res = yield call(getArticleTitle)
      if (res.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { labelList: res.data },
        });
      }
    },
    //获取文章详情
    *getArticleDateil({ payload }, { call, put }) {
      let res = yield call(getArticleDateil, payload)
      if (res.statusCode === 200) {
        yield put({
          type: 'save',
          payload: {
            articledateil: res.data,
            likeslist: res.data.likes,
            catalog: JSON.parse(res.data.toc)
          },

        });
      }
    },
    //获取评论
    *getArticleComment({ payload }, { call, put }) {
      let res = yield call(getArticleComment, payload.id, payload.page)
      if (res.statusCode === 200) {
        yield put({
          type: 'save',
          payload: {
            articlecomment: res.data[0],
            articleCommentCount: res.data[1],
          },
        });
      }
    },
    *getTag({ payload }, { call, put }) {
      let res = yield call(getTag)
      if (res.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { tagTitle: res.data }
        })
      }
    },
    *getTagClassify({ value, payload }, { call, put, select }) {
      let data = yield select((state: IRootState) => state.article.tagList)
      let res = yield call(getTagClassify, value, payload)
      payload === 1 ? data = res.data[0] : data

      if (res.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { tagList: data, tagCount: res.data[1] }
        })
      }
    },

    //喜欢
    *getArticleLikes({ payload }, { call, put }) {
      let res = yield call(getArticleLikes, payload.id, payload.type)
      if (res.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { likeslist: res.data.likes }
        })
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default IndexModel;
