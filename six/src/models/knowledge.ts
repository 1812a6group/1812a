import {
  getRecommend,
  getknowledge,
  getcategory,
  getdetail,
  getsondetail,
  getknowLikes,
} from '@/services';
import { Getdetail, IArticleItem, RootObject, Getsondetail } from '@/types';
import { Effect, Reducer } from 'umi';


export interface KnowledgeModelState {
  // 定义接口类型
  recommend: IArticleItem[];
  knowlist: IArticleItem[];
  getlist: RootObject[];
  detail: Partial<Getdetail>;
  sondetail: Partial<Getsondetail>;
  knowtoc:Getsondetail[];
  likeslist: number
}

export interface KnowledgeModelType {
  namespace: 'knowledge';
  state: KnowledgeModelState;
  effects: {
    // 在 Effect 中 定义一下方法
    getRecommend: Effect;
    getknowledge: Effect;
    getcategory: Effect;
    getdetail: Effect;
    getsondetail: Effect;
    getknowLikes:Effect;
  };
  reducers: {
    save: Reducer<KnowledgeModelState>;
  };
}

const IndexModel: KnowledgeModelType = {
  namespace: 'knowledge', //命名空间
  state: {
    recommend: [],
    knowlist: [],
    getlist: [],
    detail: {},
    sondetail: {},
    knowtoc:[],
    likeslist:0,
  },

  effects: {
    //获取首页数据
    *getRecommend({ payload }, { call, put }) {
      let result = yield call(getRecommend);
      console.log('result...', result);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { recommend: result.data },
        });
      }
    },
    // 获取小册数据
    *getknowledge({ payload }, { call, put, select }) {
      let knowlist = yield select((state: any) => state.knowledge.knowlist);
      // let knowlist = yield select((state:any) => state.article.knowlist)
      // console.log(knowlist);
      let result = yield call(getknowledge, payload);
      if (result.statusCode === 200) {
        knowlist =
          payload === 1 ? result.data[0] : [...knowlist, ...result.data[0]];
        yield put({
          type: 'save',
          payload: {
            knowlist,
            articleCount: result.data[1],
          },
        });
      }
    },
    //获取分类阅读数据
    *getcategory({ payload }, { call, put, select }) {
      let result = yield call(getcategory, payload);
      // console.log(result);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: {
            getlist: result.data,
          },
        });
      }
    },
    // 获取详情页数据
    *getdetail({ payload }, { call, put }) {
      let result = yield call(getdetail, payload);
      console.log(result);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: {
            detail: result.data,
          },
        });
      }
    },
    // 获取子详情的数据
    *getsondetail({ payload }, { call, put }) {
      let result = yield call(getsondetail, payload);
      console.log(result);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: {
            sondetail:result.data,
            knowtoc:JSON.parse(result.data.toc)
          },
        });
      }
    },
    // 喜欢
    *getknowLikes({ payload }, { call, put }) {
      let res = yield call(getknowLikes, payload.id, payload.type)
      if (res.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { likeslist: res.data.likes }
        })
      }
    },
    
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default IndexModel;
