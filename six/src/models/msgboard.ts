import { getMsgboar, getList, setcomment } from '@/services';
import { IMsgboard, RootObject } from '@/types';
import { Effect, Reducer } from 'umi';
export interface MsgboardModelState {
  recommend: IMsgboard[];
  list: RootObject[];
  total: number;
}
export interface ArticleModelType {
  namespace: 'msgboard';
  state: MsgboardModelState;
  effects: {
    getList: Effect;
    getMsgboar: Effect;
    setcomment: Effect;
  };
  reducers: {
    save: Reducer<MsgboardModelState>;
  };
}
const IndexModel: ArticleModelType = {
  namespace: 'msgboard',
  state: {
    recommend: [],
    list: [],
    total: 0,
  },

  effects: {
    *getList({ payload }, { call, put }) {
      let res = yield call(getList);
      if (res.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { list: res.data },
        });
      }
    },
    *getMsgboar({ payload }, { call, put }) {
      let result = yield call(getMsgboar, payload);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { recommend: result.data[0], total: result.data[1] },
        });
      }
    },
    *setcomment({ payload }, { call, put }) {
      let result = yield call(setcomment, payload);
      console.log('result...', result);
      yield put({
        type: 'save',
      });
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default IndexModel;
