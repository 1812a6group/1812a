import { getSearchList } from '@/services';
import { SearchObject } from '@/types';
import { RootObject1 } from '@/types/about';
import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface SearchModelState {
    searchList: SearchObject[]
}

export interface SearchModelType {
    namespace: 'search';
    state: SearchModelState;
    effects: {
        getSearchList: Effect
    };
    reducers: {
        save: Reducer<SearchModelState>;
    };
}

const IndexModel: SearchModelType = {
    namespace: 'search',

    state: {
        searchList: []
    },

    effects: {
        *getSearchList({ payload }, { call, put }) {
            let result = yield call(getSearchList, payload);
            console.log(result);
            if (result.statusCode === 200) {
                yield put({
                    type: 'save',
                    payload: { searchList: result.data }
                })
            }
        }
    },
    reducers: {
        save(state, action) {
            return {
                ...state,
                ...action.payload,
            };
        },
    }
};

export default IndexModel;