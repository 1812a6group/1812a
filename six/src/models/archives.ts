import { getArchivesvList } from '@/services';
import { IArchivesItem } from '@/types/archives';
import { Effect, Reducer } from 'umi';


export interface ArchivesModelState {
  archives: IArchivesItem;
}

export interface ArchivesModelType {
  namespace: 'archives';
  state: ArchivesModelState;
  effects: {
    getArchivesvList: Effect;
  };
  reducers: {
    save: Reducer<ArchivesModelState>;
  };
}

const IndexModel: ArchivesModelType = {
  namespace: 'archives',

  state: {
    archives: {} as IArchivesItem,
  },

  effects: {
    *getArchivesvList({ payload }, { call, put }) {
      let result = yield call(getArchivesvList);
      // console.log('result...', result);
      if (result.statusCode === 200) {
        yield put({
          type: 'save',
          payload: { archives: result.data },
        });
      }
    },
  },
  reducers: {
    save(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};

export default IndexModel;
