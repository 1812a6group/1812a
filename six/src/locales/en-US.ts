export default {
    'menu.article': 'Article',
    'menu.archive': 'Archive',
    'menu.knowledge': 'Knowledge',
    'menu.message': 'Message',
    'menu.about': 'About',
    'menu.chare': 'chare',
    'menu.language.chinese': 'Chinese',
    'menu.language.english': 'English',
}
