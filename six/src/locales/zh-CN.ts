export default {
    'menu.article': '文章',
    'menu.archive': '归档',
    'menu.knowledge': '知识小册',
    'menu.message': '留言板',
    'menu.about': '关于',
    'menu.chare': '分享',
    'menu.language.chinese': '中文',
    'menu.language.english': '英语',
}
