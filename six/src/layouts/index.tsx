import { NavLink, Redirect, setLocale, useDispatch, useIntl, useSelector } from 'umi';
import './index.less';
import '../global.less';
import 'emoji-mart/css/emoji-mart.css';
// import { NavLink, Redirect } from 'umi';
import { IRootState } from '@/types';
import { useState, useRef, useEffect } from 'react';
import SearchContent from '@/components/search/Searchcontent';
const Layouts: React.FC = (props) => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const { language } = useSelector((state: IRootState) => state)
  let [islocale, setislocale] = useState(false);
  let [flag, setflag] = useState(false);
  let [searchFlag, setsearchFlag] = useState(false);
  const r: any = useRef(null);
  const top: any = useRef('');
  const X:any = useRef('')
  const Y:any = useRef('')
  useEffect(() => {
    setLocale(language.locale, false);
  }, [language.locale]);
  

  const menus = [{
    title: 'menu.article',
    link: '/article'
  }, {
    title: 'menu.archive',
    link: '/archives'
  }, {
    title: 'menu.knowledge',
    link: '/knowledge'
  }, {
    title: 'menu.message',
    link: '/msgboard'
  }, {
    title: 'menu.about',
    link: '/about'
  },]


  useEffect(() => {
    dispatch({
      type: 'article/getCategory'
    }),
      dispatch({
        type: 'article/getRecommend'
      }),
      dispatch({
        type: 'article/getLabel'
      }),
      dispatch({
        type: 'article/getTag'
      })
  }, [])

  let searchContent = () => {
    setsearchFlag((searchFlag) => (searchFlag = true));
  };
  let setearchbtn = () => {
    setsearchFlag((searchFlag) => (searchFlag = false));
  };
  window.onscroll = () => {
    if (window.scrollY >= 200) {
      top.current.style.display = 'block';
    } else {
      top.current.style.display = 'none';
    }
  };

  return (
    <div className="_oU_ibYFSP27GhOy5w0XY">
      <header className="_3qxGtLoeoc9_cTrMop-zTn">
        <div className="_3M_Iyty-_u6Mq1VeSk14Rq">
          <div className="container">
            <div className="_3U9PAhHDbA5NYPGpvj3gix">
              <a href="/">
                <img
                  src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-logo.png"
                  alt=""
                ></img>
              </a>
            </div>
            <div className="_1m0pVz8iNOboGDqVTeBfmy" ref={X} onClick={()=>{
                if(X.current.className == '_1m0pVz8iNOboGDqVTeBfmy'){
                  X.current.className =  '_1m0pVz8iNOboGDqVTeBfmy _1KU4i5t4zJjAhq0ZlikqlG'
                  Y.current.className = '_1KU4i5t4zJjAhq0ZlikqlG'
                }else{
                  X.current.className  =  '_1m0pVz8iNOboGDqVTeBfmy'
                  Y.current.className = ''
                }
            }}>
              <div className="_2-i62DvrXpASKd_zBrhHUV"></div>
              <div className="_2-i62DvrXpASKd_zBrhHUV"></div>
              <div className="_2-i62DvrXpASKd_zBrhHUV"></div>
            </div>
            <nav className="" ref={Y} onClick={()=>{
               X.current.className  =  '_1m0pVz8iNOboGDqVTeBfmy'
               Y.current.className = ''
            }}>
              <ul>
                {
                  menus.map(item => {
                    return <li key={item.link}>
                      <NavLink to={item.link}>
                        {intl.formatMessage({ id: item.title })}
                      </NavLink></li>
                  })
                }
                <li className="_38OmGXMfjYo_xu-5O1rJEK">

                  <span
                    role="img"
                    aria-label="search"
                    style={{ cursor: 'pointer' }}
                    className="anticon anticon-search"
                    onClick={() => searchContent()}
                  >
                    <svg
                      viewBox="64 64 896 896"
                      focusable="false"
                      data-icon="search"
                      width="1em"
                      height="1em"
                      fill="currentColor"
                      aria-hidden="true"
                    >
                      <path d="M909.6 854.5L649.9 594.8C690.2 542.7 712 479 712 412c0-80.2-31.3-155.4-87.9-212.1-56.6-56.7-132-87.9-212.1-87.9s-155.5 31.3-212.1 87.9C143.2 256.5 112 331.8 112 412c0 80.1 31.3 155.5 87.9 212.1C256.5 680.8 331.8 712 412 712c67 0 130.6-21.8 182.7-62l259.7 259.6a8.2 8.2 0 0011.6 0l43.6-43.5a8.2 8.2 0 000-11.6zM570.4 570.4C528 612.7 471.8 636 412 636s-116-23.3-158.4-65.6C211.3 528 188 471.8 188 412s23.3-116.1 65.6-158.4C296 211.3 352.2 188 412 188s116.1 23.2 158.4 65.6S636 352.2 636 412s-23.3 116.1-65.6 158.4z"></path>
                    </svg>
                  </span>
                </li>
                <li className="_38OmGXMfjYo_xu-5O1rJEK">
                  <div className="_2BHtRLIf7EUPHvdL0oIKeR"
                    ref={r}
                    onClick={() => {
                      if (r.current) {
                        setflag((flag) => !flag);

                        if (flag) {
                          r.current.className = '_2BHtRLIf7EUPHvdL0oIKeR';
                          document.body.className = '';
                        } else {
                          r.current.className =
                            '_2BHtRLIf7EUPHvdL0oIKeR _1M2d1ahyt0GjdRDlVxVqCN';
                          document.body.className = 'dark';
                        }
                      }
                    }}

                  >
                    <span className="_2DxxB4r6HYBEDcLqnvTKMg"></span>
                    <span className="_3FbZOtsOBAPa2UFFqIaC_z"></span>
                    <small className="_3Gvr4LAEinc-IIBjwFSMH_"></small>
                    <small className="_3Gvr4LAEinc-IIBjwFSMH_"></small>
                    <small className="_3Gvr4LAEinc-IIBjwFSMH_"></small>
                    <small className="_3Gvr4LAEinc-IIBjwFSMH_"></small>
                    <small className="_3Gvr4LAEinc-IIBjwFSMH_"></small>
                    <small className="_3Gvr4LAEinc-IIBjwFSMH_"></small>
                  </div>
                </li>

                <li className="_38OmGXMfjYo_xu-5O1rJEK" onClick={() => {


                  setislocale(!islocale)
                  let locale
                  if (islocale) {
                    locale = "zh-CN"
                  } else {
                    locale = "en-US"
                  }

                  dispatch({
                    type: 'language/save',
                    payload: { locale }
                  })
                }}>
                  <div className="ant-dropdown-trigger">
                    <svg
                      viewBox="0 0 1024 1024"
                      version="1.1"
                      xmlns="http://www.w3.org/2000/svg"
                      width="1em"
                      height="1em"
                    >
                      <path
                        d="M547.797333 638.208l-104.405333-103.168 1.237333-1.28a720.170667 720.170667 0 0 0 152.490667-268.373333h120.448V183.082667h-287.744V100.906667H347.605333v82.218666H59.818667V265.386667h459.178666a648.234667 648.234667 0 0 1-130.304 219.946666 643.242667 643.242667 0 0 1-94.976-137.728H211.541333a722.048 722.048 0 0 0 122.453334 187.434667l-209.194667 206.378667 58.368 58.368 205.525333-205.525334 127.872 127.829334 31.232-83.84m231.424-208.426667h-82.218666l-184.96 493.312h82.218666l46.037334-123.306667h195.242666l46.464 123.306667h82.218667l-185.002667-493.312m-107.690666 287.744l66.56-178.005333 66.602666 178.005333z"
                        fill="currentColor"
                      ></path>
                    </svg>
                  </div>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </header>
      <main className="main_all">
        <div className="container">
          <Redirect to="/article" />
          {props.children}
        </div>
      </main>
      <footer className="_3SL8bXyPeDS8XR5EoRJ31s">
        <ul className="_1-uVZKG0Zi-XehmZOgdOtA">
          <li>
            <a href="/rss" target="_blank">
              <span role="img" className="anticon">
                <svg
                  viewBox="0 0 1024 1024"
                  version="1.1"
                  p-id="4788"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="24"
                  height="24"
                >
                  <defs>
                    <style type="text/css"></style>
                  </defs>
                  <path
                    d="M512 0C230.4 0 0 230.4 0 512s230.4 512 512 512 512-230.4 512-512S793.6 0 512 0z m-182.4 768C288 768 256 736 256 694.4s32-73.6 73.6-73.6 73.6 32 73.6 73.6-32 73.6-73.6 73.6z m185.6 0c0-144-115.2-259.2-259.2-259.2v-80c185.6 0 339.2 150.4 339.2 339.2h-80z m172.8 0c0-240-195.2-432-432-432V256c281.6 0 512 230.4 512 512h-80z"
                    fill="currentColor"
                  ></path>
                </svg>
              </span>
            </a>
          </li>
          <li>
            <a
              href="https://github.com/fantasticit/wipi"
              target="_blank"
              rel="noreferrer"
            >
              <span
                role="img"
                aria-label="github"
                className="anticon anticon-github"
              >
                <svg
                  viewBox="64 64 896 896"
                  focusable="false"
                  data-icon="github"
                  width="1em"
                  height="1em"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path d="M511.6 76.3C264.3 76.2 64 276.4 64 523.5 64 718.9 189.3 885 363.8 946c23.5 5.9 19.9-10.8 19.9-22.2v-77.5c-135.7 15.9-141.2-73.9-150.3-88.9C215 726 171.5 718 184.5 703c30.9-15.9 62.4 4 98.9 57.9 26.4 39.1 77.9 32.5 104 26 5.7-23.5 17.9-44.5 34.7-60.8-140.6-25.2-199.2-111-199.2-213 0-49.5 16.3-95 48.3-131.7-20.4-60.5 1.9-112.3 4.9-120 58.1-5.2 118.5 41.6 123.2 45.3 33-8.9 70.7-13.6 112.9-13.6 42.4 0 80.2 4.9 113.5 13.9 11.3-8.6 67.3-48.8 121.3-43.9 2.9 7.7 24.7 58.3 5.5 118 32.4 36.8 48.9 82.7 48.9 132.3 0 102.2-59 188.1-200 212.9a127.5 127.5 0 0138.1 91v112.5c.8 9 0 17.9 15 17.9 177.1-59.7 304.6-227 304.6-424.1 0-247.2-200.4-447.3-447.5-447.3z"></path>
                </svg>
              </span>
            </a>
          </li>
        </ul>
        <div className="_2KuyeuhURuEtdsOPRH7xZB">
          <p>
            Designed by{' '}
            <a href="https://github.com/fantasticit/wipi" target="_blank">
              Fantasticit
            </a>{' '}
            .{' '}
            <a href="https://admin.blog.wipi.tech/" target="_blank">
              后台管理
            </a>
          </p>
          <p>Copyright © 2021. All Rights Reserved.</p>
          <p>皖ICP备18005737号</p>
        </div>
      </footer>

      <div
        className="ant-back-top"
        ref={top}
        onClick={() => {
          window.scrollTo({
            top: 0,
            behavior: 'smooth',
          });
          top.current.style.display = 'none';
        }}
      >
        <div>
          <div className="ant-fade-enter ant-fade-enter-active ant-fade ant-back-top-content">
            <div className="ant-back-top-icon">
              <span
                role="img"
                aria-label="vertical-align-top"
                className="anticon anticon-vertical-align-top"
              >
                <svg
                  viewBox="64 64 896 896"
                  focusable="false"
                  data-icon="vertical-align-top"
                  width="1em"
                  height="1em"
                  fill="currentColor"
                  aria-hidden="true"
                >
                  <path d="M859.9 168H164.1c-4.5 0-8.1 3.6-8.1 8v60c0 4.4 3.6 8 8.1 8h695.8c4.5 0 8.1-3.6 8.1-8v-60c0-4.4-3.6-8-8.1-8zM518.3 355a8 8 0 00-12.6 0l-112 141.7a7.98 7.98 0 006.3 12.9h73.9V848c0 4.4 3.6 8 8 8h60c4.4 0 8-3.6 8-8V509.7H624c6.7 0 10.4-7.7 6.3-12.9L518.3 355z"></path>
                </svg>
              </span>
            </div>
          </div>
        </div>
      </div>
      {searchFlag ? <SearchContent setearchbtn={setearchbtn} /> : null}
    </div>
  );
};

export default Layouts;
