import { Getdetail, IRootState } from '@/types';
import React from 'react';
import { useEffect, useState } from 'react';
import './index.less';
import { fomatTime } from "@/utils"
import { IRouteComponentProps, NavLink, useDispatch, useSelector, } from 'umi';
import HigLight from '@/components/HigLight';
import styles from "./index.less";
import Comment from '@/components/msgboard/comment/comment';
import {
  HeartFilled,
  CommentOutlined,
  ShareAltOutlined,
} from '@ant-design/icons';
import share from "@/components/share";
import classnames from "classnames";
const sondetail: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
  const dispatch = useDispatch();
  let id = props.match.params.id;
  console.log(id);
  const { sondetail, likeslist } = useSelector((state: IRootState) => state.knowledge);
  console.log(sondetail);
  const { knowtoc } = useSelector((state: IRootState) => state.knowledge);
  console.log(knowtoc);
  const { detail } = useSelector((state: IRootState) => state.knowledge)
  console.log(detail);
  const [comnentPage, setnewindex] = useState(1);
  // const [count, setCount] = useState(1)
  const [newlike, setnewlike] = useState(false)

  useEffect(() => {
    dispatch({
      type: 'knowledge/getsondetail',
      payload: id,
    });
  }, [id]);
  useEffect(() => {
    dispatch({
      type: "knowledge/getdetail",
      payload: sondetail.parentId
    })
  }, [sondetail.parentId])
  //评论
  useEffect(() => {
    dispatch({
      type: 'article/getArticleComment',
      payload: {
        id,
        page: comnentPage,
      },
    });
  }, [comnentPage]);

  function changedTabIndex(index: number, id: string) {
    setnewindex(index)
    document.getElementById(`${id}`)?.scrollIntoView()
  }
  let floors = document.querySelectorAll(".left_box")
  let liAll = document.querySelectorAll(".rboxb1>div>li")
  console.log(floors, liAll);
  useEffect(() => {
    let floors = document.querySelectorAll("h2,h3");
    let liAll = document.querySelectorAll(".rboxb1>div>li")
    console.log(floors, liAll);
    window.onscroll = function () {  //监听滚动事件
      let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
      let curIndex = 0;
      floors.forEach((item: any, index) => {
        {
          item.offsetTop - 90 <= scrollTop ? curIndex = index : null;
        }
      })
      //根据索引数设置楼层样式
      liAll.forEach((nav, index) => {
        index === curIndex ? nav.classList.add('active') : nav.classList.remove('active');
      });
      setnewindex(curIndex)
    }
  }, [floors])
  // 分享海报
  function fan(e: React.MouseEvent, item: Partial<Getdetail>) {
    e.stopPropagation()
    share(item)
  }
  useEffect(() => {
    let type = window.localStorage.getItem('likes') ? JSON.parse(window.localStorage.getItem('likes')!).includes(id) : null
    setnewlike(type)
  }, [])
  //点击喜欢
  const handleLikes = () => {
    let likes = window.localStorage.getItem("likes") ? JSON.parse(window.localStorage.getItem("likes")!) : []
    let index = likes.findIndex((item: string) => item === id)
    if (index !== -1) {
      likes.splice(index, 1)
      window.localStorage.setItem("likes", JSON.stringify(likes))
      dispatch({
        type: "knowledge/getknowLikes",
        payload: {
          id,
          type: 'dislike',
        }
      })
    } else {
      dispatch({
        type: "knowledge/getknowLikes",
        payload: {
          id,
          type: 'like',
        }
      })
      likes.push(id)
      window.localStorage.setItem("likes", JSON.stringify(likes))
    }
    if (window.localStorage.getItem('likes')) {
      let type = JSON.parse(window.localStorage.getItem('likes')!).includes(id)
      setnewlike(type)
    }
  }
  //跳评论
  function gocomment() {
    console.log(111);
    document.querySelector('.article_dateil_p')?.scrollIntoView()
  }
  return (
    <div className='box'>
      <p className="p11">
        <NavLink to={'/knowledge'} className="ap1">
          知识小册/
        </NavLink>
        <NavLink to={`/detail1/${detail && detail.id}`} className="ap2">{detail && detail.title}/</NavLink>
        <span>{sondetail.title}</span>
      </p>
      <div className='contbox'>
        <div className='leftbox1'>
          <div className='left_box'>
            <div className='lefthead'>
              <h1>{sondetail.title}</h1>
              <p><i>发布于{fomatTime(sondetail.publishAt!)}.阅读量{sondetail.views}</i></p>
            </div>
            <HigLight>
              <div dangerouslySetInnerHTML={{ __html: sondetail.html! }}></div>
            </HigLight>
          </div>

          <div className={styles.article_dateil_comment}>
            <p className='article_dateil_p'>评论</p>
            <Comment />
            <div className={styles.article_dateil_comment_bottom}></div>
          </div>
        </div>
        <div className='rightbox1'>
          <div className="rboxa">
            <h3>{detail && detail.title}</h3>
            <div className='hbox'>
              {
                detail && detail.children && detail.children.map((item2, index2) => {
                  return <li key={index2} className='hli1' onClick={() => {
                    props.history.replace('/sondetail/' + item2.id)
                  }}>
                    {item2.title}
                  </li>
                })
              }
            </div>
          </div>
          <div className="rboxb">
            <p>目录</p>
            <div className='rboxb1'>
              <div>
                {
                  knowtoc && knowtoc.map((item, index) => {
                    if (item.level == "2") {
                      return <li key={index} className='rlib' onClick={() => changedTabIndex(index, item.id)}>
                        <p>{item.text}</p>
                      </li>
                    } else {
                      return <li key={index} className='rlic' onClick={() => changedTabIndex(index, item.id)}>
                        <p>{item.text}</p>
                      </li>
                    }
                  })
                }
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='_3tC3y9W6nEYVdAwGypNta9'>
        <span
          onClick={() => handleLikes()}
          className={classnames(styles.like, newlike ? styles.like_active : '')}>
          <HeartFilled />
          <b>{likeslist}</b>
        </span>
        <span onClick={() => gocomment()}><CommentOutlined /></span>
        <span onClick={e => fan(e, detail)}><ShareAltOutlined /></span>
      </div>
    </div >
  );
};

export default sondetail



