
import Right_bot from '@/components/Right_bot'
import Right_top from '@/components/Right_top'
import { IArchivesItem, IRootState, July2, _2020 } from '@/types'
import moment from 'moment'
import React from 'react'
import { useEffect } from 'react'
import { NavLink, useDispatch, useSelector } from 'umi'
import styles from "./index.less"

const archives: React.FC = (props) => {
  const dispatch = useDispatch()
  const { archives } = useSelector((state: IArchivesItem) => state.archives)
  const { recommend, category } = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    dispatch({
      type: 'archives/getArchivesvList'
    })
  }, [])

  return (
    <div className={styles.outer}>
      <section className={styles.first}>
        <div className={styles.firstitem}>
          <div className={styles.maintop}>
            <p>
              <span>归档</span>
            </p>
            <p>
              共计
              <span>33</span>
              篇
            </p>
          </div>

          <div className={styles.mainmiddle}>
            <h2>{Object.keys(archives)[1]}</h2>
            {archives[2021] && Object.keys(archives[2021]).map((item, index) => {
              return <div className={styles.archivesitem} key={index}>
                <h3>{item}</h3>
                {
                  archives[2021][item].map((item2: July2) => {
                    return <ul key={item2.id}>
                      {
                        <li>
                          <NavLink to={`/article/${item2.id}`}>
                            <span className={styles.time}>
                              {moment(item2.publishAt).format("MM-DD")}
                            </span>
                            <span className={styles.headline}>
                              {item2.title}
                            </span>
                          </NavLink>
                        </li>
                      }
                    </ul>
                  })
                }
              </div>
            })}
          </div>
          <div className={styles.mainmiddle}>
            <h2>{Object.keys(archives)[0]}</h2>
            {archives[2020] &&
              Object.keys(archives[2020]).map((item, index) => {
                return (
                  <div className={styles.archivesitem} key={index}>
                    <h3>{item}</h3>
                    {archives[2020][item].map((item2: July2) => {
                      return (
                        <ul key={item2.id}>
                          {
                            <li>
                              <NavLink to={`/article/${item2.id}`}>
                                <span className={styles.time}>
                                  <time>
                                    {' '}
                                    {moment(item2.publishAt).format('MM-DD')}
                                  </time>
                                </span>
                                <span className={styles.headline}>
                                  {item2.title}
                                </span>
                              </NavLink>
                            </li>
                          }
                        </ul>
                      );
                    })}
                  </div>)
              })}
          </div>
        </div>
      </section>
      <aside className={styles.zuo}>
        <div className={styles.sticky}>
          <Right_top List={recommend} />
          <Right_bot List={category} />
        </div>
      </aside>
    </div>
  )
}
export default archives;
