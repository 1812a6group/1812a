import HigLight from '@/components/HigLight';
import ImageView from '@/components/ImageView';
import {
  HeartFilled,
  CommentOutlined,
  ShareAltOutlined,
} from '@ant-design/icons';
import { IArticleItem, IRootState } from '@/types';
import moment from 'moment';
import React from 'react';
import { useState, useEffect } from 'react';
import Comment from '@/components/msgboard/comment/comment';
import { IRouteComponentProps, useDispatch, useSelector } from 'umi';
import styles from './id.less';
import Right_top from '@/components/Right_top';
import classnames from "classnames"
import Mask from '@/components/Mask';
const ArticleDateil: React.FC<IRouteComponentProps<{ id: string }>> = (
  props,
) => {
  const id = props.match.params.id;
  const dispatch = useDispatch();
  const { articledateil, recommend, catalog, likeslist } = useSelector(
    (state: IRootState) => state.article,
  );
  const [comnentPage, setcomnentPage] = useState(1);
  const [newindex, setnewindex] = useState(0)
  const [newlike, setnewlike] = useState(false)


  //文章详情
  useEffect(() => {
    dispatch({
      type: 'article/getArticleDateil',
      payload: id,
    });
  }, [id]);
  //评论
  useEffect(() => {
    dispatch({
      type: 'article/getArticleComment',
      payload: {
        id,
        page: comnentPage,
      },
    });
  }, [comnentPage]);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [item, setItem] = useState({})
  const showModal = (item: IArticleItem) => {
    setItem(item)
    setIsModalVisible(true);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };
  //目录
  function changedTabIndex(index: number, id: string) {
    setnewindex(index)
    document.getElementById(`${id}`)?.scrollIntoView()
  }
  let floors = document.querySelectorAll("h2,h3");
  useEffect(() => {
    let floors = document.querySelectorAll("h2,h3");
    window.onscroll = function () {
      let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
      let curIndex = 0;
      floors.forEach((item: any, index) => {
        {
          item.offsetTop - 90 <= scrollTop ? curIndex = index : null;
        }
      })
      setnewindex(curIndex)
    }
  }, [floors])

  useEffect(() => {
    let type = window.localStorage.getItem('likes') ? JSON.parse(window.localStorage.getItem('likes')!).includes(id) : null
    setnewlike(type)
  }, [])
  //点击喜欢
  const handleLikes = () => {
    let likes = window.localStorage.getItem("likes") ? JSON.parse(window.localStorage.getItem("likes")!) : []
    let index = likes.findIndex((item: string) => item === id)
    if (index !== -1) {
      likes.splice(index, 1)
      window.localStorage.setItem("likes", JSON.stringify(likes))
      dispatch({
        type: "article/getArticleLikes",
        payload: {
          id,
          type: 'dislike',
        }
      })
    } else {
      dispatch({
        type: "article/getArticleLikes",
        payload: {
          id,
          type: 'like',
        }
      })
      likes.push(id)
      window.localStorage.setItem("likes", JSON.stringify(likes))
    }
    if (window.localStorage.getItem('likes')) {
      let type = JSON.parse(window.localStorage.getItem('likes')!).includes(id)
      setnewlike(type)
    }
  }
  //跳评论
  const gocomment = () => {
    document.querySelector('.article_dateil_p')?.scrollIntoView()
  }

  let password = null,
    pay = null;
  if (articledateil.needPassword) {
    password = '';
  }
  if (articledateil.totalAmount) {
    pay = <button onClick={async () => {
      let result = await fetch('http://127.0.0.1:7001/pay', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ totalAmount: articledateil.totalAmount, id: articledateil.id })
      }).then(res => res.json());
      if (result.url) {
        window.location.href = result.url;
      }
    }}>立即支付：¥{articledateil.totalAmount}</button>
  }
  return (
    <div className={styles.container}>
      <div className={styles.article_dateil}>
        <div className={styles.article_dateil_flex}>
          <div className={styles.article_flex_all} onClick={() => handleLikes()}>
            <span className={styles.article_num}>{likeslist}</span>
            <span className={classnames(styles.like, newlike ? styles.like_active : "")} >
              <HeartFilled />
            </span>
          </div>
          <div className={styles.article_flex_all}>
            <span className={styles.comment} onClick={() => gocomment()}><CommentOutlined /></span>
          </div>
          <div className={styles.article_flex_all}>
            <span className={styles.share} onClick={() => showModal(articledateil)}><ShareAltOutlined /></span>
          </div>
        </div>
        <section className={styles.article_dateil_left}>
          <ImageView>
            <HigLight>
            {password}
            {pay}
              <div className={styles.article_dateil_item}>
                {articledateil.cover && (
                  <img src={articledateil.cover} alt="" />
                )}
                <h1>{articledateil.title}</h1>
                <p className={styles.article_publish}>
                  发布于 {moment(articledateil.publishAt).format('llll')} •
                  阅读量{articledateil.views}
                </p>
                {!password &&<div
                  dangerouslySetInnerHTML={{ __html: articledateil.html! }}
                ></div> }
                
                <p>
                  发布于{moment(articledateil.publishAt).format('llll')} |
                  版权信息：非商用-署名-自由转载
                </p>
              </div>
            </HigLight>
          </ImageView>
          <div className={styles.article_dateil_comment}>
            <p className={classnames("article_dateil_p", styles.article_dateil_p)} >评论</p>

            <Comment />

            <div className={styles.article_dateil_comment_bottom}></div>
          </div>
        </section>
        <aside className={styles.article_dateil_right}>
          <div className={styles.article_dateil_right_sticky}>
            <Right_top List={recommend} />
            <div className={styles.article_dateil_right_bottom}>
              <header>目录</header>
              <main>
                <div>
                  <div>
                    {
                      catalog.map((item, index) => {
                        if (item.level == "2") {
                          return <div key={item.id} className={styles.catalogitem}>
                            <div className={classnames(styles.catalogitemcontent, newindex == index ? styles.active : "")}
                              onClick={() => changedTabIndex(index, item.id)}
                              style={{ opacity: 1, height: '32px', paddingLeft: "12px" }}
                            >{item.text}</div>
                          </div>
                        } else {
                          return <div key={item.id} className={styles.catalogitem_son}>
                            <div className={classnames(styles.catalogitemcontent, newindex == index ? styles.active : "")}
                              onClick={() => changedTabIndex(index, item.id)}
                              style={{ opacity: 1, height: '32px', paddingLeft: "25px" }}
                            >{item.text}</div>
                          </div>
                        }
                      })
                    }
                  </div>
                </div>
              </main>
            </div>
          </div>
        </aside>
      </div>
      <Mask item={articledateil} isModalVisible={isModalVisible} handleCancel={handleCancel} />
    </div >
  );
};

export default ArticleDateil;
