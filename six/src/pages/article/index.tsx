import { IRootState } from '@/types';
import { useEffect, useState } from 'react';
import Styles from './index.less';
import { Carousel } from 'antd';
import Swiper from '@/components/Swiper';
import Right_top from '@/components/Right_top';
import { NavLink, useDispatch, useSelector } from 'umi';
import Scroll from '@/components/Scroll'

const IndexPage: React.FC = (props) => {
  let [page, setPage] = useState(1)
  const dispatch = useDispatch()
  const { recommend, articleList, count, category, tagTitle } = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    dispatch({
      type: 'article/getArticleList',
      payload: page
    })
  }, [page])
  function pullupLoader() {
    setPage((page) => page + 1);
  }

  return (
    <div className={Styles.box}>
      {/* 页面左侧 */}
      <section className={Styles.Left}>
        <div className={Styles.swiper}>
          {/* 轮播图 */}
          <Carousel autoplay={true} >
            {
              recommend.map(item => {
                return item.isRecommended && <Swiper item={item} key={item.id} />
              })
            }
          </Carousel >
        </div>
        <div className={Styles.cont}>
          {/* 分类路由 */}
          <header>
            <NavLink to={'/article'} activeStyle={{ color: '#ff0064' }}>所有</NavLink>
            {
              category.map(item => {
                return <NavLink to={`/category/${item.value}`} activeStyle={{ color: '#ff0064' }} key={item.id}>{item.label}</NavLink>
              })
            }
          </header>
          {/* 上拉加载的数据 */}
          <main>
            <Scroll List={articleList} Count={count} page={page} pullupLoader={pullupLoader} />
          </main>
        </div>
      </section>
      {/* 页面右侧 */}
      <aside className={Styles.right}>
        <div className={Styles.sticky}>
          {/* 推荐阅读 */}
          <Right_top List={recommend} />
          <div className={Styles.bottom}>
            <div className={Styles.title}>
              <span>文章标签</span>
            </div>
            <div className={Styles.bottom_main}>
              {
                tagTitle.map(item => {
                  return <li key={item.id}>
                    <NavLink to={`/tag/${item.value}`}>{item.label}[{item.articleCount}]</NavLink>
                  </li>
                })
              }
            </div>
          </div>
        </div>
      </aside>
    </div>
  );
};
export default IndexPage;
