
import Right_bot from '@/components/Right_bot';
import Right_top from '@/components/Right_top'
import { IRootState } from '@/types';
import { NavLink, Redirect, useSelector } from 'umi';
import Styles from './all.less'

import { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom'
interface Props extends RouteComponentProps {

}
const fe: React.FC<Props> = (props) => {
  let [Ind, setInd] = useState(0)
  const { recommend, tagTitle, tagCount, category } = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    let name = props.location.pathname.substr(5)
    let num = tagTitle.findIndex(item => item.value === name)
    setInd(num)
  }, [tagTitle])
  return (
    <div className={Styles.box}>
      <section className={Styles.Left}>
        <div className={Styles.title}>
          <p><span>{tagTitle[Ind] && tagTitle[Ind].label}</span>分类文章</p>
          <p>共搜索到<span>{tagCount}</span>篇</p>
        </div>
        <div className={Styles.cont}>
          <header>
            <NavLink to={'/article'}>所有</NavLink>
            {props.location.pathname === '/tag' ? <Redirect exact from='/tag' to='/tag/git' /> : ''}
            {
              tagTitle.map((item, index) => {
                return <NavLink to={`/tag/${item.value}`} activeStyle={{ color: '#ff0064' }} key={index} onClick={() => {
                  setInd(index)
                }}>{item.label}</NavLink>
              })
            }
          </header>
          <main>
            {
              props.children
            }
          </main>
        </div>
      </section>
      <aside className={Styles.right}>
        <div className={Styles.sticky}>
          <Right_top List={recommend} />
          <Right_bot List={category} />
        </div>
      </aside>
    </div>
  )
}
export default fe
