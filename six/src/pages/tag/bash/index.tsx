
import React, { useEffect, useState } from 'react'
import style from '../all.less'
import { useDispatch, useSelector } from 'umi';
import { IRootState } from '@/types';
import Scroll from '@/components/Scroll'
const bash: React.FC = (props) => {
  let [page, setPage] = useState(1)
  const dispatch = useDispatch()
  const { tagList, tagCount } = useSelector((state: IRootState) => state.article)
  useEffect(() => {
    dispatch({
      type: 'article/getTagClassify',
      value: 'bash',
      payload: page
    })
  }, [page])


  function pullupLoader() {
    setPage(page => page + 1)
  }

  return (
    <div className={style.main}>
      <Scroll List={tagList} Count={tagCount} page={page} pullupLoader={pullupLoader} />
    </div>
  )
}
export default bash
