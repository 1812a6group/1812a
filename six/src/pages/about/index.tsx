import { IRootState } from '@/types';
import { useEffect } from 'react';
import { useState } from 'react';
import styles from './index.less';
import moment from 'moment';
import Comment from '../../components/msgboard/comment/comment';
moment.locale('zh-cn');
import Answer from '@/components/msgboard/reply/Answer';
import { useDispatch, useSelector } from 'umi';
import Scroll from '@/components/Scroll';
export default function About() {
  let [page, setPage] = useState(1);
  const dispatch = useDispatch();
  const { aboutlist, aboutCount } = useSelector((state: IRootState) => state.about);
  const { recommend, count } = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    dispatch({
      type: 'about/getComment',
      payload: page
    })
  }, [page])

  const getPage = (page: number) => {
    setPage(page)
  }
  function pullupLoader() {
    setPage(page => page + 1)
  }
  return (
    <div className={styles.about_warp}>
      <div className={styles.wrapper}>
        <div className={styles.about_content}>
          <div className={styles.about_img}>
            <img src="https://wipi.oss-cn-shanghai.aliyuncs.com/2020-04-04/despaired-2261021_1280.jpg" alt="" />
          </div>
          <div>
            <div>
              <h2 className={styles.about_h2}>
                这世界只有一种英雄主义，就是在看清了生活的真相之后，依然热爱生活。
              </h2>
            </div>
          </div>
          <div>
            <h3 className={styles.about_h3}>评论</h3>
          </div>
          <Comment />
        </div >
        {
          aboutlist.map(item => {
            return <div key={item.id} className={styles.aboutdiv}>
              <p>
                <span className={styles.aboutname} style={{ background: '#' + Math.random().toString(16).substr(2, 6).toUpperCase() }}>{item.name[0]}</span>
                <strong>{item.name}</strong></p>
              <p dangerouslySetInnerHTML={{ __html: item.content }}></p>
              <div className={`${styles.aboutmodel}`}>
                <span className={styles.aboutrevert}>{item.userAgent} · <time dateTime={item.createAt}>
                  {
                    moment(+new Date(item.createAt)).fromNow()
                  }
                </time>
                  <span className={styles.reply}>
                    <Answer name={item.name} id={item.id} email={item.email} />
                  </span>
                </span>
                {
                  item.children && item.children.map(item2 => {
                    return <div key={item2.id}>
                      <br />
                      <p><span className={styles.aboutname} style={{ background: '#' + Math.random().toString(16).substr(2, 6).toUpperCase() }}>{item2.name[0]}</span><strong>{item2.name}</strong>回复<strong>{item.name}</strong></p>
                      <p className={`${styles.aboutconent}`} dangerouslySetInnerHTML={{ __html: item2.content }}></p>
                      <p className={`${styles.aboutrevert} ${styles.aboutrever2}`}>{item2.userAgent} · <time dateTime={item.createAt}>
                        {
                          moment(+new Date(item2.createAt)).fromNow()
                        }
                      </time>
                        <span className={styles.reply}>
                          <Answer name={item2.name} id={item2.id} email={item2.email} />
                        </span>
                      </p>
                    </div>
                  })
                }  </div>
            </div>
          }
          )
        }

        <div className={styles.foot}>
          <h3 className={styles.about_h3}>
            推荐阅读
          </h3>
          <div className={styles.recomend}>
            <Scroll List={recommend} Count={count} page={page} pullupLoader={pullupLoader} />
          </div>
        </div>

      </div>
    </div>
  )
}