import React, { useEffect, useState } from 'react'
import style from '../all.less'
import { useDispatch, useSelector } from 'umi';
import { IRootState } from '@/types';
import Scroll from '@/components/Scroll';
const be: React.FC = (props) => {
  let [page, setPage] = useState(1)
  const dispatch = useDispatch()
  const { catrgoryClassify, catrgoryClassifyNum } = useSelector((state: IRootState) => state.article)
  useEffect(() => {
    dispatch({
      type: 'article/getCategoryClassify',
      value: 'be',
      payload: page
    })
  }, [page])
  function pullupLoader() {
    setPage(page => page + 1)
  }

  return (
    <div className={style.main}>
      <Scroll List={catrgoryClassify} Count={catrgoryClassifyNum} page={page} pullupLoader={pullupLoader} />
    </div>
  )
}
export default be
