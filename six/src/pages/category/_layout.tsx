
import Right_top from '@/components/Right_top';
import { IRootState } from '@/types';
import { NavLink, Redirect, useSelector } from 'umi';
import Styles from './all.less'

import { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom'
interface Props extends RouteComponentProps {

}
const fe: React.FC<Props> = (props) => {
  let [Ind, setInd] = useState(0)
  const { recommend, category, tagTitle, catrgoryClassifyNum } = useSelector((state: IRootState) => state.article);
  useEffect(() => {
    let name = props.location.pathname.substr(10)
    let num = category.findIndex(item => item.value === name)
    setInd(num)
  }, [category])
  return (
    <div className={Styles.box}>
      <section className={Styles.Left}>
        <div className={Styles.title}>
          <p><span>{category[Ind] && category[Ind].label}</span>分类文章</p>
          <p>共搜索到<span>{catrgoryClassifyNum}</span>篇</p>
        </div>
        <div className={Styles.cont}>
          <header>
            <NavLink to={'/article'}>所有</NavLink>
            {props.location.pathname === '/category' ? <Redirect exact from='/category' to='/category/fe' /> : ''}
            {
              category.map((item, index) => {
                return <NavLink to={`/category/${item.value}`} activeStyle={{ color: '#ff0064' }} key={index} onClick={() => {
                  setInd(index)
                }}>{item.label}</NavLink>
              })
            }
          </header>
          <main>
            {
              props.children
            }
          </main>
        </div>
      </section>
      <aside className={Styles.right}>
        <div className={Styles.sticky}>
          <Right_top List={recommend} />
          <div className={Styles.bottom}>
            <div className={Styles.title}>
              <span>文章标签</span>
            </div>
            <div className={Styles.bottom_main}>
              {
                tagTitle.map(item => {
                  return <li key={item.id}>
                    <NavLink to={`/tag/${item.value}`}>{item.label}[{item.articleCount}]</NavLink>
                  </li>
                })
              }
            </div>
          </div>
        </div>
      </aside>
    </div>
  );
};
export default fe;
