import { IRootState } from '@/types';
import React from 'react';
import { useEffect } from 'react';
import './index.less';
const moment = require('moment');
import { fomatTime } from '@/utils';  //引入时间
import { IRouteComponentProps, NavLink, useDispatch, useSelector } from 'umi';
import { useState } from 'react';
import share from '@/components/share';  //引入海报插件

const detail1: React.FC<IRouteComponentProps<{ id: string }>> = (props) => {
  let id = props.match.params.id;  //获取传过来的id
  const dispatch = useDispatch();
  let [page, setPage] = useState(1);
  const { detail } = useSelector((state: IRootState) => state.knowledge);
  const { knowlist } = useSelector((state: IRootState) => state.knowledge);
  useEffect(() => {
    dispatch({  //获取左侧数据
      type: 'knowledge/getknowledge',
      payload: page,
    });
  }, [page]);
  useEffect(() => {
    dispatch({  //获取详情数据
      type: 'knowledge/getdetail',
      payload: id,
    });
  }, [id]);
  function sharebtn(e: React.MouseEvent, item: any) {
    e.stopPropagation()
    e.preventDefault()
    share(item)
  }
  return (
    <div className="aa">
      <p className="p11">
        <NavLink to={`/knowledge`} className="ap1">
          知识小册/
        </NavLink>
        <span>{detail.title}/</span>
      </p>
      <div className="bboo">
        <div className="leftbox">
          <h3>{detail.title}</h3>
          <div className="box2">
            <div className="box3">
              <img src={detail.cover} alt="" />
              <div className="sonbox">
                <h4>{detail.title}</h4>
                <p className="p2">{detail.summary}</p>
                <p className="p3">
                  {detail.views}次阅读.
                  <span>{fomatTime(detail.createAt!)}</span>
                </p>
                <button onClick={()=>{
                  props.history.push({
                    pathname:`/sondetail/${detail.children&&detail.children[0].id}`
                  })
                }}>开始阅读</button>
              </div>
              <div className="sonbox2">
                {detail.children &&
                  detail.children.map((item1: { id: any; title: boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined; publishAt: string; }, index1: React.Key | null | undefined) => {
                    return (                   
                      // 点击列表 跳转第三层详情
                      <NavLink
                        to={`/sondetail/${item1.id}`}
                        key={index1}
                        className="li1"
                      >
                        <span className="span3">{item1.title}</span>
                        <span className="span4">
                          {fomatTime(item1.publishAt!)}&gt;
                        </span>
                      </NavLink>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
        <div className="rightbox">
          <h3>其他知识笔记</h3>
          <div className="rbox1">
            {knowlist.map((item, index) => {
              return (
                item.id != id && (
                  <li
                    key={index}
                    className="li2"
                    onClick={() => {
                      props.history.replace('/detail1/' + item.id);
                      console.log(item.id);
                    }}
                  >
                    <div className="ce1">
                      <h3>{item.title}</h3> |
                      <span>
                        {moment(`${item.createAt}`).fromNow()}
                      </span>
                    </div>
                    <div className="ce3">
                      <img src={item.cover} alt="" />
                      <div className="div1">
                        <p>{item.summary}</p>
                        <p className="p2">
                        <span>
                        <svg viewBox="64 64 896 896" focusable="false" data-icon="eye" width="1em" height="1em" 
                        fill="currentColor" aria-hidden="true">
                        <path d="M942.2 486.2C847.4 286.5 704.1 186 512 186c-192.2 0-335.4 100.5-430.2 300.3a60.3 60.3 0 000 51.5C176.6 737.5 319.9 838 512 838c192.2 0 335.4-100.5 430.2-300.3 7.7-16.2 7.7-35 0-51.5zM512 766c-161.3 0-279.4-81.8-362.7-254C232.6 339.8 350.7 258 512 258c161.3 0 279.4 81.8 362.7 254C791.5 684.2 673.4 766 512 766zm-4-430c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm0 288c-61.9 0-112-50.1-112-112s50.1-112 112-112 112 50.1 112 112-50.1 112-112 112z"></path>
                        </svg>
                            {item.views}
                          </span>
                          .
                          <span onClick={e => sharebtn(e, item)}>
                          <svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" 
                          fill="currentColor" aria-hidden="true">
                          <path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path>
                          </svg>
                          分享</span>
                        </p>
                      </div>
                    </div>
                  </li>
                )
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default detail1;
