import './index.less';
import { useDispatch, useSelector } from 'umi';
import { useEffect } from 'react';
// import { IRootState1 } from '@/types';
import moment from 'moment';
import Read from '@/components/msgboard/read/Read';
import Center from '@/components/msgboard/center/Center';
import Header from '@/components/msgboard/header/Header';
import { IRootState } from '@/types';
const msgboard: React.FC = (props) => {
  moment.locale('zh-cn');
  const dispath = useDispatch();
  const { recommend, list, total } = useSelector(
    (state: IRootState) => state.msgboard,
  );

  useEffect(() => {
    dispath({
      type: 'msgboard/getList',
    });
  }, []);
  // 留言板主页
  return (
    <div className="msgboard">
      <Header />
      <Center recommend={recommend} total={total} />
      <Read />
    </div>
  );
};

export default msgboard;
