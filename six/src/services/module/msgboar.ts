import { comment } from '@/types';
import { request } from 'umi';

// 获取文章列表
export function getMsgboar(page = 1, pageSize = 6) {
  return request('/api/comment/host/8d8b6492-32e5-44e5-b38b-9a479d1a94bd', {
    params: {
      page,
      pageSize,
    },
  });
}

// 获取推荐文章
export function getList() {
  return request('/api/article/recommend');
}

// 发布
export function setcomment(payload: comment) {
  return request('/api/comment', {
    method: 'post',
    data: payload,
  });
}
