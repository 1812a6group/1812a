import { request } from 'umi';

// 获取知识小册
export function getknowledge(page: number, pageSize = 12, status = 'publish') {
  return request('/api/knowledge', {
    params: {
      page,
      pageSize,
      status,
    },
  });
}
// /api/category

// 获取详情数据接口
export function getdetail(id: string) {
  return request(`/api/knowledge/${id}`);
}

// 获取子详情页数据
export function getsondetail(id: string) {
  return request(`/api/knowledge/${id}`)
}
// xihuan
// api/knowledge/755b5b6c-363c-453c-9063-2408f14de55f/likes
export function getknowLikes(id: string, type: string) {
  return request(`/api/knowledge/${id}/likes`, {
    method: "POST",
    data: {
      type,
      id
    }
  })
}
