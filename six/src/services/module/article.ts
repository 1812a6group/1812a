
import { request } from "umi"

// 像一个 api 一样  获取路径 数据

// 获取文章列表
export function getArticleList(
  page: number,
  pageSize = 12,
  status = 'publish',
) {
  return request('/api/article', {
    params: {
      page,
      pageSize,
      status,
    },
  });
}

//获取文章分类
export function getCategoryClassify(value: string, page: number, pageSize = 12, status = 'publish') {
  return request(`/api/article/category/${value}`, {
    params: {
      page,
      pageSize,
      status
    }
  })
}
//tag分类数据
export function getTagClassify(value: string, page: number, pageSize = 12, status = 'publish') {
  return request(`/api/article/tag/${value}`, {
    params: {
      page,
      pageSize,
      status
    }
  })
}

//喜欢
export function getArticleLikes(id: string, type: string) {
  return request(`/api/article/${id}/likes`, {
    method: "POST",
    data: {
      type,
      id
    }
  })
}