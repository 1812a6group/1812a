
import { request } from "umi";

//获取关于评论
export function getComment(page: number, pageSize = 6) {
  return request('/api/comment/host/a5e81ffe-0ad0-4be9-acca-c0462b1b98a1', {
    params: {
      page,
      pageSize,
    },
  });
}
