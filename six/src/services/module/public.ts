import { request } from "umi"
import { IPostItem } from "@/types";
//获取推荐阅读的数据
export function getRecommend() {
    return request('/api/article/recommend')
}
export function getcategory(articleStatus = 'publish') {
    return request('/api/category', {
        params: {
            articleStatus,
        }
    })
}
//获取文章标题
export function getArticleTitle(status = 'publish') {
    return request(`/api/category`, {
        params: {
            status
        }
    })
}
export function getTag(status = 'publish') {
    return request('/api/tag', {
        params: {
            status
        }
    })
}
//获取文章详情
export function getArticleDateil(id: string) {
    return request(`/api/article/${id}/views`, {
        method: "POST"
    })
}
//获取评论列表
export function getArticleComment(id: string, page = 1, pageSize = 6) {
    return request(`/api/comment/host/${id}?page=${page}&pageSize=6`)
}
//生成海报
export function genePoster(data: IPostItem) {
    return request(`/api/poster`, {
        method: 'POST',
        data
    });
}