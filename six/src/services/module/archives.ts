import { request } from 'umi';

export function getArchivesvList() {
  return request('/api/article/archives');
}
