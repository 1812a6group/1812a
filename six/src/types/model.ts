import { ArticleModelState } from "@/models/article";
import { KnowledgeModelState } from "@/models/knowledge"
import { ArchivesModelState } from "@/models/archives";
import { MsgboardModelState } from '@/models/msgboard'
import { AboutModelState } from '@/models/about'
import { SearchModelState } from '@/models/search'
import { LanguageModelState } from "@/models/language";
export interface IRootState {
  article: ArticleModelState;
  about: AboutModelState;
  archives: ArchivesModelState;
  msgboard: MsgboardModelState;
  knowledge: KnowledgeModelState;
  search: SearchModelState
  language: LanguageModelState
}
