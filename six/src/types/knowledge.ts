interface IRootObject {
  id: string;
  title: string;
  cover?: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category?: Category;
  tags: Category[];
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}
import { ReactNode } from 'react';

export interface KnowledgeObj {
  label: ReactNode;
  id: string;
  title: string;
  cover?: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category?: Category;
  tags: Category[];
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}
export interface KnowledgeItem {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}

export interface Getdetail {
  getdetail: any;
  id: string;
  parentId?: any;
  order: number;
  title: string;
  cover: string;
  summary: string;
  content?: any;
  html?: any;
  toc?: any;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  children: Child[];
}

interface Child {
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}

export interface Getsondetail {
  level: string;
  text: ReactNode;
  id: string;
  parentId: string;
  order: number;
  title: string;
  cover?: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
}
