export interface RootObject1 {
  id: string;
  name: string;
  email: string;
  content: string;
  html?: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: any;
  replyUserName?: any;
  replyUserEmail?: any;
  createAt: string;
  updateAt: string;
  children?: Child[];
}

export interface Child {
  id: string;
  name: string;
  email: string;
  content: string;
  html?: any;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
  createAt: string;
  updateAt: string;
}
