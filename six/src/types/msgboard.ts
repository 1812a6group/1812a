import { JSXElementConstructor } from 'react';
import { ReactPortal } from 'react';
import { ReactNodeArray } from 'react';
import { ReactElement } from 'react';
import { ReactNode } from 'react';

export interface IMsgboard {
  id: string;
  name: string;
  email: string;
  content: string;
  html?: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: any;
  replyUserName?: any;
  replyUserEmail?: any;
  createAt: string;
  updateAt: string;
  children: Child[];
}

interface Child {
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId: string;
  replyUserName: string;
  replyUserEmail: string;
  createAt: string;
  updateAt: string;
}

export interface RootObject {
  articleCount:
    | string
    | number
    | boolean
    | {}
    | ReactElement<any, string | JSXElementConstructor<any>>
    | ReactNodeArray
    | ReactPortal
    | null
    | undefined;
  label: ReactNode;
  id: string;
  title: string;
  cover?: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category?: Category;
  tags: Category[];
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}

export interface comment {
  content: string;
  email: string;
  hostId: string;
  name: string;
  parentCommentId?: string;
  replyUserEmail?: string;
  replyUserName?: string;
  url: string;
}
