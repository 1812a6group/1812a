export interface IPostItem{
    width: number;
    height: number;
    html: string;
    name: string;
    pageUrl: string;
}
