// 定义数据类型
import { ReactNode } from 'react';
export interface IArticleItem {
  label?: ReactNode;
  id?: string;
  title?: string;
  cover?: string;
  summary?: string;
  content?: string;
  html?: string;
  toc?: string;
  status?: string;
  views?: number;
  likes?: number;
  isRecommended?: boolean;
  needPassword?: boolean;
  isCommentable?: boolean;
  publishAt?: string;
  createAt?: string;
  updateAt?: string;
  category?: Category;
  tags?: Category[];
}

interface Category {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}
//类别
export interface ICategory {
  text: ReactNode;
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
  password?: any;
  totalAmount: string;
}
//文章标签
export interface Ilabel {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
  password?: any;
  totalAmount: string;
}
export interface ItemMask {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  password?: any;
  totalAmount: string;
}
//tag
export interface Itag {
  id: string;
  title: string;
  cover: string;
  summary: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: Tag[];
  password?: any;
  totalAmount: string;
}

interface Tag {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}
export interface ICategoryfe {
  id: string;
  title: string;
  cover: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  category?: Categorys;
  tags?: Categorys[]
}

interface Categorys {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}

export interface IScroll {
  id: string;
  title: string;
  cover?: string;
  summary?: string;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: Tag[];
  category?: Tag;
}

interface Tag {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}


export interface ItemCatalog {
  level: string;
  id: string;
  text: string;
}
