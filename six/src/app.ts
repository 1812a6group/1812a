import { createLogger } from 'redux-logger';
import { message } from 'antd';
import './font/iconfont.css';
// import { RequestConfig } from 'umi';
import { RequestConfig } from "umi"
import Nprogress from 'nprogress'
import 'nprogress/nprogress.css'
Nprogress.configure({ showSpinner: false })
export const dva = {
  config: {
    // onAction: createLogger(),
    onError(e: Error) {
      message.error(e.message, 3);
    },
  },
};
// 进度条
export function onRouteChange({ matcheRoutes }: any) {
  Nprogress.start();
  setTimeout(() => {
    Nprogress.done()
  }, 2000)
}

<<<<<<< HEAD
const baseUrl = 'https://creation.shbwyz.com';
=======
<<<<<<< HEAD
const baseUrl = 'https://creationapi.shbwyz.com';
=======
const baseUrl = process.env.NODE_ENV==='production'?'https://creationapi.shbwyz.com':'https://creationapi.shbwyz.com';
>>>>>>> 3c141d5dff3f0c1e63040570b4b0e93203387e1d
>>>>>>> main
export const request: RequestConfig = {
  timeout: 100000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [
    (url: any, options: any) => {
      return {
        url: `${baseUrl}${url}`,
        options,
      };
    },
  ],

  responseInterceptors: [
    (response: any) => {
      const codeMaps: { [key: number]: string } = {
        400: '错误的请求',
        403: '禁止访问',
        404: '找不到资源',
        500: '服务器内部错误',
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
      };
      if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1) {
        message.error(codeMaps[response.status]);
      }
      return response;
    },
  ],
};









