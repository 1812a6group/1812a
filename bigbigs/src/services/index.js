export * from './modules/home';
export * from './modules/discount';
export * from './modules/main';
export * from './modules/city';
export * from './modules/tasty';
export * from './modules/coupon';
export * from './modules/collect';
