import request from '../../utils/request';

export function getCollectList() {
    return request.post('/basic/shop/collections', {
        "page": 1,
        "pageSize": 6,
        "latitude": 39.95933151245117,
        "longitude": 116.29844665527344,
        "likeType": 1,
    })
}

export function getCollectShopList() {
    return request.post('/c_msh/mLife/goods/goodCollectList', {
        "pageNo": 1,
        "pageSize": 5,
    })
}