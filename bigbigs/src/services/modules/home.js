import fly from "../../utils/request";
export function getBanner() {
    return fly.post('/basic/app/banner')
}
export function getHomeList() {
    return fly.post('/basic/app/homeFunctionWidget')
}
export function getOperatList() {
    return fly.post('/basic/app/operatList')
}