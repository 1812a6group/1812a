import fly from "../../utils/request";
export function getdiscountList() {
    return fly.post('msh/app/index.php?i=5&t=0&v=1.0.1&from=wxapp&c=entry&a=wxapp&do=index&&m=zofui_sales&sign=49a0572da79fb2457787dc3ea91e249c', {
        "op": "info",
        "lat": 39.95933,
        "lng": 116.29845,
        "zfid": 0,
        "isnew": 1,
        "isposter": 0,
        "shopid": 0,
        "cityId": 19,
        "mwtoken": "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqtQNMJwnVZp76YqPVMtsVVh4XmpdLdIF3rNSY-BJe8dSyjvPbVYY-BQli6uLY-BEil4XrICqEntClyPqOXxHz0Qrp5wBODD6AGADJ2NX-CS8QoMNY-B8O2bX-C50uH9Yoi9wbmevKQrr7Nrm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
        "from": "wxapp"
    }, {
        headers: {
            "content-type": "application/x-www-form-urlencoded"
        }
    })
}


//table 
export function gettabList() {
    return fly.post('/basic/pageConfig/tabs', {
        "showPage": 1,
        "platform": 0,
    })
}

//全部
export function getallList(payload) {
    return fly.post('/c_msh/mLife/goods/list/queryByTab', {
        "cityId": 19,
        "tabId": payload,
        "showPage": 1,
        "platform": 0,
        "showSort": 1,
        "showSortMode": 1,
        "pageNo": 1,
        "pageSize": 10,
    })
}