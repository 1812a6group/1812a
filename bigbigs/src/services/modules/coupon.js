import request from '../../utils/request';

//获取优惠券数据
export function getCouponList(type) {
    return request.post('/c_msh/mLife/coupon/user/list', {
        "type": type,
        "startId": 0,
        "size": 20,
        "pageNo": 1,

    })
}