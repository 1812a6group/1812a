import fly from "../../utils/request";
//获取本地城市信息
export function getLocationCity() {
    return fly.post('/api-comm/api/comm.location2city', {
        "longitude": 116.29845,
        "latitude": 39.95933,
    })
}
//获取城市
export function getCityList() {
    return fly.post('/basic/app/getCityList')
}