import request from '../../utils/request';

//获取美味付款数据
export function getTastyList() {
    return request.post('/c_msh/mLife/orders/newOrderList', {
        "type": 0,
        "pageNo": 1,
        "pageSize": 20,
        "allMsh": 0,
    });
}