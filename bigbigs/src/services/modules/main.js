import request from '../../utils/request';

//我的用户信息
export function getMainUser() {
    return request.post('/c_msh/mLife/users/userGet');
}