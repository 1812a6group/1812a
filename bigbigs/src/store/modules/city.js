import { getCityList, getLocationCity } from "@/services";
const state = {
    CityList: [],
    cityId: 0
}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async getCityList({ commit }) {
        let res = await getCityList()
        if (res.errNo === 0) {
            commit('update', {
                CityList: res.data
            })
        }
    },
    async getLocationCity({ commit }) {
        let res = await getLocationCity()
        if (res.errno === 0) {
            commit('update', {
                cityId: res.data.cityId
            })
        }
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};