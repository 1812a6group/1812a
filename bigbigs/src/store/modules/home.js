import { getBanner, getHomeList, getOperatList } from "@/services";

const state = {
    banner: [],
    homeList: [],
    OperatList: [],

}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    async getBanner({ commit }, payload) {
        let res = await getBanner()
        if (res.errNo === 0) {
            commit('update', {
                banner: res.data
            })
        }
    },
    async getHomeList({ commit }) {
        let res = await getHomeList()
        if (res.errNo === 0) {
            commit('update', {
                homeList: res.data.items
            })
        }
    },
    async getOperatList({ commit }) {
        let res = await getOperatList()
        if (res.errNo === 0) {
            commit('update', {
                OperatList: res.data
            })
        }
    },

};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};