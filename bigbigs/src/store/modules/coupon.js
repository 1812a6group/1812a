import { getCouponList } from '@/services';

const state = {
    couponList: []
}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key]
        }
    }
};

const actions = {
    async getCouponList({ commit }, payload) {
        let result = await getCouponList(payload);
        if (result.errNo === 0) {
            commit('update', {
                couponList: result.data
            })
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}