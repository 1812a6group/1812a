import { getCollectList, getCollectShopList } from '../../services';

const state = {
    collectList: [],
    collectShopsList: []
}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key]
        }
    }
};

const actions = {
    async getCollectList({ commit }) {
        let result = await getCollectList();
        if (result.errNo === 0) {
            commit('update', {
                collectList: result.data.shops
            })
        }
    },
    async getCollectShopList({ commit }) {
        let result = await getCollectShopList();
        if (result.errNo === 0) {
            commit('update', {
                collectShopsList: result.data.collectList
            })
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}