import {
    getdiscountList,
    gettabList,
    getallList
} from "../../services";

const state = {
    discountList: [],
    slideshowList: [], //轮播图 
    navList: [], //导航
    recommendList: [], //新品推荐
    sermealList: [], //超值套餐
    tabList: [], //tab
    allList: [], //全部
}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
};

const actions = {
    //大牌优惠数据
    async getdiscountList({
        commit
    }, payload) {
        let res = await getdiscountList()
        console.log(res);
        if (res.errno === 0) {
            commit('update', {
                discountList: res.data, //总数据
                slideshowList: res.data.page[1].params.data, //轮播图
                navList: res.data.page[2].params.data, //导航
                recommendList: res.data.page[3].params.data, //新品推荐
                sermealList: res.data.page[4].params.data //超值套餐
            })
        }
    },
    //tab
    async gettabList({
        commit
    }, payload) {
        let res = await gettabList()
        console.log(res);
        if (res.errNo === 0) {
            commit("update", {
                tabList: res.data
            })
        }
    },
    //全部
    async getallList({
        commit
    }, payload) {
        let res = await getallList(payload)
        console.log(res);
        if (res.errNo === 0) {
            commit("update", {
                allList: res.data.data
            })
        }
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};