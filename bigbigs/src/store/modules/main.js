import { getMainUser } from '@/services';

const state = {
    usermessage: [],
}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key]
        }
    }
};

const actions = {
    async getMainUser({ commit }, payload) {
        let result = await getMainUser();
        if (result.errNo === 0) {
            commit('update', {
                usermessage: result.data
            })
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}