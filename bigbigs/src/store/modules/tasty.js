import { getTastyList } from '../../services';

const state = {
    tastyList: []
}

const mutations = {
    update(state, payload) {
        for (let key in payload) {
            state[key] = payload[key]
        }
    }
};

const actions = {
    async getTastyList({ commit }, payload) {
        let result = await getTastyList();
        if (result.errNo === 0) {
            commit('update', {
                tastyList: result.data.list
            })
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}