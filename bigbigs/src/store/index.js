import Vuex from 'vuex';
import Vue from 'vue';

import createLogger from 'vuex/dist/logger';
import home from './modules/home';
import discount from './modules/discount';
import main from './modules/main';
import city from './modules/city';
import tasty from './modules/tasty';
import coupon from './modules/coupon';
import collect from './modules/collect';
Vue.use(Vuex);

export default new Vuex.Store({
        modules: {
                home,
                discount,
                main,
                city,
                tasty,
                coupon,
                collect,
        },
        plugins: [createLogger()]
});