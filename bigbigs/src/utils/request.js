var Fly = require("flyio/dist/npm/wx")
var fly = new Fly
fly.config = {
    baseURL: 'https://capi.mwee.cn',
    timeout: 5000
}

//添加请求拦截器
const body = {
    longitude: 116.29844665527344,
    latitude: 39.95933151245117,
    cityId: 19,
    "mwAuthToken": "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqtKt7m3ZnIFOSX-C2jDEX6NtEeHqtqY-BNSAQcx56TOuU3wYSUP7Y-Bn4fSz08PX2nrk7rsQA0HYr6IjdJUXlo9nJfjDbknyA0xEiYVh6AN4d2immg3ikGbioX-CdX-CmxquXpSF7U5Krm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
    "mAuthToken": "uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqtKt7m3ZnIFOSX-C2jDEX6NtEeHqtqY-BNSAQcx56TOuU3wYSUP7Y-Bn4fSz08PX2nrk7rsQA0HYr6IjdJUXlo9nJfjDbknyA0xEiYVh6AN4d2immg3ikGbioX-CdX-CmxquXpSF7U5Krm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
    fromw: 1401,
    // mwtoken:"uPCQvKl9RhX5r9SGP2noD0Ei0D3egOAN8IrKQ9lIY-BS0nZ8NKqZIMOel3oNkoUYqtQNMJwnVZp76YqPVMtsVVh4XmpdLdIF3rNSY-BJe8dSyjvPbVYY-BQli6uLY-BEil4XrICqEntClyPqOXxHz0Qrp5wBODD6AGADJ2NX-CS8QoMNY-B8O2bX-C50uH9Yoi9wbmevKQrr7Nrm1u2ZIbmPpChaX-Ce3QuHcg-Z-Z",
    // op: "info"


}
fly.interceptors.request.use((request) => {
    //给所有请求添加自定义header
    if (request.body) {
        request.body = {
            ...request.body,
            ...body
        }
    } else {
        request.body = body;
    }

    request.headers["X-Tag"] = "flyio";

    // if (request.url.indexOf("pop") > 0) {
    //     request.headers['Content-Type'] = 'application/x-www-form-urlencoded'
    // }

    //打印出请求体
    console.log(request.body)
        //终止请求
        //var err=new Error("xxx")
        //err.request=request
        //return Promise.reject(new Error(""))

    //可以显式返回request, 也可以不返回，没有返回值时拦截器中默认返回request
    return request;
})

//添加响应拦截器，响应拦截器会在then/catch处理之前执行
fly.interceptors.response.use(
    (response) => {
        //只将请求结果的data字段返回
        return response.data
    },
    (err) => {
        //发生网络错误后会走到这里
        //return Promise.resolve("ssss")
        console.log(err);
    }
)


export default fly